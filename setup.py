import json
from setuptools import setup


def get_version():
    info = json.loads(open('iocdb/data/build_info.json').read())
    return info['version_id']


setup(
    name='iocdb',
    version=get_version(),
    description='security systems integration and retention of securty events',
    long_description=open('README.md').read(),
    url='https://github.com/vz-risk/iocdb',
    author='Nathan Buesgens, Stephen Brannon, Keith Gilbert, Andy Barilla',
    author_email='andrew.barilla@verizon.com',
    license='Other/Proprietary',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Information Technology',
        'Topic :: Security',
        'License :: Other/Proprietary',
        'Programming Language :: Python :: 2.7'
    ],
    keywords='security SIEM intelligence',
    packages=['iocdb', 'iocdb.encoders', 'iocdb.model',
              'iocdb.rest', 'iocdb.session',
              'iocdb.mappings', 'iocdb.feeds',
              'iocdb.feeds.legacy_hourly', 'iocdb.feeds.legacy_daily',
              'iocdb.feeds.legacylibs', 'iocdb.feeds.legacylibs.lib',
              'iocdb.feeds.legacylibs.lib.dns',
              'iocdb.feeds.legacylibs.lib.mechanize'],
    install_requires=['sqlalchemy', #TODO: only required for sql
                      #'psycopg2', #...
                      'celery', #TODO: only required for the worker
                      #'mock', #TODO: only required for unit testing
                      'elasticsearch',
                      'elasticsearch_dsl',
                      'pyyaml',
                      'flask',
                      'flask-sqlalchemy',
                      'flask-migrate',
                      'mattdaemon',
                      'requests',
                      'lockfile',
                      'stix',
                      'cybox',
                      'passlib',
                      'geoip2',
                      'shodan'
                      ],
    entry_points={
        'console_scripts': [
            'iocdb = iocdb.querycli:main',
            'iocdb-worker = iocdb.dispatcher:app.start',
            'iocdb-rest = iocdb.rest:main',
            'iocdb-edit = iocdb.editcli:main',
            'iocdb-init = iocdb.initcli:main',
            'iocdb-feed-test = iocdb.feedtest:main'
        ],
    },
    package_data={
        'iocdb': ['data/ioc.db', 'data/settings.yaml', 'data/build_info.json', 'rest/static/lib/*', 'rest/static/img/*', 'rest/static/*.*']
    }
)
