import datetime
from Queue import Queue
from threading import Thread

import elasticsearch
import elasticsearch.helpers

start_date = datetime.datetime(2013,03,01)
end_date = datetime.datetime(2014,12,8,16)

legacy_hosts = [
  {'host':'10.114.75.164'}
]

staging_hosts = [
  {'host':'10.114.75.131'},
  {'host':'10.114.75.132'},
  {'host':'10.114.75.133'},
]

#TODO: sessions do not appear to be thread safe maybe due to 
#https://github.com/elasticsearch/elasticsearch-py/issues/158
#instead give each session it's own connection
#legacy_session = elasticsearch.Elasticsearch(legacy_hosts)
#staging_session = elasticsearch.Elasticsearch(staging_hosts)

migrate_q = Queue()
status_q = Queue()

number_of_migrate_workers = 8

def migrate_range(received_starting, received_ending, legacy_session, staging_session):
    job_string = '%s -> %s' % (received_starting, received_ending)
    status_q.put('starting job: %s' % job_string)
    query = {'query':{
      "filtered":{
        "filter":{
          "range":{
            "document.received":{"gte":received_starting, "lt":received_ending}
          }
        }
      }
    }}
    legacy_results = elasticsearch.helpers.scan(legacy_session, query, '60m', 
                                                index='iocdb')
    results = elasticsearch.helpers.bulk(staging_session,
                                         _make_actions(legacy_results))
    status_q.put('job result: %s: %s' % (job_string, results))

def _make_actions(legacy_results):
    for result in legacy_results:
        result[u'_index'] = 'iocdb'
        del result[u'_score']
        yield result

def migrate_worker():
    legacy_session = elasticsearch.Elasticsearch(legacy_hosts)
    staging_session = elasticsearch.Elasticsearch(staging_hosts)
    while True:
        received_starting, received_ending = migrate_q.get()
        migrate_range(received_starting, received_ending, legacy_session, staging_session)
        migrate_q.task_done()

def status_worker():
    while True:
        print(status_q.get())
        status_q.task_done()

for i in range(number_of_migrate_workers):
    t = Thread(target=migrate_worker)
    t.daemon = True
    t.start()

t = Thread(target=status_worker)
t.daemon = True
t.start()

for hour in range(1, int((end_date - start_date).days)*24+1):
    received_starting = end_date - datetime.timedelta(hours=hour)
    received_ending = end_date - datetime.timedelta(hours=hour-1)
    migrate_q.put((received_starting, received_ending))

migrate_q.join()
status_q.join()
    
