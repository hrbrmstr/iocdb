...a platform for security systems integration and retention of securty events.
Security events are made available via a cli, or rest api.

## Quick Start
```shell
$ wget https://gist.githubusercontent.com/natb1/26e4cbae693985d6c111/raw/8d105536507fadf36dfc6bf995969cbc29ee429f/requirements.txt
$ pip install -r requirements.txt
```
> ###### _Hint_
> - _make sure your ssh key grants you access to the private iocdb repo_  
> - _make sure your user has the appropriate permissions for your environment - consider `virtualenv`_

Some of the retention strategies implemented by iocdb require an initialization
step. For example, to initialize a sqlite database for use by iocdb:
```python
>>> import iocdb.session_managers.sqlite
>>> default_sqlite = iocdb.session_managers.sqlite.SQLite()
>>> default_sqlite.setup()

```
[Feeds]() will send messages at their own pace once the iocdb [dispatcher]() is
running. Test data can be loaded on demand for demonstration purposes:
```python
>>> import test_data.threat_intel
>>> with default_sqlite.make_session() as session:
...     session.add_all(test_data.threat_intel.rumors)

```

## Testing
```
$ pip install nose
$ nosetests --with-doctest --doctest-extension=md --doctest-fixtures=-fixt 2>&1 README.md tests/examples | less
```

## The Query CLI
```shell
$ iocdb --help
```
Query rumors about an observable (output results to a [json encoder]() by 
default):
```js
$ iocdb rumors --observable_values evil.example.com
[
  {
    "actor": {
      "id": 3,
      "name": "KungFuPanda"
    },
    "campaign": {
      "id": 9,
      "name": "some breach event"
    },
    ...
    "document": {
      "category": "osint",
      "id": 777,
      "investigator": "Steven Seagal",
      "name": "some extracted intel",
      "nocomm": null,
      "noforn": false,
      "received": "2014-04-11T16:30:00",
      "source": "some blog post",
      "text": null,
      "tlp": "white"
    },
    ...
    "observable": {
      "id": 123,
      "value": "evil.example.com",
      "variety": "domain"
    },
    "ttp": {
      "actions": null,
      "category": "exfiltration site",
      "id": 22,
      "malware": null
    },
    "valid": "2014-04-10T00:00:00",
    ...
  },
  ...
]

```
Results can be output to any implemented [encoder]():
```shell
$ iocdb rumors --encoder csv \
>     --csv_columns valid document_name document_investigator
>     --observable_values evil.example.com
valid,actor_name,document_investigator
2014-04-10 00:00:00,KungFuPanda,Steven Seagal
2013-03-10 15:29:00,GuyFox,Jackie Chan

```
The [`STIXWriter`]() will output XML that adheres to the [STIX]()
specification:
```shell
$ iocdb rumors --encoder stix \
>     --observable_values evil.example.com
...

```
The query cli can take observable values from a file of newline seperated 
values:
```shell
$ cat observable_values.dat
evil.example.com
benign.example.com
$ iocdb rumors --observable_file observable_values.dat
...

```

## Feeds
Feeds are primarily responsible for sending messages to the 
[dispatcher]().
This may involve:
- extracting data or processing messages from external systems
- enriching data if necessary
- translating data to the iocdb [domain model]()

A guide to writing feeds can be found in the wiki:
[Implementing Feeds]().

The following feeds are packaged with iocdb:
- abuse.ch
- atlas.arbor.net
- atomicorp.com/channels/rules/delayed/modsec/domain-blacklist.txt
- autoshun.org
- blocklist.de
- botscout.com
- charles.the-haleys.org/ssh\_dico\_attack\_hdeny_format.php/hostsdeny.txt
- ciarmy.com/list/ci-badguys.txt
- dan.me.uk/tornodes
- danger.rulez.sk/projects/bruteforceblocker
- dnsbl.manitu.net
- dragonresearchgroup.org
- dshield.org
- emergingthreats.net
- hosts-file.net
- isc.sans.edu
- kleissner.org/text/ZeuSGameover_Domains.txt
- malc0de.com
- malwaredb.malekal.com
- malwaredomainlist.com
- mtc.sri.com
- nothink.org/blacklist
- openbl.org
- osint.bambenekconsulting.com
- packetmail.net/iprep.txt
- phishtank.com
- projecthoneypot.org
- reputation.alienvault.com
- security-research.dyndns.org
- stopforumspam.com
- tor directory authorities
- trustedsec.com/banlist.txt
- twitter.com/naughty_ips
- virbl.org
- vxvault.siri-urz.net
- webinspector.com/recent_detections

## The REST API:
`rest.py` is a [flask]() app that implements a rest api for querying and
producing messages for the [dispatcher](). 

`iocdb-rest` will initialize a 
[flask dev server]() to host the rest api:
```shell
$ iocdb-rest --help
...

```

## The Message Dispatcher
`iocdb-dispatcher` is a [celery]() app that propagates messages sent by 
[feeds]() or [the rest api](). By default, the `iocdb-dispatcher` requires
deployment of [RabbitMQ](http://www.rabbitmq.com/download.html).
```shell
$ iocdb-dispatcher worker --loglevel info
...

```
`iocdb-dispatcher` is also used to invoke cron-jobs:
```shell
$ iocdb-dispatcher beat --loglevel info
...

```
The [`propagate`]() function of the dispatcher is 
used to distribute messages to message handlers:
```python
>>> import iocdb.dispatcher
>>> iocdb.dispatcher.propagate(domain_objects) # doctest: +SKIP
```
...where `domain_objects` is an iterator of [domain objects](). 

Message handlers look like:
```python
class MyMessageHandler:
    def handle_message(self, domain_objects):
        ...
```
...where `domain_objects` is an iterator of [domain objects]().

A guid to writing message handlers can be found in the wiki:
[Implementing Message Handlers]().

## Repository and Encoder Sessions
[Repository sessions]() implement strategies for querying:
```python
class MyRepositorySession:
    def query(criteria):
        ...
        return domain_objects
```
...where `criteria` is a [query criteria object]() and `domain_objects` is an 
iterator of domain objects.

[Encoder sessions]() implement strategies for handling domain objects:
```python
class MyEncoderSession:
    def add_all(domain_objects):
        ...
```
...where `domain_objects` is an iterator of domain objects.

Sessions are
[context managers](https://docs.python.org/2.7/library/stdtypes.html#context-manager-types)
that are initialized by [session managers]():
```python
class MySessionManager:
    def make_session(self):
        ...
        return context_manager
```
...where `context_manager` is a session context manager.

The following [session managers]() are packaged with iocdb:
- SQLite: SQL encoding and querying with minimal environmental dependencies
- SQLAlchemy: encoding and querying using [SQLAlchemy]()
- ElasticSearch: encoding and querying using [ElasticSearch]()
- CSVEncoder: encoder for producing csv data
- JSONEncoder: encoder for producing json data
- STIXEncoder: encoder for producing [STIX]() XML

A guide to writing [session managers]() is in the wiki: 
[Implementing SessionManagers]()

## The Domain Model
Rumor: relates a cyber observable to a threat - TTP, threat actor, or threat
campaign.
```python
{
  "valid":"2013-05-16",
  "observable":{ "variety":"url", "value":"evil.example.com/bad" },
  "ttp":{ "category":"Exploit server" }
  "document":{
    "name":"govindicators05162013.csv",
    "category":"govint",
    "investigator":"Steven Seagal",
    "tlp":"amber"
  }
}
```

Association: describes a relationship between observables.
```python
{
  "valid":"2013-05-16",
  "variety":"resolves to",
  "left": {"value":"example.com", "variety":"domain"},
  "right": {"value":"192.0.43.10", "variety":"ip"},
  "document":{"source":"iptrack"}
}
```

![alt text](https://docs.google.com/drawings/d/1eljckvF3yhveguBwAX69HmAqzvpdxJvjB14F9VTMgA8/pub?w=689&h=268 "ER Diagram")

## Additional Resources
- [wiki home](https://github.com/vz-risk/iocdb/wiki)
- [configuration](https://github.com/vz-risk/iocdb/wiki/configuration)
- [examples](https://github.com/vz-risk/iocdb/tree/master/tests/examples)
