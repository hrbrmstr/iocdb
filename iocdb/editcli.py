__author__ = 'abarilla'

import argparse
import json

from utils import arg_to_datetime
from encoders import CustomJSONEncoder
from config import Config
from model import Actor
from model import Campaign
from model import Observable
import iocdb.model

def main():
    config, args = parse_args()

    session = config.make_session()

    if args.edittype == 'update':
        if args.object_id is None:
            print "object_id parameter is required for update function"
            return

        if args.model == 'rumor':
            rumor = session.get_rumor_by_id(args.object_id)
            if rumor is None:
                print "rumor not found for id: %s" % args.object_id
                return

            rumor = fill_rumor_from_args(rumor, args)
            session.update_rumor(rumor)
            rumor = session.get_rumor_by_id(args.object_id)

            print json.dumps(rumor, cls=CustomJSONEncoder, indent=2, sort_keys=True)
            return
        elif args.model == 'association':
            association = session.get_association_by_id(args.object_id)
            if association is None:
                print "association not found for id: %s" % args.object_id
                return

            association = fill_association_from_args(association, args)
            session.update_association(association)
            association = session.get_association_by_id(args.object_id)

            print json.dumps(association, cls=CustomJSONEncoder, indent=2, sort_keys=True)
            return
    elif args.edittype == 'delete':
        if args.object_id is None:
            print "object_id parameter is required for delete function"
            return

        if args.model == 'rumor':
            rumor = session.get_rumor_by_id(args.object_id)
            if rumor is None:
                print "rumor not found for id: %s" % args.object_id
                return

            session.delete_rumor(args.object_id)
            return

        elif args.model == 'association':
            association = session.get_association_by_id(args.object_id)
            if association is None:
                print "association not found for id: %s" % args.object_id
                return

            session.delete_association(args.object_id)
            return


def fill_rumor_from_args(rumor, args):
    if args.observable_value is not None:
        rumor.observable.value = args.observable_value
    if args.observable_variety is not None:
        rumor.observable.variety = args.observable_variety

    if args.valid is not None:
        rumor.valid = args.valid

    if args.document_name is not None:
        rumor.document.name = args.document_name
    if args.document_category is not None:
        rumor.document.category = args.document_category
    if args.document_source is not None:
        rumor.document.source = args.document_source
    if args.document_investigator is not None:
        rumor.document.investigator = args.document_investigator
    if args.document_tlp is not None:
        rumor.document.tlp = args.document_tlp
    if args.document_noforn is not None:
        rumor.document.noforn = args.document_noforn
    if args.document_nocomm is not None:
        rumor.document.nocomm = args.document_nocomm
    if args.document_received is not None:
        rumor.document.received = args.document_received
    if args.document_text is not None:
        rumor.document.text = args.document_text

    if args.ttp_category is not None:
        rumor.ttp.category = args.ttp_category
    if args.ttp_malware is not None:
        rumor.ttp.malware = args.ttp_malware
    if args.ttp_actions is not None:
        rumor.ttp.actions = args.ttp_actions

    if args.actor_name is not None:
        if rumor.actor is None:
            rumor.actor = Actor(name=args.actor_name)
        else:
            rumor.actor.name = args.actor_name

    if args.campaign_name is not None:
        if rumor.campaign is None:
            rumor.campaign = Campaign(name=args.campaign_name)
        else:
            rumor.campaign.name = args.campaign_name

    if args.description is not None:
        rumor.description = args.description

    return rumor


def fill_association_from_args(association, args):
    if args.variety is not None:
        association.variety = args.variety

    if args.valid is not None:
        association.valid = args.valid

    if args.document_name is not None:
        association.document.name = args.document_name
    if args.document_category is not None:
        association.document.category = args.document_category
    if args.document_source is not None:
        association.document.source = args.document_source
    if args.document_investigator is not None:
        association.document.investigator = args.document_investigator
    if args.document_tlp is not None:
        association.document.tlp = args.document_tlp
    if args.document_noforn is not None:
        association.document.noforn = args.document_noforn
    if args.document_nocomm is not None:
        association.document.nocomm = args.document_nocomm
    if args.document_received is not None:
        association.document.received = args.document_received
    if args.document_text is not None:
        association.document.text = args.document_text

    if args.left_value is not None:
        association.left.value = args.left_value
    if args.left_variety is not None:
        association.left.variety = args.left_variety

    if args.right_value is not None:
        association.right.value = args.right_value
    if args.right_variety is not None:
        association.right.variety = args.right_variety

    if args.description is not None:
        association.description = args.description

    return association


def parse_args(commandline=None):
    parser = argparse.ArgumentParser(description='Edit IOCDB records')

    group = parser.add_argument_group('config')
    group.add_argument('--config', help='path to config file')

    group = parser.add_argument_group('logging')
    group.add_argument('--loglevel',
                       choices=('DEBUG', 'INFO', 'WARNING', 'ERROR'))

    group = parser.add_argument_group('edit type')
    group.add_argument(
        '--edittype', default='update', choices=['update', 'delete'],
        nargs='?', help='type of function to execute')

    group = parser.add_argument_group(
        'record args',
        'valid DATETIME format is yyyy-mm-dd or yyyy-mm-ddThh:mm:ss')
    group.add_argument(
        'model', default='rumor', choices=['rumor', 'association'],
        nargs='?', help='name of model to query')

    group = parser.add_argument_group('common fields')
    group.add_argument(
        '--document_category', type=unicode,  nargs='?', choices=iocdb.model.document.DOCUMENT_CATEGORY)
    group.add_argument(
        '--document_source', type=unicode,  nargs='?', metavar='TEXT')
    group.add_argument(
        '--document_name', type=unicode,  nargs='?', metavar='TEXT')
    group.add_argument(
        '--document_tlp', type=unicode,  nargs='?', choices=iocdb.model.document.TLP)
    group.add_argument(
        '--document_investigator', type=unicode,  nargs='?', metavar='TEXT')
    group.add_argument(
        '--document_noforn', type=unicode,  nargs='?', choices=['true', 'false'])
    group.add_argument(
        '--document_nocomm', type=unicode,  nargs='?', choices=['true', 'false'])
    group.add_argument(
        '--document_received',  nargs='?', type=arg_to_datetime, metavar='DATETIME')
    group.add_argument(
        '--document_text', type=unicode,  nargs='?', metavar='TEXT')
    group.add_argument(
        '--valid',  nargs='?', type=arg_to_datetime, metavar='DATETIME')
    group.add_argument(
        '--description', type=unicode,  nargs='?', metavar='TEXT')

    group = parser.add_argument_group('rumor only fields')
    group.add_argument(
        '--observable_variety', type=unicode,  nargs='?', choices=iocdb.model.observable.OBSERVABLE_VARIETY)
    group.add_argument(
        '--observable_value', type=unicode,  nargs='?', metavar='TEXT')
    group.add_argument(
        '--actor_name', type=unicode,   nargs='?', metavar='TEXT')
    group.add_argument(
        '--campaign_name', type=unicode,  nargs='?', metavar='TEXT')
    group.add_argument(
        '--ttp_malware', type=unicode,  nargs='?', metavar='TEXT')
    group.add_argument(
        '--ttp_category', type=unicode,  nargs='?', choices=iocdb.model.ttp.TTP_CATEGORY)
    group.add_argument(
        '--ttp_actions', type=unicode,  nargs='?', metavar='TEXT')

    group = parser.add_argument_group('association only fields')
    group.add_argument(
        '--variety', type=unicode,  nargs='?', metavar='TEXT')

    group.add_argument(
        '--left_value', type=unicode,  nargs='?', metavar='TEXT')
    group.add_argument(
        '--left_variety', type=unicode,  nargs='?', metavar='TEXT')

    group.add_argument(
        '--right_value', type=unicode,  nargs='?', metavar='TEXT')
    group.add_argument(
        '--right_variety', type=unicode,  nargs='?', metavar='TEXT')

    group.add_argument(
        '--object_id', type=unicode, metavar='TEXT', help='id of object as stored in data store')

    if commandline is None:
        args = parser.parse_args()
    else:
        args = parser.parse_args(args=commandline.split())

    config = Config(args.config)

    if args.loglevel is not None:
        config.debug_mode = True

    return config, args


if __name__ == '__main__':
    main()
