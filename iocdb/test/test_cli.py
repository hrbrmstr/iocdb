__author__ = 'abarilla'

import mock
import unittest
from tempfile import NamedTemporaryFile

import elasticsearch

from iocdb.errors import ParametersRequiredError
from iocdb import querycli
from iocdb.utils import find_key

class TestCommandLineParameters(unittest.TestCase):
    def test_parameter_required(self):
        """Ensures that an error is raised if no parameters are passed
        """
        config, args = querycli.parse_args('')
        self.assertRaises(ParametersRequiredError, querycli.search_rumors, config, args)

    @mock.patch('elasticsearch.helpers.scan', spec=elasticsearch.helpers.scan)
    def test_observable_file(self, mock_scan):
        """Ensures that the observable_file parameter is read and treated like entries for the
        observable_values parameter
        """
        sample_input = ['value1', 'value2']
        with NamedTemporaryFile() as input_file:
            input_file.write('\n'.join(sample_input))
            input_file.flush()
            config, args = querycli.parse_args('rumors --observable_file %s' % input_file.name)
            querycli.search_rumors(config, args)
            args, kwargs = mock_scan.call_args
            query = args[1]
            # ensure the values of the file ended up in the query in the observable.value field
            self.assertEqual(find_key(u'observable.value', query), sample_input)
