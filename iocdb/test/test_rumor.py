import unittest
import mock

from iocdb import querycli
from iocdb.session.elasticsearchsession import ElasticsearchSession

class TestRumorFetch(unittest.TestCase):

    @mock.patch.object(ElasticsearchSession, 'get_rumor_by_id', autospec=True)
    def test_get_by_id(self, mock_get_rumor):
        """Ensures that the object_id parameter loads a rumor and the proper parameter value is used
        """

        test_object_id = '123456'

        config, args = querycli.parse_args('--object_id %s' % test_object_id)
        querycli.search_rumors(config, args)

        args, kwargs = mock_get_rumor.call_args
        self.assertTrue(test_object_id in args)
