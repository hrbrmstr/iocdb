import logging
import logging.config
import os
import os.path
from yaml import load
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

import session

from iocdb.errors import InvalidSettingsFileError
from iocdb.utils import Singleton

ENCODER_CHOICES = [
    'json',
    'csv',
    'summary',
    'stix'
]

REPOSITORY_CHOICES = [
    'sqlite',
    'elasticsearch'
]


class Config(object):
    __metaclass__ = Singleton

    DEBUG_MODE = 'debug'
    MESSAGE_HANDLERS = 'message_handlers'
    ELASTICSEARCH_CONFIG = 'elasticsearch_config'
    ELASTICSEARCH_HOSTS = 'hosts'
    REPOSITORY = 'repository'
    URL_TIMEOUT = 'url_timeout'
    HOURLY_MINUTE = 'hourly_minute'
    DAILY_HOUR = 'daily_hour'
    DAILY_MINUTE = 'daily_minute'
    ES_TRACE = 'es_trace'

    DEFAULT_SETTINGS = ['/src/iocdb/iocdb/data/settings.yaml',
                        os.path.join(os.getcwd(), 'settings.yaml')]

    _message_handlers = None
    _elastic_search_hosts = ['localhost']
    _debug_mode = False
    _repository = 'sqlite'
    _encoder = 'json'
    _url_timeout = 300
    _hourly_minute = 40
    _daily_hour = 4
    _daily_minute = 0
    _es_trace = False

    files = []
    feedtest = False

    def __init__(self, settings_file_name=None):
        for settings in self.DEFAULT_SETTINGS:
            if os.path.isfile(settings):
                    self._load_settings(settings)

            if settings_file_name is not None:
                if os.path.isfile(settings_file_name):
                    self._load_settings(settings_file_name)
                else:
                    raise InvalidSettingsFileError

    def _load_settings(self, file_name):
        self.files.append(file_name)
        # print("=====> Loading settings from %s" % file_name)
        f = open(file_name, 'r')
        data = load(f)

        self._message_handlers = data[self.MESSAGE_HANDLERS]
        if self.ELASTICSEARCH_CONFIG in data:
            self._elastic_search_hosts = data[self.ELASTICSEARCH_CONFIG][self.ELASTICSEARCH_HOSTS]
        if self.DEBUG_MODE in data:
            self._debug_mode = data[self.DEBUG_MODE]
        if self.REPOSITORY in data:
            self._repository = data[self.REPOSITORY]
        if self.URL_TIMEOUT in data:
            self._url_timeout = data[self.URL_TIMEOUT]
        if self.DAILY_HOUR in data:
            self._daily_hour = data[self.DAILY_HOUR]
        if self.DAILY_MINUTE in data:
            self._daily_minute = data[self.DAILY_MINUTE]
        if self.HOURLY_MINUTE in data:
            self._hourly_minute = data[self.HOURLY_MINUTE]
        if self.ES_TRACE in data:
            self._es_trace = data[self.ES_TRACE]

        if 'logging' in data:
            dict_config = data['logging']
            logging.config.dictConfig(dict_config)

    @property
    def message_handlers(self):
        return self._message_handlers

    @property
    def elastic_search_hosts(self):
        return self._elastic_search_hosts

    @property
    def debug_mode(self):
        return self._debug_mode

    @debug_mode.setter
    def debug_mode(self, value):
        self._debug_mode = value

    @property
    def encoder(self):
        return self._encoder

    @encoder.setter
    def encoder(self, value):
        self._encoder = value

    @property
    def repository(self):
        return self._repository

    @repository.setter
    def repository(self, value):
        self._repository = value

    @property
    def url_timeout(self):
        return self._url_timeout

    @url_timeout.setter
    def url_timeout(self, value):
        self._url_timeout = value

    @property
    def daily_hour(self):
        return self._daily_hour

    @daily_hour.setter
    def daily_hour(self, value):
        self._daily_hour = int(value)

    @property
    def daily_minute(self):
        return self._daily_minute

    @daily_hour.setter
    def daily_minute(self, value):
        self._daily_minute = int(value)

    @property
    def hourly_minute(self):
        return self._hourly_minute

    @hourly_minute.setter
    def hourly_minute(self, value):
        self._hourly_minute = int(value)

    @property
    def es_trace(self):
        return self._es_trace

    @es_trace.setter
    def es_trace(self, value):
        self._es_trace = value

    def make_session(self, repo_type=None):
        return session.get_session_manager(self, repo_type)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def __json__(self):
        return self.__dict__
