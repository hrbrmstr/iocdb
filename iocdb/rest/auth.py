__author__ = 'abarilla'

from functools import wraps

from flask import abort
from flask import g
from flask import request
from sqlalchemy.orm.exc import NoResultFound

from iocdb.rest.models import LogonSession


def check_token(token):
    try:
        session = LogonSession.query.filter(LogonSession.logon_token == unicode(token)).one()
    except NoResultFound:
        return None

    if session is None:
        return None

    if session.validate_time():
        return session
    else:
        return None


def process_logon():
    token = request.args['token'] if 'token' in request.args else None
    if token is None and 'X-LogonToken' in request.headers:
        token = request.headers['X-LogonToken']
    if not token:
        abort(401)
    session = check_token(token)
    if session is None:
        abort(401)
    g.active_user = session.active_user


def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        process_logon()
        return f(*args, **kwargs)
    return decorated


def requires_admin(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        process_logon()
        if not g.active_user.admin:
            abort(401)
        return f(*args, **kwargs)
    return decorated
