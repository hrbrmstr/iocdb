__author__ = 'abarilla'

import json

from flask import Response


from iocdb.utils import get_csv_output
from iocdb.encoders import CustomJSONEncoder


def dump_json(data):
    return Response(
        json.dumps(data, cls=CustomJSONEncoder, indent=2, sort_keys=True),
        status=200,
        mimetype='application/json')


def dump_csv(data, csv_columns):
    return Response(
        get_csv_output(data, csv_columns),
        status=200,
        mimetype='text/csv')
