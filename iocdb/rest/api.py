__author__ = 'Andrew Barilla <andrew.barilla@one.verizon.com>'

import json
import os
from threading import Thread

from flask import abort
from flask import request
from flask import g
import geoip2.database

import iocdb
from iocdb.rest import app
from iocdb.rest.utils import dump_csv
from iocdb.rest.utils import dump_json
from iocdb.utils import arg_to_datetime
import iocdb.config as config
from iocdb.errors import UnknownEncoderError
from iocdb.encoders import INTELSummaryEncoderSession
from iocdb.model import Actor
from iocdb.model import Campaign
from iocdb.model import Rumor
from iocdb.model import Association
from iocdb.rest.auth import requires_auth
from iocdb.rest.auth import requires_admin
from iocdb.utils import urlopen_with_retry
from iocdb import ext

URL_PREFIX = '/api/ioc'


@app.route(URL_PREFIX + '/ips/<ip>', methods=['GET'])
@requires_auth
def get_ip_report(ip):
    data = {}
    threads = []

    t = Thread(target=ext.vt_ip_check, args=(ip, data))
    t.start()
    threads.append(t)

    t = Thread(target=ext.pdns_ip_check, args=(ip, data))
    t.start()
    threads.append(t)

    t = Thread(target=ext.geo_locate, args=(ip, data))
    t.start()
    threads.append(t)

    t = Thread(target=get_ip_ioc, args=(ip, data))
    t.start()
    threads.append(t)

    t = Thread(target=ext.shodan_lookup, args=(ip, data))
    t.start()
    threads.append(t)

    for t in threads:
        t.join()

    data['address'] = ip
    data['type'] = 'ip'
    return dump_json(data)


def get_ip_geolocation(ip):
    data = {}

    reader = geoip2.database.Reader(os.path.join(iocdb.datadir, 'GeoLite2-City.mmdb'))
    response = reader.city(ip)
    if response is not None:
        data['latitude'] = response.location.latitude
        data['longitude'] = response.location.longitude
        data['ccode'] = response.country.iso_code
        data['country'] = response.country.name
        data['city'] = response.city.name
        data['region'] = response.subdivisions.most_specific.iso_code
        data['rname'] = response.subdivisions.most_specific.name
        data['time_zone'] = response.location.time_zone

    geodata = urlopen_with_retry('http://www.telize.com/geoip/' + ip)
    if geodata is not None:
        geodata = json.loads(geodata.read())
        data['organization'] = geodata.get('isp')
        data['asn'] = geodata.get('asn')
    # dats['organization'] = organization.decode('utf-8', "ignore")
    # dats['range'] = org_range
    # dats['asn'] = str(asn_id).decode('utf-8', "ignore")

    reader.close()
    return data


def get_ip_ioc(ip, data):
    session = config.Config().make_session()
    data['rumors'] = session.search_rumors(observable_values=[ip])


@app.route(URL_PREFIX + '/rumors/<rumor_id>', methods=['GET'])
@requires_auth
def get_rumor_by_id(rumor_id):
    session = app.config['config'].make_session()
    rumor = session.get_rumor_by_id(rumor_id)
    user = g.active_user
    if rumor.document.noforn and not user.us_citizen:
        abort(404)
    if rumor.document.nocomm and user.commercial:
        abort(404)
    return dump_json(rumor)


@app.route(URL_PREFIX + '/rumors/<rumor_id>/_update', methods=['PUT'])
@requires_admin
def update_rumor_legacy(rumor_id):
    session = app.config['config'].make_session()
    rumor = session.get_rumor_by_id(rumor_id)

    if 'observable_value' in request.args:
        rumor.observable.value = request.args['observable_value']
    if 'observable_variety' in request.args:
        rumor.observable.variety = request.args['observable_variety']

    if 'valid' in request.args:
        rumor.valid = arg_to_datetime(request.args['valid'])

    if 'document_name' in request.args:
        rumor.document.name = request.args['document_name']
    if 'document_category' in request.args:
        rumor.document.category = request.args['document_category']
    if 'document_source' in request.args:
        rumor.document.source = request.args['document_source']
    if 'document_investigator' in request.args:
        rumor.document.investigator = request.args['document_investigator']
    if 'document_tlp' in request.args:
        rumor.document.tlp = request.args['document_tlp']
    if 'document_noforn' in request.args:
        rumor.document.noforn = request.args['document_noforn']
    if 'document_nocomm' in request.args:
        rumor.document.nocomm = request.args['document_nocomm']
    if 'document_received' in request.args:
        rumor.document.received = arg_to_datetime(request.args['document_received'])
    if 'document_text' in request.args:
        rumor.document.text = request.args['document_text']

    if 'ttp_category' in request.args:
        rumor.ttp.category = request.args['ttp_category']
    if 'ttp_malware' in request.args:
        rumor.ttp.malware = request.args['ttp_malware']
    if 'ttp_actions' in request.args:
        rumor.ttp.actions = request.args['ttp_actions']

    if 'actor_name' in request.args:
        if rumor.actor is None:
            rumor.actor = Actor(name=request.args['actor_name'])
        else:
            rumor.actor.name = request.args['actor_name']

    if 'campaign_name' in request.args:
        if rumor.campaign is None:
            rumor.campaign = Campaign(name=request.args['campaign_name'])
        else:
            rumor.campaign.name = request.args['campaign_name']

    if 'description' in request.args:
        rumor.description = request.args['description']

    session.update_rumor(rumor)
    return dump_json(session.get_rumor_by_id(rumor_id))


@app.route(URL_PREFIX + '/rumors', methods=['POST'])
@requires_admin
def create_rumor():
    session = app.config['config'].make_session()
    data = json.loads(get_post_data())
    if isinstance(data, list):
        for item in data:
            rumor = Rumor.from_dict(item)
            session.create_rumor(rumor)
    else:
        rumor = Rumor.from_dict(data)
        session.create_rumor(rumor)
    return ''


@app.route(URL_PREFIX + '/rumors/<rumor_id>', methods=['PUT'])
@requires_admin
def update_rumor(rumor_id):
    session = app.config['config'].make_session()
    rumor = Rumor.from_dict(json.loads(get_post_data()))

    session.update_rumor(rumor)
    return dump_json(session.get_rumor_by_id(rumor_id))


@app.route(URL_PREFIX + '/rumors/<rumor_id>', methods=['DELETE'])
@requires_admin
def delete_rumor(rumor_id):
    session = app.config['config'].make_session()
    session.delete_rumor(rumor_id)
    return ''


@app.route(URL_PREFIX + '/rumors', methods=['GET'])
@requires_auth
def search_rumors():
    observable_values = get_param_value('observable_value', require_array=True)
    valid_starting = get_param_value('valid_starting', is_datetime=True)
    valid_ending = get_param_value('valid_ending', is_datetime=True)
    received_starting = get_param_value('received_starting', is_datetime=True)
    received_ending = get_param_value('received_ending', is_datetime=True)
    observable_varieties = \
        get_param_value('observable_variety', require_array=True)
    document_sources = get_param_value('document_source', require_array=True)
    document_names = get_param_value('document_name', require_array=True)
    document_investigators = \
        get_param_value('document_investigator', require_array=True)
    actor_names = get_param_value('actor_name', require_array=True)
    campaign_names = get_param_value('campaign_name', require_array=True)
    ttp_malware = get_param_value('ttp_malware', require_array=True)
    ttp_categories = get_param_value('ttp_category', require_array=True)
    csv_columns = get_param_value('csv_columns', require_array=True)
    do_summary = get_param_value('summary', default=False, is_boolean=True)
    encoder = get_param_value('f', default='json').lower()

    session = config.Config().make_session()
    data = session.search_rumors(observable_values=observable_values,
                                 valid_starting=valid_starting,
                                 valid_ending=valid_ending,
                                 received_starting=received_starting,
                                 received_ending=received_ending,
                                 observable_varieties=observable_varieties,
                                 document_sources=document_sources,
                                 document_names=document_names,
                                 document_investigators=document_investigators,
                                 actor_names=actor_names,
                                 campaign_names=campaign_names,
                                 ttp_malware=ttp_malware,
                                 ttp_categories=ttp_categories)

    if do_summary:
        with INTELSummaryEncoderSession() as summary_encoder:
            data = summary_encoder.get_summary(data)

    if encoder == 'json':
        return dump_json(data)
    elif encoder == 'csv':
        return dump_csv(data, csv_columns)
    else:
        raise UnknownEncoderError


@app.route(URL_PREFIX + '/associations/<association_id>', methods=['GET'])
@requires_auth
def get_association_by_id(association_id):
    session = app.config['config'].make_session()
    return dump_json(session.get_association_by_id(association_id))


@app.route(URL_PREFIX + '/associations', methods=['POST'])
@requires_admin
def create_association():
    session = app.config['config'].make_session()
    association = Association.from_dict(json.loads(get_post_data()))

    session.create_association(association)
    return ''


@app.route(URL_PREFIX + '/associations/<association_id>', methods=['PUT'])
@requires_admin
def update_association(association_id):
    session = app.config['config'].make_session()
    association = Association.from_dict(json.loads(get_post_data()))

    session.update_association(association)
    return dump_json(session.get_association_by_id(association_id))


@app.route(URL_PREFIX + '/associations/<association_id>', methods=['DELETE'])
@requires_admin
def delete_association(association_id):
    session = app.config['config'].make_session()
    session.delete_association(association_id)
    return ''


@app.route(URL_PREFIX + '/associations', methods=['GET'])
@requires_auth
def search_associations():
    valid_starting = get_param_value('valid_starting', is_datetime=True)
    valid_ending = get_param_value('valid_ending', is_datetime=True)
    received_starting = get_param_value('received_starting', is_datetime=True)
    received_ending = get_param_value('received_ending', is_datetime=True)
    csv_columns = get_param_value('csv_columns', require_array=True)
    encoder = get_param_value('f', default='json').lower()

    session = config.Config().make_session()
    data = session.search_associations(valid_starting=valid_starting,
                                       valid_ending=valid_ending,
                                       received_starting=received_starting,
                                       received_ending=received_ending)

    if encoder == 'json':
        return dump_json(data)
    elif encoder == 'csv':
        return dump_csv(data, csv_columns)
    else:
        raise UnknownEncoderError


def get_param_value(name, default=None, is_datetime=False, require_array=False,
                    is_boolean=False):
    if name in request.args:
        if is_datetime:
            return arg_to_datetime(request.args.get(name))
        elif is_boolean:
            value = request.args.get(name)
            return value == '1' or value == 1 or \
                value is True or value.lower() == 'true'
        elif require_array:
            return request.args.getlist(name)
        else:
            return request.args.get(name)
    else:
        return default


def get_post_data():
    data = None
    if request.data is not None and request.data is not '':
        data = request.data
    elif len(request.form) == 1:
        data = request.form.items()[0][0]

    return data
