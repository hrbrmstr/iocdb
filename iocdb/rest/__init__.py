__author__ = 'Andrew Barilla <andrew.barilla@one.verizon.com'

import os
import sys

from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.script import Manager, Server
from flask.ext.migrate import Migrate, MigrateCommand
import mattdaemon

import iocdb.config
from iocdb import datadir

app = Flask(__name__)
app.config['config'] = iocdb.config.Config()
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(datadir, 'iocdbapp.db')

db = SQLAlchemy(app)
migrate = Migrate(app, db)

manager = Manager(app)
manager.add_command('runserver', Server(use_debugger=True, use_reloader=True))
manager.add_command('db', MigrateCommand)

from iocdb.rest import views
from iocdb.rest import api
from iocdb.rest import models
from iocdb.rest import essa


@manager.command
def seed():
    if db.session.query(models.User).count() == 0:
        portal = models.User()
        portal.username = u'PortalLogin1'
        portal.password = u'password'
        portal.admin = True
        portal.commercial = False
        portal.us_citizen = False
        db.session.add(portal)
        db.session.commit()
        print('PortalLogin1 user created')
    else:
        print('ERROR: Can\'t seed database that already has data')


class FlaskDaemon(mattdaemon.daemon):
    def run(self, *args, **kwargs):
        print('Starting IOCDB rest interface')
        debug = False
        reloader = False

        if 'debug' in kwargs:
            debug = kwargs['debug']

        if 'reloader' in kwargs:
            reloader = kwargs['reloader']

        app.run(host='0.0.0.0', debug=debug, use_reloader=reloader, port=8000)


def main(debug=True, daemonize=True, reloader=False):
    action = 'start'
    if len(sys.argv) > 1:
        action = sys.argv[1]
    daem = FlaskDaemon(pidfile='/tmp/iocdb-rest.pid',
                       stdout='/var/log/iocdb/iocdb-rest.log',
                       stderr='/var/log/iocdb/iocdb-rest.log',
                       daemonize=daemonize)
    if action == 'start':
        daem.start(debug=debug, reloader=reloader)
    elif action == 'stop':
        daem.stop()
