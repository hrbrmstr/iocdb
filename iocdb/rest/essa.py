__author__ = 'Andrew Barilla <andrew.barilla@one.verizon.com>'

import uuid
from datetime import datetime

from flask import abort
from flask import request
from flask import make_response

from iocdb.rest import app
from iocdb.rest import db
from iocdb.rest.models import User
from iocdb.rest.models import LogonSession
from iocdb.rest.utils import dump_json
from iocdb.buildinfo import BuildInfo

URL_PREFIX = '/api/auth'
date_format = '%Y-%m-%dT%H:%M:%S.%fZ'


def raise_logon_failed():
    abort(400, (10044, 'Logon failed, contact support for further details'))


@app.errorhandler(400)
def custom400(error):
    if isinstance(error.description, (list, tuple)):
        return make_response(
            dump_json({'message': error.description[1],
                       'displayMessage': '%s, contact support for further details' % error.description[1],
                       'errorCode': error.description[0]}), 400)
    else:
        return make_response(dump_json({'displayMessage': error.description}), 400)


@app.route(URL_PREFIX + '/build')
def get_build():
    return dump_json({'version': BuildInfo.get_version_detailed()})


@app.route(URL_PREFIX + '/logon', methods=['POST'])
def logon():
    auth = request.authorization
    if auth is None:
        raise_logon_failed()

    if auth.username == '' or auth.password == '':
        raise_logon_failed()

    user = User.query.filter(User.username == unicode(auth.username)).first()
    if user is None or not user.verify_password(auth.password):
        raise_logon_failed()

    session = LogonSession()
    session.logon_token = unicode(uuid.uuid4())
    session.logon_time = datetime.utcnow()
    session.update_time = session.logon_time

    if 'impersonate' in request.args:
        if 'clientip' not in request.args:
            abort(400, (10125, 'Unable to perform logon, client ip required'))
        session.client_ip = request.args['clientip']
        user = User.query.filter(User.sso_id == unicode(request.args['impersonate'])).first()
        if user is None:
            user = User()
            user.sso_id = request.args['impersonate']
            user.us_citizen = False
            user.commercial = True
            user.admin = False
            db.session.add(user)
            db.session.commit()

    session.authenticated_user_id = user.id
    session.active_user_id = user.id
    db.session.add(session)
    db.session.commit()

    return dump_json(build_token_response(session))


def build_token_response(logon_session):
    return {
        "token": {
            "expires": logon_session.expiration_date.strftime(date_format),
            "tokenId": logon_session.logon_token
        },
        "user": {
            "@flexId": logon_session.active_user_id
        }
    }
