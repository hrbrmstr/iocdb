angular.module('localdb', []).
  factory('localdb', function() {

    var db = null;

    onerror = function(e) {
      console.log(e.value);
    };

    function opendb(onsuccess, onerror){
      if (db == null){
        _opendb(onsuccess, onerror);
      }else{
        onsuccess();
      }
    }

    LocalDB = {}
    LocalDB.get_user = function(onsuccess, onerror) {

      opendb(function (){

        var trans = db.transaction(['user']);
        var store = trans.objectStore('user');
        var request = store.openCursor();
      
        request.onsuccess = function(e) {
          if (e.target.result)
            onsuccess(e.target.result.value)
          else
            onsuccess(e.target.result)
        };

        request.onerror = function(e) {
          console.log(e.value)
          onerror()
        };
      }, onerror);
    };

    LocalDB.set_user = function(user, onerror) {

      opendb(function (){

        var trans = db.transaction(['user'], "readwrite");
        var store = trans.objectStore('user');
        //delete old user data
        store.openCursor().onsuccess = function(e) {
          if (e.target.result)
            store.delete(e.target.result.value.id)
        }
        var request = store.put(user);
      
        trans.oncomplete = function(){
          pass = true
          //do nothing
        };
      
        request.onerror = function(e) {
          console.log(e.value);
          onerror()
        };
      }, onerror);
    };
    
    function _opendb(oncomplete, onerror){
      newversion = false;
      var version = 1;
      var myobjects = []
      var request = indexedDB.open('appindex', version);
    
      request.onupgradeneeded = function(e) {
        db = e.target.result;
        transaction = e.target.transaction;
        e.target.transaction.onerror = onerror;
    
        if(db.objectStoreNames.contains('docs')) {
          db.deleteObjectStore('docs');
        }
    
        legacy_users = []
        if(db.objectStoreNames.contains('user')) {
          migraterequest = transaction.objectStore('user').openCursor();
          migraterequest.onsuccess = function(e){ 
            var cursor = e.target.result;
            if (cursor){
              legacy_users.push(cursor.value);
              cursor.continue();
            }
          };
          db.deleteObjectStore('user');
        }
    
        var doc_store = db.createObjectStore('docs',
          {keyPath: 'id', autoIncrement: true})
    
        var user_store = db.createObjectStore('user',
          {keyPath: 'id'})
    
        newversion = true
      }
    
      request.onsuccess = function(e) {
        db = e.target.result;
        oncomplete()
      }
    
      request.onerror = function(e) {
        console.log(e.value)
        onerror()
      }
    }

    LocalDB.save_tab = function(tab, oncomplete) {
      var trans = db.transaction(['docs'], "readwrite");
      var store = trans.objectStore('docs');
    
      if ('id' in tab) tab.id = parseInt(tab.id);
      var request = store.put(tab);
    
      trans.oncomplete = function(e) {
        if (oncomplete) oncomplete(request.result);
      }
    
      request.onerror = function(e) {
        console.log(e.value);
      };
    };
    
    LocalDB.query_tabs = function(onsuccess) {
    
      opendb(function(){
        var trans = db.transaction(['docs']);
        var store = trans.objectStore('docs');
      
        var request = store.openCursor();
        var tabresults = []
      
        request.onsuccess = function(e) {
          result = e.target.result;
          if(result == null){ //no more results
            if (onsuccess) onsuccess(tabresults);
            return;
          }
          tabresults.push(result.value);
          result.continue();
        }
      
        request.onerror = onerror;
      });
    };
    
    LocalDB.delete_tab = function(id) {
      var trans = db.transaction(['docs'], "readwrite");
      var store = trans.objectStore('docs');
    
      var request = store.delete(id);
    
      trans.oncomplete = function(e) {
        //do nothing
      };
    
      request.onerror = function(e) {
        //TODO: pass error to user
        console.log(e);
      };
    };
    
    return LocalDB;
  });

