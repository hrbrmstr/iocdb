module = angular.module('document', [])

//TODO: compile js Models from domain code
module.factory('Document', function() {
  return {

    category:['ciscp', 'csint', 'govint', 'ids2', 'osint', 'vecirt',
              'vzir', 'atims'],
    tlp:['white','green','amber','red'],

    init: function (kwargs) {
      if (!kwargs)
        kwargs = {}
      return {
          text:kwargs.text,
          name:kwargs.name,
          category:kwargs.category,
          tlp:kwargs.tlp,
          noforn:kwargs.noforn,
          nocomm:kwargs.nocomm,
          investigator:kwargs.investigator
    }}
  }});

//TODO: compile these regex's from domain model
var regex = {
  'url':/[A-Z0-9_\-\.]+\.(?:XN--CLCHC0EA0B2G2A9GCD|XN--HGBK6AJ7F53BBA|XN--HLCJ6AYA9ESC7A|XN--11B5BS3A9AJ6G|XN--MGBERP4A5D4AR|XN--XKC2DL3A5EE0H|XN--80AKHBYKNJ4F|XN--XKC2AL3HYE2A|XN--LGBBAT1AD8J|XN--MGBC0A9AZCG|XN--9T4B11YI5A|XN--MGBAAM7A8H|XN--MGBAYH7GPA|XN--MGBBH1A71E|XN--FPCRJ9C3D|XN--FZC2C9E2C|XN--YFRO4I67O|XN--YGBI2AMMX|XN--3E0B707E|XN--JXALPDLP|XN--KGBECHTV|XN--OGBPF8FL|XN--0ZWM56D|XN--45BRJ9C|XN--80AO21A|XN--DEBA0AD|XN--G6W251D|XN--GECRJ9C|XN--H2BRJ9C|XN--J6W193G|XN--KPRW13D|XN--KPRY57D|XN--PGBS0DH|XN--S9BRJ9C|XN--90A3AC|XN--FIQS8S|XN--FIQZ9S|XN--O3CW4H|XN--WGBH1C|XN--WGBL6A|XN--ZCKZAH|XN--P1AI|XN--NGBC5AZD|XN--80ASEHDB|XN--80ASWG|XN--UNUP4Y|MUSEUM|TRAVEL|AERO|ARPA|ASIA|COOP|INFO|JOBS|MOBI|BIZ|CAT|COM|EDU|GOV|INT|MIL|NET|ORG|PRO|TEL|XXX|AC|AD|AE|AF|AG|AI|AL|AM|AN|AO|AQ|AR|AS|AT|AU|AW|AX|AZ|BA|BB|BD|BE|BF|BG|BH|BI|BJ|BM|BN|BO|BR|BS|BT|BV|BW|BY|BZ|CA|CC|CD|CF|CG|CH|CI|CK|CL|CM|CN|CO|CR|CU|CV|CW|CX|CY|CZ|DE|DJ|DK|DM|DO|DZ|EC|EE|EG|ER|ES|ET|EU|FI|FJ|FK|FM|FO|FR|GA|GB|GD|GE|GF|GG|GH|GI|GL|GM|GN|GP|GQ|GR|GS|GT|GU|GW|GY|HK|HM|HN|HR|HT|HU|ID|IE|IL|IM|IN|IO|IQ|IR|IS|IT|JE|JM|JO|JP|KE|KG|KH|KI|KM|KN|KP|KR|KW|KY|KZ|LA|LB|LC|LI|LK|LR|LS|LT|LU|LV|LY|MA|MC|MD|ME|MG|MH|MK|ML|MM|MN|MO|MP|MQ|MR|MS|MT|MU|MV|MW|MX|MY|MZ|NA|NC|NE|NF|NG|NI|NL|NO|NP|NR|NU|NZ|OM|PA|PE|PF|PG|PH|PK|PL|PM|PN|PR|PS|PT|PW|PY|QA|RE|RO|RS|RU|RW|SA|SB|SC|SD|SE|SG|SH|SI|SJ|SK|SL|SM|SN|SO|SR|ST|SU|SV|SX|SY|SZ|TC|TD|TF|TG|TH|TJ|TK|TL|TM|TN|TO|TP|TR|TT|TV|TW|TZ|UA|UG|UK|US|UY|UZ|VA|VC|VE|VG|VI|VN|VU|WF|WS|YE|YT|ZA|ZM|ZW|[0-9]{1,3})(?:\/\S*)/i,
  'ip':/(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(?:\.|\[\.\])){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)/,
  'email':/[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.(?:XN--CLCHC0EA0B2G2A9GCD|XN--HGBK6AJ7F53BBA|XN--HLCJ6AYA9ESC7A|XN--11B5BS3A9AJ6G|XN--MGBERP4A5D4AR|XN--XKC2DL3A5EE0H|XN--80AKHBYKNJ4F|XN--XKC2AL3HYE2A|XN--LGBBAT1AD8J|XN--MGBC0A9AZCG|XN--9T4B11YI5A|XN--MGBAAM7A8H|XN--MGBAYH7GPA|XN--MGBBH1A71E|XN--FPCRJ9C3D|XN--FZC2C9E2C|XN--YFRO4I67O|XN--YGBI2AMMX|XN--3E0B707E|XN--JXALPDLP|XN--KGBECHTV|XN--OGBPF8FL|XN--0ZWM56D|XN--45BRJ9C|XN--80AO21A|XN--DEBA0AD|XN--G6W251D|XN--GECRJ9C|XN--H2BRJ9C|XN--J6W193G|XN--KPRW13D|XN--KPRY57D|XN--PGBS0DH|XN--S9BRJ9C|XN--90A3AC|XN--FIQS8S|XN--FIQZ9S|XN--O3CW4H|XN--WGBH1C|XN--WGBL6A|XN--ZCKZAH|XN--P1AI|XN--NGBC5AZD|XN--80ASEHDB|XN--80ASWG|XN--UNUP4Y|MUSEUM|TRAVEL|AERO|ARPA|ASIA|COOP|INFO|JOBS|MOBI|BIZ|CAT|COM|EDU|GOV|INT|MIL|NET|ORG|PRO|TEL|XXX|AC|AD|AE|AF|AG|AI|AL|AM|AN|AO|AQ|AR|AS|AT|AU|AW|AX|AZ|BA|BB|BD|BE|BF|BG|BH|BI|BJ|BM|BN|BO|BR|BS|BT|BV|BW|BY|BZ|CA|CC|CD|CF|CG|CH|CI|CK|CL|CM|CN|CO|CR|CU|CV|CW|CX|CY|CZ|DE|DJ|DK|DM|DO|DZ|EC|EE|EG|ER|ES|ET|EU|FI|FJ|FK|FM|FO|FR|GA|GB|GD|GE|GF|GG|GH|GI|GL|GM|GN|GP|GQ|GR|GS|GT|GU|GW|GY|HK|HM|HN|HR|HT|HU|ID|IE|IL|IM|IN|IO|IQ|IR|IS|IT|JE|JM|JO|JP|KE|KG|KH|KI|KM|KN|KP|KR|KW|KY|KZ|LA|LB|LC|LI|LK|LR|LS|LT|LU|LV|LY|MA|MC|MD|ME|MG|MH|MK|ML|MM|MN|MO|MP|MQ|MR|MS|MT|MU|MV|MW|MX|MY|MZ|NA|NC|NE|NF|NG|NI|NL|NO|NP|NR|NU|NZ|OM|PA|PE|PF|PG|PH|PK|PL|PM|PN|PR|PS|PT|PW|PY|QA|RE|RO|RS|RU|RW|SA|SB|SC|SD|SE|SG|SH|SI|SJ|SK|SL|SM|SN|SO|SR|ST|SU|SV|SX|SY|SZ|TC|TD|TF|TG|TH|TJ|TK|TL|TM|TN|TO|TP|TR|TT|TV|TW|TZ|UA|UG|UK|US|UY|UZ|VA|VC|VE|VG|VI|VN|VU|WF|WS|YE|YT|ZA|ZM|ZW)/i,
  'domain':/([A-Z0-9_\-\.]+\.(?:XN--CLCHC0EA0B2G2A9GCD|XN--HGBK6AJ7F53BBA|XN--HLCJ6AYA9ESC7A|XN--11B5BS3A9AJ6G|XN--MGBERP4A5D4AR|XN--XKC2DL3A5EE0H|XN--80AKHBYKNJ4F|XN--XKC2AL3HYE2A|XN--LGBBAT1AD8J|XN--MGBC0A9AZCG|XN--9T4B11YI5A|XN--MGBAAM7A8H|XN--MGBAYH7GPA|XN--MGBBH1A71E|XN--FPCRJ9C3D|XN--FZC2C9E2C|XN--YFRO4I67O|XN--YGBI2AMMX|XN--3E0B707E|XN--JXALPDLP|XN--KGBECHTV|XN--OGBPF8FL|XN--0ZWM56D|XN--45BRJ9C|XN--80AO21A|XN--DEBA0AD|XN--G6W251D|XN--GECRJ9C|XN--H2BRJ9C|XN--J6W193G|XN--KPRW13D|XN--KPRY57D|XN--PGBS0DH|XN--S9BRJ9C|XN--90A3AC|XN--FIQS8S|XN--FIQZ9S|XN--O3CW4H|XN--WGBH1C|XN--WGBL6A|XN--ZCKZAH|XN--P1AI|XN--NGBC5AZD|XN--80ASEHDB|XN--80ASWG|XN--UNUP4Y|MUSEUM|TRAVEL|AERO|ARPA|ASIA|COOP|INFO|JOBS|MOBI|BIZ|CAT|COM|EDU|GOV|INT|MIL|NET|ORG|PRO|TEL|XXX|AC|AD|AE|AF|AG|AI|AL|AM|AN|AO|AQ|AR|AS|AT|AU|AW|AX|AZ|BA|BB|BD|BE|BF|BG|BH|BI|BJ|BM|BN|BO|BR|BS|BT|BV|BW|BY|BZ|CA|CC|CD|CF|CG|CH|CI|CK|CL|CM|CN|CO|CR|CU|CV|CW|CX|CY|CZ|DE|DJ|DK|DM|DO|DZ|EC|EE|EG|ER|ES|ET|EU|FI|FJ|FK|FM|FO|FR|GA|GB|GD|GE|GF|GG|GH|GI|GL|GM|GN|GP|GQ|GR|GS|GT|GU|GW|GY|HK|HM|HN|HR|HT|HU|ID|IE|IL|IM|IN|IO|IQ|IR|IS|IT|JE|JM|JO|JP|KE|KG|KH|KI|KM|KN|KP|KR|KW|KY|KZ|LA|LB|LC|LI|LK|LR|LS|LT|LU|LV|LY|MA|MC|MD|ME|MG|MH|MK|ML|MM|MN|MO|MP|MQ|MR|MS|MT|MU|MV|MW|MX|MY|MZ|NA|NC|NE|NF|NG|NI|NL|NO|NP|NR|NU|NZ|OM|PA|PE|PF|PG|PH|PK|PL|PM|PN|PR|PS|PT|PW|PY|QA|RE|RO|RS|RU|RW|SA|SB|SC|SD|SE|SG|SH|SI|SJ|SK|SL|SM|SN|SO|SR|ST|SU|SV|SX|SY|SZ|TC|TD|TF|TG|TH|TJ|TK|TL|TM|TN|TO|TP|TR|TT|TV|TW|TZ|UA|UG|UK|US|UY|UZ|VA|VC|VE|VG|VI|VN|VU|WF|WS|YE|YT|ZA|ZM|ZW))(?:\s|$)/i,
  'sha256':/(?:[A-F]|[0-9]){64}/i,
  'sha1':/(?:[A-F]|[0-9]){40}/i,
  'md5':/(?:[A-F]|[0-9]){32}/i}

CodeMirror.defineMode('iocdb-extract', function(){
  return {
    startState: function() {return {tsvcolsfound:0, intsv:false, inheader:false}},
    token: function(stream, state){

      style = ''

      if (stream.sol()){
        //look for potential tsv
        line = stream.match(/^.*/, false)
        columns = line[0].split('\t')
        if (columns.length > 1){
          if (columns.length == state.tsvcolsfound){
            state.intsv = true
            state.inheader = false
          }else{
            state.tsvcolsfound = columns.length
            state.intsv = false
            state.inheader = true
          }
        }else{
          state.tsvcolsfound = 0
          state.intsv = false
          state.inheader = false
        }
      }

      if (state.intsv){
        style = 'intsv '+style
      }

      if (state.inheader){
        style = 'inheader '+style
      }

      //regex
      escaped = stream.eat('\\');
      for (indicator in regex){
        groups = stream.match(regex[indicator]);
        if (groups !== null){ //match
          if (escaped !== undefined){
            return style;
          }else{
            if (groups.length > 1) //if trailing whitespace
              stream.backUp(groups[0].length - groups[1].length)
            return indicator+" indicator "+style;
          }
        }
      }

      //no match
      stream.next();
      return style;
    }
  };
});

module.controller('DocumentNewEndpoint', ['$scope', '$location', 'sidebar',
                                          'Document', 'Rumor', 'TTP',
  function ($scope, $location, sidebar, Document, Rumor, TTP){
    $scope.view_template = 'document-new.html'
    sidebar.url = null

    docpage = {
      doc : Document.init({
        investigator:$scope.user.id,
        tlp:'white',
        text:'\
\n\
## use regex\'s to extract rumors\n\
1.1.1.1 <-- rumor with observable ip:\\1.1.1.1 interpreted from regex.\n\
\n\
## extract rumors from rows of tab separated tables:\n\
observable	ttp.category	valid	description\n\
1.1.1.1	exfiltration site	2012-10-10	bad stuff went here\n\
2.2.2.2	c2	2012-11-11	cnc came from here\n\
\n\
## extract associations from rows of tab separated tables:\n\
left	right	variety	valid\n\
1.1.1.1	evil.example.com	resolves to	2012-10-10\n\
2.2.2.2	evil2.example.com	connects to	2012-11-11\n\
\n\
## escape indicators with "\\":\n\
evil.example.com\n\
\\benign.example.com\n\
'}),
      default_edit : {
          rumor: Rumor.init({
            valid: new Date(Date.now()).toISOString().slice(0,19)
          })
      },
      selected_edit : {
          rumor: Rumor.init(),
      },
      rumors : [],
      associations : [],
      extracted_observables: {},
      tab: 'text'
    }

    if ($scope.message.text)
      docpage.doc.text = $scope.message.text
      $scope.message.text = undefined
    if ($scope.message.name)
      docpage.doc.name = $scope.message.name
      $scope.message.name = undefined
    idx = $scope.documents.push(docpage) - 1
    $location.path('/document/'+idx)
}])

//TODO: combine this with DocumentPage
module.controller('DocumentEndpoint', ['$scope', '$location', 'sidebar',
  function ($scope, $location, sidebar){

    //validate route
    if (isNaN($scope.document_index)){
      $location.path('/documents/new')
      return
    }else if ($scope.documents.length <= $scope.document_index){
      $location.path('/documents/new')
      return
    }

    sidebar.url = null
    $scope.view_template = 'document.html'
}])

module.controller('DocumentText', ['$scope', '$location', 'sidebar', 
  function ($scope, $location, sidebar){

    //$scope.select_tokens = function (instance, selection){
    //  //when selection changes update selected list

    //  selectedtokens = {} //selectedtokens = set()
    //  doc = $scope.cm.getDoc()
    //  startindex = doc.indexFromPos(selection.anchor)
    //  endindex = doc.indexFromPos(selection.head)
    //  if (startindex > endindex){ //startindex < endindex
    //    temp = startindex
    //    startindex = endindex
    //    endindex = temp
    //  }
  
    //  for (i=startindex; i<=endindex; i+=1){
  
    //    //get all tokens at that index
    //    pos = doc.posFromIndex(i)
    //    token = $scope.cm.getTokenAt(pos)
    //    if (token.type === null) { continue }
    //    types = token.type.split(' ')
  
    //    //ignore selections in tables
    //    //if (types.indexOf('intsv') != -1) { continue }
    //    //if (types.indexOf('inheader') != -1) { continue }
  
    //    //add indicators at index to selection
    //    indicatorindex = types.indexOf('indicator')
    //    if (indicatorindex != -1){
    //      type = types[indicatorindex-1]
    //      selectedtokens[[type,token.string]] = [type,token.string]
    //    }
    //  }
  
    //  //selectedlist = selectedtokens.values()
    //  $scope.document.selected.indicators = Object.keys(selectedtokens)
    //                                     .map(function(key){
    //    return selectedtokens[key];
    //  });

    //  $scope.$apply() //apply scope of selection display
    //}

  }
])

module.controller('DocumentPage', ['$scope', 'sidebar', 'Rumor', 'TTP',
  function ($scope, sidebar, Rumor, TTP){
    
    //TODO: define here and pass to aside service
    //don't use app scope
    //$scope.document.rumors = []
    //$scope.document.associations = []

    //aside.open({
    //  templateUrl: 'document-aside.html',
    //  controller: 'DocumentAside'
    //})
    $scope.sidebar.url = 'document-aside.html'

    $scope.columns = {
      'associations':['valid','left','variety','right', 'description']
    }

    $scope.selected = {}
    sidebar.selected = $scope.selected

    $scope.apply = function () {

      if ($scope.document.selected_edit.valid_date){
        if (!$scope.document.selected_edit.valid_time)
          $scope.document.selected_edit.valid_time = '00:00:00'
        $scope.document.selected_edit.valid = 
          $scope.document.selected_edit.valid_date + 'T' +
          $scope.document.selected_edit.valid_time
      }

      for (key in $scope.document.extracted_observables) {
        rumor = $scope.document.extracted_observables[key]
        if (rumor.selected){
          for (inputkey in $scope.document.selected_edit){
              if (inputkey == 'rumor')
                continue
              if (inputkey == 'document')
                continue
              value = $scope.document.selected_edit[inputkey]
              if (!value)
                continue
              if (inputkey == 'ttp'){
                if (!rumor.ttp)
                  rumor.ttp = {}
                for (ttpkey in value){
                  ttpvalue = value[ttpkey]
                  if (ttpvalue)
                    rumor.ttp[ttpkey] = ttpvalue
                }
                continue
              }
              //else
              rumor[inputkey] = value
          }
          rumor.selected = false
        }
      }
      $scope.document.selected_edit.rumor = Rumor.init()
      $scope.document.selected_edit.ttp = undefined
      $scope.document.selected_edit.actor = undefined
      $scope.document.selected_edit.campaign = undefined
    };

    $scope.clear = function() {
      for (observable in $scope.document.extracted_observables){
        rumor = $scope.document.extracted_observables[observable]
        if (rumor.selected){
          rumor.valid = undefined
          rumor.description = undefined
          rumor.ttp = undefined
          rumor.actor = undefined
          rumor.campaign = undefined
          rumor.selected = false
        }
      }
    };

    extract = function () {

      $scope.document.rumors.length = 0 //$scope.document.rumors = []
      $scope.document.associations.length = 0 //$scope.document.associations = []

      //set all extracted observables rumors to stale
      for (obs in $scope.document.extracted_observables)
        $scope.document.extracted_observables[obs].stale = true

      CodeMirror.runMode($scope.document.doc.text, 'iocdb-extract', _extract);
      _extract() //parse last line (if no newline)

      //delete all stale rumors
      for (obs in $scope.document.extracted_observables){
        if ($scope.document.extracted_observables[obs].stale)
          delete $scope.document.extracted_observables[obs]
      }
    };

    state = {column:'', row:[], header:[], new_object:null }
    _extract = function (token, style, line, start) {
      if (line == undefined){ //if newline
        if (state.row.length > 0){ //parse tsv

          //terminate last column
          state.row.push(state.column) 
          state.column = ''

          if (state.header.length == state.row.length){ //if data (not header)

            //zip row to header
            obj = {}
            for (i=0; i<state.row.length; i+=1){
              data = state.row[i]
              foo = state.header[i]

              //TODO: recursive for the more general case
              path = foo.split('.')
              if (path.length > 1){
                if (!(path[0] in obj))
                  obj[path[0]] = {}
                obj[path[0]][path[1]] = data
              }else{
                obj[path[0]] = data
              }
            }
            state.row.length = 0 //state.row = []

            //if rumor data
            if (state.header.indexOf('observable') != -1)
              $scope.document.rumors.push(obj)
            else
              $scope.document.associations.push(obj)

          }else{ //if header row
            state.header = angular.copy(state.row)
            state.row.length = 0 //state.row = []
          }

        }else{ //if not tsv
          state.header = []
        }
      }

      if (!style) return;
      style = style.split(' ');
      observable_idx = style.indexOf('indicator')
      intsv = style.indexOf('intsv') != -1
      inheader = style.indexOf('inheader') != -1
        
      if (observable_idx != -1) { //if token is an observable
        variety = style[observable_idx-1]
        observable = {'variety':variety, 'value':token}
        if (intsv){
          field = null
          if (state.header.length > state.row.length){
            field = state.header[state.row.length]
          }
          //TODO: find a better way to know when to jsonify an object
          //if field in ...
          if (['left', 'right', 'observable'].indexOf(field) != -1){
            state.column = observable
          }else{
            state.column += JSON.stringify(observable)
          }
        }else{ //not in tsv
          key = JSON.stringify(observable)
          if (key in $scope.document.extracted_observables){
            $scope.document.extracted_observables[key].stale = false
          }else{
            $scope.document.extracted_observables[key] = 
              Rumor.init({observable:observable, stale:false})
          }
        }
      }else{ //token not an observable
        if (intsv || inheader){
          if (token == '\t'){
            state.row.push(state.column)
            state.column = ''
          }else{
            state.column += token
          }
        }
      }
    }
            
    $scope.$watch('document.tab', function (tab) { 
      extract()
    })

  }
])

module.controller('DocumentAside', ['$scope', 'sidebar', '$http', '$modal',
  'Document', 'Rumor', 'TTP',
  function ($scope, sidebar, $http, $modal, Document, Rumor, TTP){

    $scope.document.default_edit.document = $scope.document.doc
    $scope.document.selected_edit.document = $scope.document.doc
    $scope.input = $scope.document.default_edit
    $scope.Document = Document
    $scope.Rumor = Rumor
    $scope.TTP = TTP
    $scope.sidebar = sidebar

    $scope.$watch('sidebar.selected.count', function () {
      if ($scope.sidebar.selected.count)
        $scope.input = $scope.document.selected_edit
      else
        $scope.input = $scope.document.default_edit
    });

    $scope.$watch('input.rumor.valid', function () {
      $scope.input.valid = $scope.input.rumor.valid
    });
    $scope.$watch('input.rumor.description', function () {
      $scope.input.description = $scope.input.rumor.description
    });

    $scope.action = {}
    $scope.add_action = function() {
      if (!$scope.input.ttp)
        $scope.input.ttp = {}
      if (!$scope.input.ttp.actions)
        $scope.input.ttp.actions = []
      $scope.input.ttp.actions.push($scope.action)
      $scope.action = {}
    };

    //watch for empty objects
    $scope.$watch('input', function () {
      if ($scope.input.ttp){
        if ($scope.input.ttp.actions){
          if ($scope.input.ttp.actions.length == 0)
            $scope.input.ttp.actions = undefined
        }
        if (!$scope.input.ttp.category)
          $scope.input.ttp.category = undefined
        if (!$scope.input.ttp.malware)
          $scope.input.ttp.malware = undefined
        if (JSON.stringify($scope.input.ttp) == '{}')
          $scope.input.ttp = undefined
      }
      if ($scope.input.actor){
        if (!$scope.input.actor.name)
          $scope.input.actor.name = undefined
        if (JSON.stringify($scope.input.actor) == '{}')
          $scope.input.actor = undefined
      }
      if ($scope.input.campaign){
        if (!$scope.input.campaign.name)
          $scope.input.campaign.name = undefined
        if (JSON.stringify($scope.input.campaign) == '{}')
          $scope.input.campaign = undefined
      }

    }, true);

    $scope.rem_action = function(idx) {
      $scope.input.ttp.actions.splice(idx,1)
    };

    $scope.document.default_edit.rumor.valid_date = 
      $scope.document.default_edit.rumor.valid.slice(0,10)
    $scope.document.default_edit.rumor.valid_time = 
      $scope.document.default_edit.rumor.valid.slice(11)
    _catdef = function () {
      if (!$scope.document.default_edit.rumor.valid_time)
        $scope.document.default_edit.rumor.valid_time = '00:00:00'
      $scope.document.default_edit.rumor.valid =
        $scope.document.default_edit.rumor.valid_date + 'T' +
        $scope.document.default_edit.rumor.valid_time
    };
    $scope.$watch('document.default_edit.rumor.valid_date', _catdef) 
    $scope.$watch('document.default_edit.rumor.valid_time', _catdef) 

    $scope.$watch('input.document.category', function() {
      if ($scope.input.document) //called once before init
        $scope.input.document.source = $scope.input.document.category
    });

    $scope.upload_document = function () {
  
      if (!$scope.document_form.$valid){
          modal = alert('fix invalid document form. TODO: use a better widget')
          return
      }
  
      modal = $modal.open({
        templateUrl: 'upload-doc.html',
        scope: $scope
      })
  
      modal.result.then(function (close_document_on_success) {
        tx = angular.copy($scope.document.rumors)
        for (key in $scope.document.extracted_observables){
          rumor = $scope.document.extracted_observables[key]
          tx.push(rumor)
        }

        for (i=0;i<tx.length;++i){
          rumor = tx[i]

          //apply default value
          for (key in $scope.document.default_edit){
            value = $scope.document.default_edit[key]
            if (value && !rumor[key])
              rumor[key] = value
          }

          //TODO: enable document text storage
          docclone = angular.copy($scope.document.doc)
          docclone.text = undefined

          clone = {
            observable:rumor.observable,
            document:docclone,
            valid:rumor.valid,
            description:rumor.description,
            ttp:rumor.ttp,
            actor:rumor.actor,
            campaign:rumor.campaign
          }

          tx[i] = clone
        }

        $http.post('/api/iocdb/rumors', tx).
            success(function(){
              console.log('rumor post success')
              if (close_document_on_success)
                $scope.rem_document($scope.document_index)
            }).
            error(function (data, status, headers, config){
              lines = data.split('\n')
              summary = lines[lines.length - 4]
              alert('post failed. server error:\n'+summary);
              console.log('post failed');
              console.log(data)
              console.log(status);
              console.log(headers());
              console.log(config);
            });
      })
    }

}])
