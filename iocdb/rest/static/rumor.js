//TODO: compile Models from domain code
module = angular.module('rumor', [])

module.factory('Rumor', [function() {
  return {
    init: function (kwargs) {
      if (!kwargs)
        kwargs = {}
      return {
        observable: kwargs.observable,
        document: kwargs.document,
        valid: kwargs.valid,
        description: kwargs.description,
        ttp: kwargs.ttp,
        actor: kwargs.actor,
        campaign: kwargs.campaign
      }}
  }}]);

module.factory('Observable', [function() {
  return {
    variety: ['ip','network','asn','url','email',
              'domain','sha256','sha1','md5'],
    init: function () {
      return {
      }}
  }}]);

module.factory('Actor', [function() {
  return {
    init: function () {
      return {
      }}
  }}]);

module.factory('Campaign', [function() {
  return {
    init: function () {
      return {
      }}
  }}]);

module.factory('TTP', function() {
  return {
    category:[
      'malicious host', 'bot', 'exfiltration site', 'c2',
      'proxy', 'malware distributor', 'exploit server',
      'malware', 'sinkhole', 'malicious email address'],
    actions:{
      "hacking": {
         "variety": [
           "Abuse of functionality",
           "Brute force",
           "Buffer overflow",
           "Cache poisoning",
           "Session prediction",
           "CSRF",
           "XSS",
           "Cryptanalysis",
           "DoS",
           "Footprinting",
           "Forced browsing",
           "Format string attack",
           "Fuzz testing",
           "HTTP request smuggling",
           "HTTP request splitting",
           "HTTP response smuggling",
           "HTTP Response Splitting",
           "Integer overflows",
           "LDAP injection",
           "Mail command injection",
           "MitM",
           "Null byte injection",
           "Offline cracking",
           "OS commanding",
           "Path traversal",
           "RFI",
           "Reverse engineering",
           "Routing detour",
           "Session fixation",
           "Session replay",
           "Soap array abuse",
           "Special element injection",
           "SQLi",
           "SSI injection",
           "URL redirector abuse",
           "Use of backdoor or C2",
           "Use of stolen creds",
           "XML attribute blowup",
           "XML entity expansion",
           "XML external entities",
           "XML injection",
           "XPath injection",
           "XQuery injection",
           "Virtual machine escape",
           "Unknown",
           "Other"
         ],
         "vector": [
           "3rd party desktop",
           "Backdoor or C2",
           "Desktop sharing",
           "Physical access",
           "Command shell",
           "Partner",
           "VPN",
           "Web application",
           "Unknown",
           "Other"
         ]
       },
       "malware": {
         "variety": [
           "Adware",
           "Backdoor",
           "Brute force",
           "Capture app data",
           "Capture stored data",
           "Client-side attack",
           "Click fraud",
           "C2",
           "Destroy data",
           "Disable controls",
           "DoS",
           "Downloader",
           "Exploit vuln",
           "Export data",
           "Packet sniffer",
           "Password dumper",
           "Ram scraper",
           "Ransomware",
           "Rootkit",
           "Scan network",
           "Spam",
           "Spyware/Keylogger",
           "SQL injection",
           "Adminware",
           "Worm",
           "Unknown",
           "Other"
         ],
         "vector": [
           "Direct install",
           "Download by malware",
           "Email autoexecute",
           "Email link",
           "Email attachment",
           "Instant messaging",
           "Network propagation",
           "Remote injection",
           "Removable media",
           "Web drive-by",
           "Web download",
           "Unknown",
           "Other"
         ]
       },
       "misuse": {
        "variety": [
           "Knowledge abuse",
           "Privilege abuse",
           "Embezzlement",
           "Data mishandling",
           "Email misuse",
           "Net misuse",
           "Illicit content",
           "Unapproved workaround",
           "Unapproved hardware",
           "Unapproved software",
           "Unknown",
           "Other"
         ],
         "vector": [
           "Physical access",
           "LAN access",
           "Remote access",
           "Non-corporate",
           "Unknown",
           "Other"
         ]
       },
       "physical": {
         "location": [
           "Partner facility",
           "Partner vehicle",
           "Personal residence",
           "Personal vehicle",
           "Public facility",
           "Public vehicle",
           "Victim secure area",
           "Victim work area",
           "Victim public area",
           "Victim grounds",
           "Unknown",
           "Other"
         ],
         "variety": [
           "Assault",
           "Sabotage",
           "Snooping",
           "Surveillance",
           "Tampering",
           "Theft",
           "Wiretapping",
           "Connection",
           "Unknown",
           "Other"
         ],
         "vector": [
           "Privileged access",
           "Visitor privileges",
           "Bypassed controls",
           "Disabled controls",
           "Uncontrolled location",
           "Unknown",
           "Other"
         ]
       },
       "social": {
         "target": [
           "Auditor",
           "Call center",
           "Cashier",
           "Customer",
           "End-user",
           "Executive",
           "Finance",
           "Former employee",
           "Helpdesk",
           "Human resources",
           "Maintenance",
           "Manager",
           "Partner",
           "Guard",
           "Developer",
           "System admin",
           "Unknown",
           "Other"
         ],
         "variety": [
           "Baiting",
           "Bribery",
           "Elicitation",
           "Extortion",
           "Forgery",
           "Influence",
           "Scam",
           "Phishing",
           "Pretexting",
           "Propaganda",
           "Spam",
           "Unknown",
           "Other"
         ],
         "vector": [
           "Documents",
           "Email",
           "In-person",
           "IM",
           "Phone",
           "Removable media",
           "SMS",
           "Social media",
           "Software",
           "Website",
           "Unknown",
           "Other"
         ]
    }},

    init: function () {
      return {
        actions:[]
      }}
  }});
