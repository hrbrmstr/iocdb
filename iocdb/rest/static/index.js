app = angular.module('index', ['ui.bootstrap', 'ui.codemirror', 'localdb',
                               'document', 'rumor', 'search', 
                               'ngRoute']).

  config(['$routeProvider', function($routeProvider) {
    $routeProvider.
      when('/document/:docIndex', {
        templateUrl: 'static/document.html',
        controller: 'DocumentEndpoint'
      }).
      //TODO: make tab endpoints # targets
      when('/documents/new', {
        templateUrl: 'static/document.html',
        controller: 'DocumentNewEndpoint'
      }).
      when('/search/:searchIndex', {
        templateUrl: 'static/search.html',
        controller: 'SearchResults'
      }).
      when('/searches/new', {
        templateUrl: 'static/search.html',
        controller: 'SearchNew'
      }).
      when('/status', {templateUrl: 'static/status.html'}).
      when('/', {templateUrl: 'helloworld.html'})
  }])

//TODO: name this "aside" and make it more like "modal"
app.factory('sidebar', function() {
  return {
    url:null,
    is_valid:function () {true},
  }
})

app.directive('vzTable', function() {
    return {
      restrict: 'E',
      scope: {
        rows: '=',
        keyedRows: '=',
        columns: '=',
        selected: '=',
        default: '='
      },
      templateUrl: 'table.html',
    }
  });

app.controller('Table', ['$scope',
  function($scope) {
  
    if (!$scope.columns){ //TODO: remove
      $scope.columns = ['valid', 'observable', 'description',// 'document',
                        'ttp', 'actor', 'campaign']
    }

    if (!$scope.selected)
      $scope.selected = {hide:true}
    $scope.selected.count = 0

    if (!$scope.rows)
      $scope.rows = []

    if (!$scope.keyedRows)
      $scope.keyedRows = {}

    $scope.$watch('selected_all', function () {
      for (i=0;i<$scope.rows.length;++i){
        row = $scope.rows[i]
        row.selected = $scope.selected_all
      }
      for (key in $scope.keyedRows){
        row = $scope.keyedRows[key]
        row.selected = $scope.selected_all
      }
    });

    for (i=0;i<$scope.rows.length;++i){
      $scope.$watch('rows['+i+'].selected', function (newval, oldval) {
        if (newval && !oldval)
          $scope.selected.count += 1
        else if (oldval && !newval)
          $scope.selected.count -= 1
        if ($scope.selected.count == 0)
          $scope.selected_all = false
    })};
    for (key in $scope.keyedRows){
      $scope.$watch('keyedRows[\''+key+'\'].selected', function (newval, oldval) {
        if (newval && !oldval)
          $scope.selected.count += 1
        else if (oldval && !newval)
          $scope.selected.count -= 1
        if ($scope.selected.count == 0)
          $scope.selected_all = false
    })};
}]);

//TODO: make this ctrl just for the navaside
app.controller('IndexCtrl', ['$scope', '$routeParams', '$modal', '$log', 
                             '$location', 'localdb', '$window', '$http', 
                             'sidebar',
    function ($scope, $routeParams, $modal, $log, $location, localdb, 
              $window, $http, sidebar) {

      $scope.message = {}
      $scope.documents = []
      $scope.searches = []
      $scope.document_index = undefined
      $scope.search_index = undefined
      $scope.search_results = {}
      $scope.user = {id:undefined}
      //TODO: args for document aside service
      $scope.sidebar = sidebar

      window.addEventListener('message', function(event) {
        $scope.message = event.data
        if ($scope.message == 'listening')
          return //message sent by this window, ignore
        $location.path('/documents/new')
      }, false);

      if (window.opener)
        window.opener.postMessage('listening', '*')

      //routing logic
      $scope.$on('$routeChangeSuccess', function(e, current, previous){
        //TODO: provide pages as services
        $scope.document_index = parseInt($routeParams.docIndex)
        if ($scope.document_index !== undefined)
          $scope.document = $scope.documents[$scope.document_index]
        $scope.search_index = parseInt($routeParams.searchIndex)
        if ($scope.search_index !== undefined)
          $scope.search = $scope.searches[$scope.search_index]
      });
  
      //generic model search fn
      $scope.get = function(model, query) {

        //ignore null query params
        for (field in query){
          if (!query[field])
            delete query[field]
        }

        search = {model:model, query:query}
        idx = $scope.searches.push(search) - 1
        loading = $modal.open({templateUrl: 'search-loading.html'})
        $http.get('/api/iocdb/'+model+'s', {params:query}).

          success(function (data){
            search.results = data
            $location.path('/search/'+idx)
            loading.dismiss()
          }).
  
          error(function (data, status, headers, config){
              lines = data.split('\n')
              summary = lines[lines.length - 4]
              alert('query failed\n'+summary);
              console.log('get failed. server error:\n');
              console.log(data);
              console.log(status);
              console.log(headers());
              console.log(config);
            });
      };
  
      $scope.open_login = function () {
    
        var modalInstance = $modal.open({
          templateUrl: 'login.html',
          controller: 'LoginCtrl',
          resolve: {
            user: function () { return $scope.user }
          }
        });
        
        modalInstance.result.then(function (user) {
          if (user.id){
            $scope.user.id = user.id
            localdb.set_user(user, function() {
              $log.error('error: failed to save user to localdb')
              $scope.open_login()
            })
          }
        }, function () {
        });
    
      };
    
      localdb.get_user(function(user){
        if(user)
          $scope.user.id = user.id
        else
          $scope.open_login()
        $scope.$apply()
      }, function() {
        $log.error('error: failed to get user from localdb')
        $scope.open_login()
        $scope.$apply()
      })
    
      $scope.rem_document = function (idx) {
        var modalInstance = $modal.open({
          templateUrl: 'remove_doc.html'
        })
    
        modalInstance.result.then(function () {
          $scope.documents.splice(idx,1)
          if ($scope.document_index == idx)
            $location.path('/')
          if ($scope.document_index > idx)
            $location.path('/document/'+($scope.document_index - 1))
        })
      }
    
      $scope.rem_search = function(idx) {
        var modalInstance = $modal.open({
          templateUrl: 'search_delete.html',
        })
    
        modalInstance.result.then(function () {
          $scope.searches.splice(idx, 1)
          if ($scope.search_index == idx)
            $location.path('/')
          if ($scope.search_index > idx)
            $location.path('/search/' + ($scope.search_index - 1))
        })
      }
}])

app.controller('LoginCtrl', function ($scope, $modalInstance, user) {

  $scope.user = {id:user.id}

  $scope.save = function () {
    $modalInstance.close($scope.user);
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
});

app.controller('SplashCtrl', function($scope, sidebar) {
  sidebar.url = null
})

