search = angular.module('search', [])

search.controller('SearchAside', ['$scope', 'sidebar', 'Document', 'Rumor',
                                  'TTP', 'Actor', 'Campaign', 'Observable',
  function ($scope, sidebar, Document, Rumor, TTP, Actor, Campaign,
            Observable){

    $scope.clear_input = function () {
      $scope.input = {
        'rumor':Rumor.init(),
        'document':Document.init(),
        'ttp':TTP.init(),
        'actor':Actor.init(),
        'campaign':Campaign.init()
      }
    };
    $scope.clear_input()

    $scope.Document = Document
    $scope.Rumor = Rumor
    $scope.TTP = TTP
    $scope.Observable = Observable
    $scope.sidebar = sidebar

    $scope.$watch('input.document.category', function() {
      if ($scope.input.document) //called once before init
        $scope.input.document.source = $scope.input.document.category
    });

    $scope.action = {}
    $scope.add_action = function() {
      $scope.input.ttp.actions.push($scope.action)
      $scope.action = {}
    };

    $scope.rem_action = function(idx) {
      $scope.input.ttp.actions.splice(idx,1)
    };

    $scope.cancel = function() {
        $scope.clear_input()
    };

  }]);

search.controller('SearchNew', ['$scope', '$location', 'sidebar', '$http',
                                '$modal',
  function ($scope, $location, sidebar, $http, $modal){
    $scope.view_template = 'search_new.html'
    sidebar.url = null

    $scope.rumor_input_fields = {
      'observable.varieties':'observable_variety',
      'observable.values':'observable_value',
      'document.sources':'document_source',
      'document.names':'document_name',
      'document.investigators':'document_investigator',
      'actor.names':'actor_name',
      'campaign.names':'campaign_name',
      'ttp.malware':'ttp_malware',
      'limit (TODO:implement paging)':'limit'
    }

    $scope.query = {
      'limit':100,
      'document_investigator':$scope.user.id
    }

}])

search.controller('SearchResults', ['$scope', '$location', 'sidebar', '$http',
                                    '$modal',
  function ($scope, $location, sidebar, $http, $modal){

    //validate route
    if (isNaN($scope.search_index)){
      $location.path('/searches/new')
      return
    }else if ($scope.searches.length <= $scope.search_index){
      $location.path('/searches/new')
      return
    }

    $scope.selected = {}

    $scope.view_template = 'search-page.html'
    sidebar.url = 'search-aside.html'
    sidebar.selected = $scope.selected

    sidebar.delete = function () {
      results = $scope.search.results[$scope.search.model+'s']
      uids = []
      for (i=0;i<results.length;++i){
        result = results[i]

        if (result.selected){
          result.selected = undefined
          uids.push(result.uid)
        }
      }
      
      loading = $modal.open({templateUrl: 'search-loading.html'})
      $http.delete('/rumors', {params:{'uids':uids}}).
          success(function(){
            loading.dismiss()
            console.log('rumor delete success')
          }).
          error(function (data, status, headers, config){
            loading.dismiss()
            lines = data.split('\n')
            summary = lines[lines.length - 4]
            alert('put failed. server error:\n'+summary);
            console.log('delete failed');
            console.log(data)
            console.log(status);
            console.log(headers());
            console.log(config);
            //reload data
            $scope.get($scope.search.model, $scope.search.query)
          });
    }

    sidebar.apply = function (input) {
      tx = []
      models = $scope.search.results[$scope.search.model+'s']
      for (i=0;i<models.length;++i){ //apply input to the models
        model = models[i]
        if (model.selected){
          model.selected = undefined
          tx.push(model)

          if (input.rumor){
            if (input.rumor.valid_date){
              if (!input.rumor.valid_time)
                input.rumor.valid_time = '00:00:00'
              model.valid = input.rumor.valid_date+'T'+input.rumor.valid_time
            }
            if (input.rumor.description)
              model.description = input.rumor.description
          }

          if (input.document){
            if (input.document.name)
              model.document.name = input.document.name
            if (input.document.category)
              model.document.category = input.document.category
            if (input.document.source)
              model.document.source = input.document.source
            if (input.document.tlp)
              model.document.tlp = input.document.tlp
            if (model.document.noforn != input.document.noforn)
              model.document.noforn = input.document.noforn
            if (model.document.nocomm != input.document.nocomm)
              model.document.nocomm = input.document.nocomm
          }
          
          if (input.observable){
            if (input.observable.variety){
              model.observable.variety = input.observable.variety
              model.observable.value = input.observable.value
            }
          }

          if (input.ttp){
            edit = false
            if (input.ttp.category){
              edit = true
              if (!model.ttp)
                model.ttp = {}
              model.ttp.category  = input.ttp.category
            }
            if (input.ttp.malware){
              edit = true
              if (!model.ttp)
                model.ttp = {}
              model.ttp.malware  = input.ttp.malware
            }
            if (input.ttp.actions.length){
              edit = true
              if (!model.ttp)
                model.ttp = {}
              model.ttp.actions  = input.ttp.actions
            }
            if (edit)
                model.ttp.uid = undefined
          }
          
          if (input.actor){
            if (input.actor.name)
              model.actor = input.actor
          }
          
          if (input.campaign){
            if (input.campaign.name)
              model.campaign = input.campaign
          }
        }
      }

      //put models
      if (tx.length){
        loading = $modal.open({templateUrl: 'search-loading.html'})
        $http.put('/rumors', tx).
            success(function(){
              loading.dismiss()
              console.log('rumor put success')
            }).
            error(function (data, status, headers, config){
              loading.dismiss()
              lines = data.split('\n')
              summary = lines[lines.length - 4]
              alert('put failed. server error:\n'+summary);
              console.log('put failed');
              console.log(data)
              console.log(status);
              console.log(headers());
              console.log(config);
              //reload data
              $scope.get($scope.search.model, $scope.search.query)
            });
      }
    };
}])
