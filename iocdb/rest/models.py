__author__ = 'abarilla'

import datetime

from passlib.hash import sha256_crypt
from sqlalchemy.orm import relationship

from iocdb.rest import db

TIMEOUT = 30


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.Unicode(128), unique=True)
    password_hash = db.Column(db.String(64))
    sso_id = db.Column(db.Unicode(128), nullable=True)
    us_citizen = db.Column(db.Boolean, default=True, nullable=False)
    commercial = db.Column(db.Boolean, default=True, nullable=False)
    admin = db.Column(db.Boolean, default=False, nullable=False)

    @property
    def password(self):
        raise(AttributeError('Password is not a readable attribute'))

    @password.setter
    def password(self, value):
        self.password_hash = sha256_crypt.encrypt(value)

    def verify_password(self, password):
        return sha256_crypt.verify(password, self.password_hash)

    def __repr__(self):
        return "<User(id='%s', username='%s', sso_id='%s'>" % (self.id, self.username, self.sso_id)


class LogonSession(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    logon_token = db.Column(db.Unicode(64), unique=True)
    authenticated_user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    active_user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    logon_time = db.Column(db.DateTime)
    logoff_time = db.Column(db.DateTime)
    update_time = db.Column(db.DateTime)
    client_ip = db.Column(db.String(39))

    active_user = relationship("User", foreign_keys=[active_user_id])
    authenticated_user = relationship("User", foreign_keys=[authenticated_user_id])

    @property
    def expiration_date(self):
        return self.update_time + datetime.timedelta(minutes=TIMEOUT)

    def validate_time(self):
        if self.logoff_time is not None:
            return False

        if self.expiration_date > datetime.datetime.utcnow():
            self.update_time = datetime.datetime.utcnow()
            db.session.commit()
            return True
        else:
            self.logoff_time = datetime.datetime.utcnow()
            db.session.commit()
            return False

    def __repr__(self):
        return "<LogonSession(id='%s', logon_token='%s', active_user_id='%s', authenticated_user_id='%s', " \
               "logon_time='%s', logoff_time='%s', update_time='%s'>" % (self.id, self.logon_token, self.active_user_id,
                                                                         self.authenticated_user_id, self.logon_time,
                                                                         self.logoff_time, self.update_time)
