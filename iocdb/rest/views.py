__author__ = 'Andrew Barilla <andrew.barilla@one.verizon.com>'

from flask import send_file

from iocdb.rest import app
from iocdb.rest.utils import dump_json
from iocdb.buildinfo import BuildInfo
import iocdb.config as config


@app.route('/')
def index():
    # TODO: not implemented in production
    return send_file('static/index.html')


@app.route('/build')
def get_build():
    return dump_json({'version': BuildInfo.get_version_detailed()})


@app.route('/config')
def get_config():
    return dump_json(config.Config())

