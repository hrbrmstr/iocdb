'''models types are used to bind domain models to encodings

#### provide accessors to self registered models:
```
>>> class Penguin(Model):
...     plural = 'penguins'
...     name = Member()
...     def __init__(self, name):
...         self.name = name
...     def __repr__(self):
...         return '<%s is a cool penguin.>' % self.name
...
>>> Penguin in Model
True
>>> Model['penguin']
<class 'lib.model.Penguin'>
>>> Model['penguins']
<class 'lib.model.Penguin'>
>>> class Goose(Model):
...     plural = 'geeses'
...     name = Member()
...     cool_friend = Member(model='Penguin')
...     def __init__(self, name, cool_friend):
...         self.name = name
...         self.cool_friend = cool_friend
...     def __repr__(self):
...         return '<%s is a goose friend of %s. %s>' \
                % (self.name, self.cool_friend.name, self.cool_friend)
...
>>> sorted([cls for cls in Model], key=lambda cls: cls.__name__)
[<class 'lib.model.Goose'>, <class 'lib.model.Penguin'>]
>>> Model['geeses']
<class 'lib.model.Goose'>

```
#### decorate models with member accessors:
```
#TODO:
#>>> member = Goose.get_member('cool_friend')
#
#>>> member.model
#<class '__main__.Penguin'>
#>>> member[Binding]
#

```
#### models have a version:
```
>>> class NewModel(Model):
...     version = 'new'
...
>>> class Goose(NewModel):
...     plural = 'geese'
...     name = Member()
...     def __init__(self, name, cool_friend):
...         self.name = name
...     def __repr__(self):
...         return '<%s is bad with names.>' % self.name
>>> gooseargs = {'name':'lucy', 'cool_friend':Penguin('flipper')}
>>> Model['goose'](**gooseargs)
<lucy is a goose friend of flipper. <flipper is a cool penguin.>>
>>> NewModel['goose'](**gooseargs)
<lucy is bad with names.>
>>> Model.get_version('new')
<class 'lib.model.NewModel'>

```
#### model object identity:
a cache is maintained of model objects and their unique keys.
initializing an object with a key collision will return the
cached object
```
>>> class ThereCanBeOnlyOne(Model):
...     plural = 'ones'
...     name = Member()
...     unique = ('name',)
...     def __init__(self, name):
...         self.name = name
...
>>> models = ModelRegister([ThereCanBeOnlyOne])
>>> one = ThereCanBeOnlyOne('highlander')
>>> two = ThereCanBeOnlyOne('highlander')
>>> one is two
True

```
'''

import logging
from collections import defaultdict

def load_tests(loader, tests, pattern):
    from doctest import DocTestSuite
    from unittest import TestSuite
    suite = TestSuite()
    suite.addTests(DocTestSuite())
    return suite

inspect = lambda:None
try:
    from sqlalchemy.inspection import inspect
except ImportError:
    pass #do nothing

class ModelRegister(type):
    '''self registering Models'''

    _versionregistry = {}

    def __init__(cls, name, bases, clsd):
        #TODO: better error or prevent
        #two registries contructed with same version
        #currently says "no attribute 'plural'"

        if cls.version not in cls._versionregistry: #initialize register
            cls._nameregistry = {}
            cls._versionregistry[cls.version] = cls

        else: #register model
            cls.modelname = cls.__name__.lower()
            cls._nameregistry[cls.modelname] = cls
            cls._nameregistry[cls.plural] = cls
            cls._nameregistry[cls.__name__] = cls

            #initialize per model registries
            cls._bindingregistry = defaultdict(lambda: [])
            cls._memberregistry = {}
            cls._uniquecache = defaultdict(lambda: None)

            #register member encoders
            for member_name, member in vars(cls).items():
                if isinstance(member, Member):

                    for binding in member.bindings:
                        if isinstance(binding, type): #TODO: ?
                            #init binding
                            binding = binding(**member.kwargs)
                        B = binding.__class__
                        cls._bindingregistry[B].append((member_name, binding))

                    cls._bindingregistry[None].append(member_name)
                    cls._memberregistry[member_name] = member.model

    def __iter__(cls):
        return iter(set(cls._nameregistry.values()))

    def __contains__(cls, model_name_or_class):
        if isinstance(model_name_or_class, str):
            return model_name_or_class in cls._nameregistry.keys()
        else:
            return model_name_or_class in cls._nameregistry.values()

    def __getitem__(cls, name):
        '''provides model access by name string or plural name string.
           plural string defined using model cls.plural.
           singular string defined as cls.__name__.lower() or cls.__name__.'''
        return cls._nameregistry[name]


    def __call__(cls, *args, **kwargs):
        '''check first for unique collision in cache'''

        #initialize candidate object (to check unique attributes)
        obj = None
        try:
            obj = type.__call__(cls, *args, **kwargs)
        except TypeError as e:
            logging.exception('type: %s, args: %s, kwargs: %s' %\
                              (type.__name__, args, kwargs))
            raise e

        if hasattr(cls, 'unique'):
            key = tuple(getattr(obj, mn) for mn in cls.unique)
            cached = cls._uniquecache[key]
            if cached:
                ##TODO: each sqla session needs it's own object
                ##handle this somewhere else
                ##should probably just be using the sqla cache
                #inspector = inspect(cached._base, raiseerr=False)
                #if inspector:
                #    inspector2 = inspect(obj._base, raiseerr=False)
                #    if inspector2 and inspector.session != inspector2.session:
                #        obj._collision = False
                #        return obj
                    
                if obj.dump_dict() != cached.dump_dict(): #cache collision
                    #TODO: extend models with proper collision accessors
                    cached._collision = obj
                obj = cached
            else:
                obj._collision = False
                cls._uniquecache[key] = obj

        return obj

    def get_version(cls, version_name):
        try:
            return cls._versionregistry[version_name]
        except KeyError:
            raise ValueError(
                'no version registered with name %s' % version_name)

#     def get_member(cls, member_name):
#         '''returns member assigned to member_name'''

#         try:
#             return cls._memberregistry[member_name]
#         except KeyError:
#             raise AttributeError('member name %s not registered in model %s'\
#                                   % (member_name, cls.__name__))

    #TODO: move this logic to the member and implement ^^
    def get_model(cls, member_name):
        '''returns model registered to member_name'''

        if member_name in cls._memberregistry:
            model_name = cls._memberregistry[member_name]
            if model_name is None:
                return None
            else:
                return Model.get_version(cls.version)[model_name]
        else:
            raise AttributeError(
                'member name %s not registered in model %s version %s'\
                                 % (member_name, cls.__name__, cls.version))

    def get_members(cls, Binding=None):
        '''returns all (member_name, binding) pairs of type Binding.
           if Binding is None, return list of member_names'''

        if Binding in cls._bindingregistry:
            return cls._bindingregistry[Binding]
        else:
            return []

    def clear_cache(cls):
        for v in cls._versionregistry.values():
            for M in v._nameregistry.values():
                M._uniquecache = defaultdict(lambda: None)

class Member:
    '''accessor to a member's model and encoder bindings. '''

    def __init__(self, *bindings, **kwargs):
        self.bindings = bindings
        self.kwargs = kwargs
        if 'model' in kwargs:
            self.model = kwargs['model']
        else:
            self.model = None

class Model(object):
    '''bind an model to a dict encoding'''

    __metaclass__ = ModelRegister
    version = None

    def dump_dict(self):
        d = {}
        for member_name in self.__class__.get_members():
            value = getattr(self, member_name)
            if hasattr(value, 'dump_dict'): #isinstance(value, Model)
                d[member_name] = value.dump_dict()
            elif value is not None:
                d[member_name] = value
        #TODO: special uid member
        if hasattr(self, '_base'):
            if self._base.uid:
                d['uid'] = self._base.uid

        return d
