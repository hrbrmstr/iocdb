'''utility library'''

def load_tests(loader, tests, pattern):
    from doctest import DocTestSuite
    from unittest import TestSuite
    suite = TestSuite()
    suite.addTests(DocTestSuite())
    return suite

import os
app_dir = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
data_dir = os.path.join(app_dir, 'data')

import os
import hashlib
import pickle
class Diff:
    '''
    a dict like container that is initialized with an identity.
    on iter, return values for keys in current container that are
    not in previous container with this identity.
    ```
    >>> diff = Diff('test')
    >>> diff['foo_a'] = 'bar_a'
    >>> diff['hello_a'] = 'world_a'
    >>> for value in sorted(diff.values()):
    ...     value
    ...
    'bar_a'
    'world_a'

    >>> diff['foo_a'] = 'bar_a'
    >>> diff['foo_b'] = 'bar_b'
    >>> diff['hello_a'] = 'world_a'
    >>> diff['hello_b'] = 'world_b'
    >>> for value in sorted(diff.values()):
    ...     value
    ...
    'bar_b'
    'world_b'

    ```
    tear down: clear test index
    ```
    >>> null = diff.clear()

    ```
    '''

    def __init__(self, identity, data_dir=data_dir):
        self.uid = hashlib.sha224(identity).hexdigest()
        self.data = {}
        self.data_dir = data_dir

    def __setitem__(self, key, value):
        self.data[key] = value

    def values(self):
        '''yield objects appended that are diff from last iter.
           last iter stored in file with name based on uid.'''

        filepath = self._filepath()
        index = set(self.data.keys())
        previndex = set()
        try:
            with open(filepath) as fp:
                previndex = pickle.load(fp)
        except IOError:
            pass #no previous index file found

        for key in index - previndex:
            yield self.data[key]

        with open(filepath, 'w') as fp:
            #dump index to file
            fp.seek(0)
            pickle.dump(index, fp)

    def clear(self):
        os.remove(self._filepath())

    def _filepath(self):
        return os.path.join(self.data_dir, 'diff-%s' % self.uid)

def recursive_merge(template, new):
    '''recursive dictionary update
       TODO: doctest'''

    for k, v in new.iteritems():
        if k in template and isinstance(v, dict):
            recursive_merge(template[k], v)
        else:
            template[k] = v

import logging
import sys
def import_modules(module_names, ignore_errors=False):
    '''returns list of modules imported by given import path'''

    modules = []
    for module_name in module_names:
        try:
            __import__(module_name)
        except StandardError as e:
            if ignore_errors:
                logging.exception(e)
                continue
            else:
                raise e
        module = sys.modules[module_name]
        if hasattr(module, '__all__'): #if package, load all modules in pkg
            for mn in module.__all__:
                fullname = '%s.%s' % (module_name, mn)
                try:
                    __import__(fullname)
                except StandardError as e:
                    if ignore_errors:
                        logging.exception(e)
                        continue
                    else:
                        raise e
                modules.append(sys.modules[fullname])
        else:
            modules.append(module)

    return modules

from collections import defaultdict
def nest(data, keys, sortkey=None):
    '''group data by key
    TODO: doctest'''

    groupedfn = lambda: defaultdict(groupedfn)
    grouped = groupedfn()

    #sort
    data = sorted(data, key=sortkey)

    #nest
    for value in data:

        groups = []
        for key in keys:
            groups.append(key(value))

        path = grouped
        parent = None
        group = None
        for group in groups:
            parent = path
            path = path[group]
        if isinstance(path, list):
            path.append(value)
        else:
            parent[group] = [value]

    return grouped

from datetime import datetime
def cli_datetime(arg):
    '''ArgumentParser helper type'''
    try:
        return datetime.strptime(arg, '%Y-%m-%d')
    except ValueError:
        return datetime.strptime(arg, '%Y-%m-%dT%H:%M:%S')

from argparse import Action
def store_dict(member_name):
    '''ArgumentParser helper Action.
       store arg dest, value in namespace.member_name dict'''

    class Store(Action):
        def __call__(self, parser, namespace, values, option_string=None):
            if hasattr(namespace, self.member_name):
                getattr(namespace, self.member_name)[self.dest] = values
            else:
                setattr(namespace, self.member_name, {self.dest:values})

    Store.member_name = member_name
    return Store

import os
import json
def load_config(default_config, data_dir=data_dir):
    '''merge config file (if found) with default config'''

    config = dict(default_config)

    try:
        with open(os.path.join(data_dir, 'config.json')) as fp:
            configfile = None
            try:
                configfile = json.load(fp)
            except ValueError:
                raise ValueError('could not parse json config file')
        recursive_merge(config, configfile)
    except IOError:
        pass #do nothing if config file not found

    return config

import time
class Timer(object):
    def __init__(self, verbose=False):
        self.verbose = verbose

    def __enter__(self):
        self.start = time.time()
        return self

    def __exit__(self, *args):
        self.end = time.time()
        self.secs = self.end - self.start
        self.msecs = self.secs * 1000  # millisecs
        if self.verbose:
            print 'elapsed time: %f ms' % self.msecs

if __name__ == '__main__':
    import doctest
    doctest.testmod()
