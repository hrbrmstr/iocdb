'''
encodings dump model objects or loads model objects.
encodings look like:
```
class Encoder:
    def dump(self, data):
        \'''encode model objects\'''
class Decoder:
    def load(self, [query]):
        \'''yields model objects\'''
```
```
>>> from model import Model, Member
>>> class Capitalize(Encoder):
...     def __init__(self):
...         super(Capitalize, self).__init__(bind_method='dump_cap')
...     def dump(self, data):
...         \'''returns generic encoding with capitalized members\'''
...         return super(Capitalize, self).dump(data)
...
>>> import copy
>>> class Capitalizable:
...     \'''models can implement bindings that are invoked by encodings\'''
...     def dump_cap(self):
...         \'''return a copy of this model with cap members\'''
...         cap = copy.deepcopy(self)
...         for member_name, binding in self.__class__.get_members(Capitalized):
...             value = getattr(cap, member_name)
...             if binding.title: setattr(cap, member_name, value.title())
...             else: setattr(cap, member_name, value.upper())
...         return cap
...
>>> class Capitalized:
...     \'''members can contain mappings that are handled by bindings\'''
...     def __init__(self, title=False):
...         self.title = title
...     def __repr__(self):
...         return '<capitalized member, title=%s>' % self.title
...
>>> class Student(Model, Capitalizable):
...     plural = 'students'
...     name = Member(Capitalized(title=True))
...     lunch = Member()
...     def __init__(self, name, lunch):
...         self.name = name
...         self.lunch = lunch
...     def __repr__(self):
...         return '<%s has a %s for lunch>' % (self.name, self.lunch)
...

#TODO:
#>>> models.get_member(Student, 'name').get_binding(Capitalized)
#[('name', <capitalized member, title=True>)]
>>> fred = Student(name='fred smith', lunch='ham sandwich')
>>> Encoder().dump(fred)
{'student': '<fred smith has a ham sandwich for lunch>'}
>>> Capitalize().dump(fred)
{'student': <Fred Smith has a ham sandwich for lunch>}

```
'''

from collections import defaultdict

from model import Model

def load_tests(loader, tests, pattern):

    from doctest import DocTestSuite
    from unittest import TestSuite
    suite = TestSuite()

    #### doctest setUp
    from model import Member
    class SomeModel(Model):
        version = 'some'

    class Penguin(SomeModel):
        plural = 'penguins'
        name = Member()
        def __init__(self, name):
            self.name = name
        def __repr__(self):
            return '<%s is a cool penguin.>' % self.name

    class Goose(SomeModel):
        plural = 'geeses'
        name = Member()
        cool_friend = Member(model='Penguin')
        def __init__(self, name, cool_friend):
            self.name = name
            self.cool_friend = cool_friend
        def __repr__(self):
            return '<%s is a goose friend of %s. %s>' \
                % (self.name, self.cool_friend.name, self.cool_friend)

    globs = dict(globals())
    globs.update({'Penguin':Penguin, 'Goose':Goose})
    suite.addTests(DocTestSuite(globs=globs))
    return suite

class Encoder(object):
    '''
    generic encoder features
    '''

    def __init__(self, bind_method='__repr__', *bind_args):
        '''
        bind_method: name of model method which binds model to encoding
        bind_args: passed to bind method
        '''
        self.bind_method = bind_method
        self.bind_args = bind_args

    def dump(self, data):
        '''
        - encodings which inherit the dump method can take a data object
        which is a model, a list of models, or a dict of models keyed
        by model name.
        - bind method is called for each data object with bind args
        - model version is identified by version of obj or first obj found
        in dict or list
        - returns dict of bound results keyed by model name
        #TODO: doctest data dict, list, obj
        '''

        d = {}
        if isinstance(data, dict):
            version = None
            for k, v in data.items():
                if isinstance(v, list):
                    if len(v) > 0:
                        d[k] = []
                        for obj in v:
                            if version is None:
                                version = obj.version
                            bind = getattr(obj, self.bind_method)
                            d[k].append(bind(*self.bind_args))
                else: #v is model
                    if version is None:
                        version = v.version 
                    bind = getattr(v, self.bind_method)
                    d[k] = bind(*self.bind_args)
            if not version is None:
                d['version'] = version #?

        elif isinstance(data, list):
            if len(data) > 0:
                d = defaultdict(lambda: [])
                if not data[0].version is None:
                    d['version'] = data[0].version
                for model in data:
                    bind = getattr(model, self.bind_method)
                    d[model.plural].append(bind(*self.bind_args))

        else: #data is model
            bind = getattr(data, self.bind_method)
            d = {data.modelname: bind(*self.bind_args)}
            if not data.version is None:
                d['version'] = data.version

        return d

    def commit(self):
        pass #do nothing by default

#--- dict
class DictEncoder(Encoder):
    '''
    encodes model as dict. model names and members encoded as keys
    ```
    >>> DictEncoder().dump(\
            Goose(name='lucy', cool_friend=Penguin('flipper'))) \
            # doctest: +NORMALIZE_WHITESPACE
    {'goose': {'cool_friend': {'name': 'flipper'}, 'name': 'lucy'},
     'version': 'some'}

    ```
    '''

    def __init__(self):
        super(DictEncoder, self).__init__(bind_method='dump_dict')

    def dump(self, data):
        return super(DictEncoder, self).dump(data)

class DictDecoder(object):
    '''
    decodes models encoded as dictionaries. decode model type from key
    by first checking if key is registered member of Parent, then checking
    if key is registered model name.
    ```
    >>> DictDecoder({'version':'some',
    ...              'geeses':[
    ...                  {'name':'phil', 'cool_friend':{'name':'lary'}},
    ...                  {'name':'todd', 'cool_friend':{'name':'greg'}}],
    ...              'penguin':{'name':'rick'}
    ...             }).load().next() # doctest: +NORMALIZE_WHITESPACE
    {'geeses':
        [<phil is a goose friend of lary. <lary is a cool penguin.>>,
         <todd is a goose friend of greg. <greg is a cool penguin.>>],
     'penguin': <rick is a cool penguin.>}

    ```
    '''

    def __init__(self, d=None):
        self.d = d

    def load(self):
        if 'version' in self.d:
            registry = Model.get_version(self.d['version'])
        else:
            registry = Model

        yield DictDecoder._load(self.d, registry)

    @staticmethod
    def _load(d, registry, Parent=dict):

        #TODO: handle uid in model
        uid = None
        if 'uid' in d:
            uid = d['uid']
            del d['uid']

        data = {}
        for member_name, member_value in d.items():

            if isinstance(member_value, dict):
                data[member_name] = DictDecoder._decodedict(
                                        Parent, member_name, member_value,
                                        registry)

            elif isinstance(member_value, list):
                data[member_name] = DictDecoder._decodelist(
                                        Parent, member_name, member_value,
                                        registry)

            else: #python "built in" type
                if not member_name == 'version': #ignore version
                    data[member_name] = member_value

        model = None
        try:
            model = Parent(**data)
        except TypeError as e:
            raise TypeError('failed to initialize model %s(%s)\n%s' \
                            % (Parent, data, e))

        #TODO: handle uid in model
        if uid and hasattr(model, '_base'):
            model._base.uid = uid
        return model

    @staticmethod
    def _decodelist(Parent, member_name, member_value, registry):

        member = []
        for v in member_value:

            if isinstance(v, dict):
                member.append(DictDecoder._decodedict(
                                  Parent, member_name, v, registry))

            elif isinstance(v, list):
                raise NotImplementedError(
                          'nested member lists not implemented ' +\
                          '(%s.%s)' % (Parent.__name__, member_name))

            else: #python "built in" type
                member.append(v)

        return member

    @staticmethod
    def _decodedict(Parent, member_name, member_value, registry):
        MemberModel = DictDecoder._getmembermodel(
                          Parent, member_name, registry)
        model = DictDecoder._load(member_value, registry, MemberModel)
        return model

    @staticmethod
    def _getmembermodel(Parent, member_name, registry):

        #first check if member name is registered to a model in Parent
        MemberModel = None
        ismodelname = False
        try:
            MemberModel = Parent.get_model(member_name)
        except AttributeError:
            ismodelname = True

        #then check if member name is a model name
        if ismodelname:
            if str(member_name) in registry:
                MemberModel = registry[member_name]
            else:
                raise TypeError(
                    'Type not registed in %s for %s.%s'\
                    % (registry, Parent.__name__, member_name))

        return MemberModel

#--- json
#TODO: probably no reason to wrap the built in lib.
import json
from datetime import datetime
class JSONEncoder(DictEncoder):
    '''
    uses the dict binding to wrap the json lib encoder/decoder
    TODO: nested data
    TODO: doctest datetimeencoder
    ```
    >>> from sys import stdout
    >>> JSONEncoder(stdout, indent=2).dump(\
            Goose(name='lucy', cool_friend=Penguin('flipper'))) \
            # doctest: +NORMALIZE_WHITESPACE
    {
      "goose": {
        "cool_friend": {
          "name": "flipper"
        },
        "name": "lucy"
      },
      "version": "some"
    }

    ```
    '''

    def __init__(self, fp, **jsonargs):
        super(JSONEncoder, self).__init__()
        self.fp = fp
        self.jsonargs = jsonargs

    def dump(self, data, nest=None):
        #TODO: handle nest arg
        return json.dump(super(JSONEncoder, self).dump(data), self.fp,
                         cls=DatetimeEncoder, **self.jsonargs)

class JSONDecoder(DictDecoder):
    '''TODO: doctest'''
    def __init__(self, fp):
        super(JSONDecoder, self).__init__(json.load(fp))

class DatetimeEncoder(json.JSONEncoder):
    def default(self, obj):
        '''serialize datetime objects as isoformat'''
        if isinstance(obj, datetime):
            return obj.isoformat()
        return json.JSONEncoder.default(self, obj)

#--- csv
from itertools import product
class TableEncoder(DictEncoder):

    def __init__(self):
        super(TableEncoder, self).__init__()

    def dump(self, data):
        '''
        creates a table from data by taking cross product of the model.
        returns rows: each row is list of values indexed by key path
        TODO: doctest
        '''
        return TableEncoder.dict_to_table(
                   TableEncoder.cross_product(
                       super(TableEncoder, self).dump(data)))

    @staticmethod
    def columns(rows):
        '''takes rows returned by dump and returns distinct columns
           TODO: doctest'''
        return set(k for keys in rows for k in keys)

    @staticmethod
    def cross_product(data):
        '''returns the cross product of all lists in dict
        TODO: doctest
        '''
        #TODO: doesn't handle nested lists

        facts = []
        for k, v in data.items():
            if isinstance(v, dict):
                data[k] = cross_product(v)
            elif isinstance(v, list): #discard empty lists
                if len(v) == 0:
                    del data[k]
            else:
                data[k] = [v]

        products = list(product(*data.values()))
        facts.extend(dict(zip(data.keys(), p)) for p in products)

        return facts

    @staticmethod
    def dict_to_table(data, key=()):
        '''convert each key path to a column and return rows
        TODO: doctest'''

        rows = []
        for d in data:
            row = []
            for k, v in d.items():
                k2 = key + (k,)
                if isinstance(v, dict):
                    row.extend(
                        TableEncoder.dict_to_table((v,), k2)[0].items())
                else:
                    row.append((k2, v))

            rows.append(dict(row))

        return rows

import csv
class CSVEncoder(TableEncoder):
    '''encode data as table by taking cross product of model.
       each column header is the dotted key path
       TODO: doctest
    '''

    def __init__(self, fp, columns=[], **csvargs):
        super(CSVEncoder, self).__init__()
        self.csvargs = csvargs
        self.columns = [tuple(col.split('.')) for col in columns]
        self.fp = fp

    def dump(self, data):
        rows = super(CSVEncoder, self).dump(data)
        columns = self.columns if self.columns \
                      else list(TableEncoder.columns(rows))

        fp = csv.writer(self.fp, **self.csvargs)
        fp.writerow(['.'.join(col) for col in columns])
        for row in rows:
            row = defaultdict(lambda: None, row)
            row = [row[col] for col in columns]
            row = [v.encode('utf8') if type(v) is unicode else v for v in row]
            fp.writerow(row)

class CSVDecoder:
    '''
    column headers identify member names using a dotted notation
    ```
    >>> from StringIO import StringIO
    >>> goosefp = StringIO('goose.name,goose.cool_friend.name\\n' +\
                           'fred,flipper\\n' +\
                           'betty,tux\\n')
    >>> CSVDecoder(goosefp, version='some').load().next() \
        # doctest: +NORMALIZE_WHITESPACE
    {'geeses':
        [<fred is a goose friend of flipper. <flipper is a cool penguin.>>,
         <betty is a goose friend of tux. <tux is a cool penguin.>>]}

    ```
    '''

    def __init__(self, fp, version=None, page_size=50000, **csvargs):
        self.fp = fp
        self.page_size = page_size
        self.csvargs = csvargs
        self.registry = Model.get_version(version)

    def load(self):

        csvr = csv.reader(self.fp, **self.csvargs)
        columns = csvr.next()

        for i in range(len(columns)):
            columns[i] = columns[i].split('.')

        base = set(column[0] for column in columns)
        if len(base) > 1:
            raise StandardError(
                'Error: Cannot parse objects from CSV file.\n'
                'Multiple models found:\n' +
                str(base))
        base = base.pop()
        cls = self.registry[base]

        objects = []
        i = 0
        for row in csvr:
            i += 1
            objects.append(self._load_row(columns, row)[base])
            if i == self.page_size:
                yield {cls.plural:objects}
                objects = []

        yield {cls.plural:objects}

    def _load_row(self, columns, row):
        d = {}
        for i in range(len(columns)):
            path = d #find path for this column
            pathlen = len(columns[i])
            for j in range(pathlen - 1): #for key in path
                key = columns[i][j]
                if not key in path: #path doesn't exists yet
                    path[key] = {}
                path = path[key]
            key = columns[i][pathlen-1]
            path[key] = row[i]

        return DictDecoder._load(d, registry=self.registry)

#--- concurrent encoder
from threading import Thread
from Queue import Queue
import traceback
def encode(decoders, Encoder, query, thread_count=1, page_commit=False):
    '''Encoder: encoder factory. encoders implement the dump(data) method
       decoders: objects implement load(**query) and commit()
       load data in parallel threads. dump to encoder
       TODO: doctest
       '''

    #TODO: use a data page q so that each page from one decoder can get
    #its own encoder thread
    decoderq = Queue()

    for i in range(min(thread_count, len(decoders))):
        t = _Encode(decoderq, Encoder, query, page_commit)
        t.setDaemon(True)
        t.start()

    for decoder in decoders:
        decoderq.put(decoder)

    decoderq.join()

from time import time
import logging
class _Encode(Thread):

    def __init__(self, queue, Encoder, queryargs, page_commit=False):
        Thread.__init__(self)
        self.queue = queue
        self.Encoder = Encoder
        self.queryargs = queryargs
        self.page_commit = page_commit

    def run(self):
        '''thread main loop'''

        while True:
            decoder = self.queue.get()
            logging.info('encode:loading %s' % decoder)

            buffereddata = None
            try:
                buffereddata = decoder.load(**self.queryargs)
            except:
                logging.exception('encode:could not load %s.' % (decoder))
                self.queue.task_done()
                continue

            encoder = None
            try:
                encoder = self.Encoder()
            except:
                logging.exception('encode:could not initialize encoder.')
                self.queue.task_done()
                continue

            try:
                logging.debug('encode:page offset 0')
                records = 0
                page_offset = 0
                pagestart = time()
                for data in buffereddata:
                    logging.debug('encode:page load %s' % (time() - pagestart))
                    page_offset += 1
                    dumpstart = time()
                    encoder.dump(data)
                    _hack_elasticsearch_support(data)
                    Model.clear_cache() #free memory for next page
                    for value in data.values():
                        if isinstance(value, list):
                            records += len(value)
                    if self.page_commit:
                        commitstart = time()
                        encoder.commit()
                        logging.debug(
                            'encode:commit time %s' % (time() - commitstart))
                    logging.debug('encode:page time %s' % (time() - pagestart))
                    logging.debug('encode:page offset %i' % (page_offset))
                    pagestart = time()
                encoder.commit()
                if hasattr(encoder, 'session'): #TODO?
                    encoder.session.close()
                logging.info('encoder: %i records from %s' % (records, decoder))
            except:
                logging.exception('could not dump %s' % decoder)

            self.queue.task_done()

def _hack_elasticsearch_support(data):
    try:
        import sys
        try:
            sys.path.remove('/opt/iocdb')
        except:
            pass
        import iocdb.mappings.legacy
        import iocdb.session_managers.elasticsearch
        elasticsearch = iocdb.session_managers.elasticsearch.ElasticSearch(
            host='10.114.75.185:9200')
        with elasticsearch.make_session() as session:
            session.add_all(iocdb.mappings.legacy.rumor_mapping.map(rumor)
                            for rumor in data['rumors'])
    except Exception as e:
        logging.debug('elasticsearch hack failed: {}'.format(e))
