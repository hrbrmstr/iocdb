from datetime import timedelta

from celery.utils.log import get_task_logger
logging = get_task_logger(__file__)

import iocdb.dispatcher
from iocdb.config import Config

config = Config()


def migrate(start_date, end_date):
    for n in range(1, int((end_date - start_date).days)*24+1):
        received_starting = end_date - timedelta(hours=n)
        received_ending = end_date - timedelta(hours=n-1)
        _migrate_range.delay(received_starting, received_ending)


@iocdb.dispatcher.app.task(ingnore_result=True)
def _migrate_range(received_starting, received_ending):
    logging.info('migrating documents received: {} -> {}'.format(
        received_starting, received_ending))
    with config.make_session() as session:
        iocdb.dispatcher.propagate(
            session.search_rumors(received_starting=received_starting, received_ending=received_ending),
            __file__, delay=False)

