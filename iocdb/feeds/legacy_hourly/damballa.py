#!/usr/bin/env python

import os
from datetime import datetime
from iocdb.feeds.legacylibs.model_0_9 import Rumor, Document, Observable, TTP, Association
import logging

target_dir = '/home/iocdb/incoming/VZW-Damballa'
target_files = []
ttp = TTP(category="malicious host")

def load(textfeeds='', **query):
    feed = {'rumors' : []}
    valid = datetime.now()

    os.chdir(target_dir)
    for f in os.listdir("."):
        if f.startswith('Top Damballa DNS Domains') and f.endswith('.csv'):
            target_files.append(f)
        
    if not target_files:
        return #do nothing

    for target in target_files:
        filename = target_dir + '/' + target
        doc = Document(name=target, category='csint', tlp='amber', source='VZW-Damballa')
        with open(filename, 'r') as infile:
            for line in infile:
                try:
                    line = line.replace('\r\n','')
                    fields = line.split(',')
                    if fields[0] != 'Non-Existent Domain' and fields[0] != 'Target Dns Domain':
                        given_date = target[target.find('-') - 2:target.find('-') + 8]
                        dp = given_date.split('-')
                        updated = datetime(int(dp[2]),int(dp[0]),int(dp[1]))
                        description = ''
                        if fields[3]:
                            description = 'Classification='+str(fields[3])+'; '
                        description += 'Event Count= '+str(fields[2])
                    
                        obs = Observable('domain', fields[0])
                        feed['rumors'].append(Rumor(obs, updated, doc, ttp=ttp, description=description))
                except StandardError as e:
                    logging.error('Error processing line in ' + \
                        target_dir + '/' + target + ':' + \
                        'msg=%s' % e.message)
    for target in target_files:
        os.remove(target_dir+'/'+target)

    yield feed
