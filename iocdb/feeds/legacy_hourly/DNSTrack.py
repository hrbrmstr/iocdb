#!/usr/bin/env python

from time import strftime, gmtime
import dns.resolver
import argparse
import sqlite3
import json
import sys
from datetime import datetime
import sys
sys.path.append('./')
from iocdb.feeds.legacylibs.model_0_9 import Rumor, Document, Observable, TTP, Association


def dbsetup(name):
	try:
		conn = sqlite3.connect(name, isolation_level=None)
		cur = conn.cursor()
	except:
		ish = "Error connecting to database\n"
		print ish

	try:
		cur.execute("CREATE TABLE IF NOT EXISTS dlist(Domain TEXT, IP TEXT, old TEXT)")
		return cur
	except:
		ish = "Error creating database table"
		print ish

def resolve(domain):
	try:
		answers = dns.resolver.query(domain, 'A')
		result = []
	
		for entry in answers:
			result.append(entry.address)
		
		return result.sort()
	except:
		#Error resolving domain (likely NXDOMAIN)
		result = None
		return result

def add(domain, db):
	test = db.execute("SELECT COUNT(*) FROM dlist WHERE Domain = ?", (domain,)).fetchone()[0]
	if test == 0:
		db.execute("INSERT INTO dlist VALUES(?, ?, ?)", (domain, None, 'no'))
	else:
		ish = "Domain: " + domain + " is already in the database, not inserted"
		print ish

def list(db, choice):
	listing = []
	for row in db.execute("SELECT DISTINCT Domain FROM dlist"):
		listing.append(row[0])
	
	if choice == "yes":	
		for entry in listing:
			print entry
	else:
		return listing

def rem(domain, db):
	db.execute("DELETE FROM dlist WHERE Domain = ?", (domain,))
	
def update(db):
	domains = list(db, "No")
	for entry in domains:
		newip = resolve(entry)
		if newip == None:
			db.execute("UPDATE dlist SET old = ? WHERE Domain = ?", ('yes', entry))	
		else:
			iplist = ""
			for ind in newip:
				iplist += ind+","
			if iplist == db.execute("SELECT IP FROM dlist WHERE Domain = ?", (entry,)).fetchone()[0]:
				db.execute("UPDATE dlist SET old = ? WHERE Domain = ?", ('yes', entry))
			else:
				db.execute("UPDATE dlist SET IP = ? WHERE Domain = ?", (iplist, entry))
				db.execute("UPDATE dlist SET old = ? WHERE Domain = ?", ('no', entry))

def export(db):
	total = []
	for row in db.execute("SELECT * FROM dlist WHERE old = ?", ('no',)):
		try:
			ips = row[1].split(',')
			domdict = {}
			domdict = {'value' : row[0], 'type' : 'domain'}
	
			for ip in ips[:-1]:
				ipdict = {}
				srcdict = {}
				pair = []
				ipdict = {'value' : ip, 'type' : 'ip'}
				pair.append(domdict)
				pair.append(ipdict)
				srcdict = {'updated' : strftime("%Y-%m-%d %H:%M:%S", gmtime()), 'source' : 'DNSTrack', 'category' : 'C2', 'indicators' : pair}
		
			total.append(srcdict)
		except:
			pass #empty row			

	print json.dumps(total, sort_keys=True, indent=4, separators=(',', ': '))

def load(textfeeds='', **query):
	loc = './data/DNSTrack'
	try:
		conn = sqlite3.connect(loc, isolation_level=None)
		cur = conn.cursor()
	except:
		ish = "Error connecting to database\n"
		print ish

	try:
		cur.execute("CREATE TABLE IF NOT EXISTS dlist(Domain TEXT, IP TEXT)")
	except:
		ish = "Error creating database table"
		print ish

	update(cur)	
	total = []
	feed = {'associations' : []}
	valid = datetime.now()
	doc = Document(name=loc, category='derived', tlp='amber', source='DNSTrack')
	#print "GOT HERE!!!!!!!!!!!!!!!!"
	for row in cur.execute("SELECT * FROM dlist WHERE old = ?", ('no',)):
		try:
			ips = row[1].split(',')		
			domobs = Observable('domain', row[0])
			
			for ip in ips[:-1]:
				#print ip+'\n'
				ipobs = Observable('ip', ip)
				
				dom2ip = Association(domobs, ipobs, variety='resolved_to', valid=valid, document=doc)
				ip2dom = Association(ipobs, domobs, variety='resolved_to', valid=valid, document=doc)
			
				feed['associations'].append(ip2dom)
				feed['associations'].append(dom2ip)				
		except:
			pass #empty row

	yield feed
	

def main():
	parser = argparse.ArgumentParser(description="Conduct Domain resolution checks and export data for iocDB")
	parser.add_argument("-a", "--add", help="Add new domain to track")
	parser.add_argument("-l", "--list", action="store_true", help="List all domains being tracked")
	parser.add_argument("-r", "--remove", help="Remove a domain from tracking")
	args = parser.parse_args()

	db = dbsetup('./data/DNSTrack')

	if args.add:
		add(args.add, db)
		db.close()
		sys.exit()
		
	if args.list:
		list(db, "yes")
		db.close()
		sys.exit()
		
	if args.remove:
		rem(args.remove, db)
		db.close()
		sys.exit()
		
	update(db)
	export(db)
	
	db.close()

if __name__ == "__main__":
	main()
	#except:
	#	print "Execution Failure!!\n"
	#	sys.exit()
