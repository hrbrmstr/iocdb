#!/usr/bin/env python 

import re
import sys
import urllib2
from datetime import datetime
from iocdb.feeds.legacylibs.model_0_9 import Rumor, Document, Observable, TTP, Association
import Queue
import threading

# sourced from https://gitweb.torproject.org/tor.git/blob/HEAD:/src/or/config.c#l836
ips = [ '86.59.21.38:80', 
    '194.109.206.212:80', 
    '82.94.251.203:80',
    '76.73.17.194:9030',
    '212.112.245.170:80',
    '193.23.244.244:80',
    '208.83.223.34:443',
    '171.25.193.9:443',
    '154.35.32.5:80' ]
dir_servers = []
for ip in ips:
    dir_servers.append('http://' + ip + '/tor/status-vote/current/consensus')

# will be called by each thread
def get_url(q, url):
    try:
        response = urllib2.urlopen(url)
        q.put([url, response.read()])
    except:
        pass #no big deal, we have eight others to try

def load(textfeeds='', **query):
    feed = {'rumors' : []}
    valid = datetime.now()
    ttp = TTP(category="proxy")

    q = Queue.Queue()

    #start threads to pull from all the servers at the same time
    for u in dir_servers:
        t = threading.Thread(target=get_url, args = (q,u))
        t.daemon = True
        t.start()

    #this should grab the first response that is put on the queue, then continue
    #it should not wait for the other threads to complete
    consensus = q.get()

    pattern = '^r\s(.*?)\s(?:.*?\s){4}(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\s(\d*)\s(\d*).*?\ns\\s(.*?)\n'
    regex = re.compile(pattern, re.MULTILINE)

    doc = Document(name=consensus[0], category='osint', tlp='white',
        source='Tor Directory Consensus')

    for record in regex.finditer(consensus[1]):
        obs = Observable('ip', record.group(2))
        desc = 'Tor '
        # node type
        if 'Exit' in record.group(5):
            desc += 'exit node '
        else:
            desc += 'relay '
        # Other data about the node
        desc += 'hostname: ' + record.group(1) + ' ORPort: ' + record.group(3) + ' DirPort: ' + record.group(4)

        feed['rumors'].append(Rumor(obs, valid, doc, ttp, description=desc))

    yield feed
