#!/usr/bin/env python

import os
import csv
import sys
from datetime import datetime
from iocdb.feeds.legacylibs.model_0_9 import Rumor, Document, Observable, TTP
import pprint
import iocdb.dispatcher

target_dir = '/home/iocdb/incoming/Shadowserver'
#target_dir = '/home/labuser/iocdb/feeds/legacy_hourly/shadow-serv'

target_files = []

def load(textfeeds='', **query):
    feed = {'rumors': []}

    os.chdir(target_dir)
    for f in os.listdir("."):
        if f.endswith('.csv'):
            target_files.append(f)
    if not target_files:
        return  # do nothing

    for target in target_files:
        filename = os.path.join(target_dir, target)
        #print filename
        doc = Document(name=target, category='csint', tlp='amber', source='Shadowserver')
        dp = target[:10].split('-')
        valid = datetime(int(dp[0]),int(dp[1]),int(dp[2]))

        with open(filename, 'rb') as f:
            reader = csv.DictReader(f, delimiter=',')

            if 'botnet_drone' in target:
                ttp = TTP(category='c2', actions='hacking: backdoor or c2')
                for row in reader:
		    #print row
                    if row['ip'] != '':
                        obs = Observable('ip', row['ip'])
                        feed['rumors'].append(Rumor(obs, valid, doc, ttp=ttp,
                            description='infection=' + row['infection'] + \
                            '; port=' + row['cc_port'] + '; dns=' + \
                            row['hostname']))

            elif 'cc_ip' in target:
                ttp = TTP(category='c2', actions='hacking: backdoor or c2')
                for row in reader:
                    ips = row['ip'].split(' ')
                    for i in ips:
                        obs = Observable('ip', i)
                        feed['rumors'].append(Rumor(obs, valid, doc, ttp=ttp,
                            description='IRC botnet C2; port=' + \
                            row['port'] + '; channel=' + row['channel']))

            elif 'cwsandbox_url' in target:
                malttp = TTP(category='malware')
                ttp = TTP(category='malicious host')
                for row in reader:
                    malobs = Observable('md5', row['md5hash'])
                    urlobs = Observable('url', row['url'])
                    feed['rumors'].append(Rumor(malobs, valid, doc, 
                        ttp=malttp, description='malware accessed url= ' + \
                        row['url']))
                    feed['rumors'].append(Rumor(urlobs, valid, doc, ttp=ttp, 
                        description='accessed by malware md5= ' + \
                        row['md5hash']))

            elif 'open_proxy' in target:
                ttp = TTP(category='proxy')
                for row in reader:
                    obs = Observable('ip', row['ip'])
                    feed['rumors'].append(Rumor(obs, valid, doc, ttp=ttp, 
                    description='port= ' + row['port'] + '; type= ' + \
                    row['type']))
	    elif 'compromised_website' in target:
		ttp = TTP(category='malicious host')
		for row in reader:
		    ipObs = Observable('ip', row['ip'])
		    urlObs = Observable('url', row['http_host'] +'/'+ row['url'])
		    desc = 'infection=' + row['tag'] + ';' + 'port=' + row['port'] +';' + 'hostname='+ row['hostname']
		    feed['rumors'].append(Rumor(ipObs,valid,doc,ttp=ttp,description=desc))
		    feed['rumors'].append(Rumor(urlObs,valid,doc,ttp=ttp,description=desc))

            elif 'sinkhole_http_drone' in target:
                pass
            elif 'microsoft_sinkhole' in target:
                pass
            elif 'spam_url' in target:
                pass
            elif 'dns_openresolver' in target:
                pass
	    elif 'ssl_scan' in target:
		pass
	    elif 'scan_ntp' in target:
                pass
            elif 'scan_chargen' in target:
                pass
            elif 'scan_ipmi' in target:
                pass
            elif 'scan_net_pmp' in target:
                pass
	    elif 'scan_ntpmonitor' in target:
		pass
	    elif 'scan_qotd' in target:
                pass
            elif 'scan_ssdp-verizon' in target:
                pass
	    elif 'scan_netbios-verizon' in target:
		pass  
	    
            else:
                sys.stderr.write('Unrecognized CSV file in directory: ' + \
                target + '\n')

    for target in target_files:
        os.remove(os.path.join(target_dir, target))

    yield feed

