import re
import json
import urllib2
from datetime import datetime

from celery.schedules import crontab
from celery.task import periodic_task
import celery.utils.log

from iocdb.model import Rumor, Document, Observable, TTP, Association
import iocdb.dispatcher

import pprint

'''
There is also (these will autopopulate with new DGAs as I add them):
c2-ipmasterlist.txt - aggregation of all "iplist" files (i.e. not
nameservers) across all families
c2-dommasterlist.txt - aggregation of all active domains across all families
c2-masterlist.txt - aggregation of all 4 indicator types across all families
dga-feed.txt - listing of all possible domains for the DGAs I am
tracking regardless of whether they resolve or not (DAILY UPDATE)
'''


@periodic_task(run_every=crontab(minute=32, hour=23), ignore_result=True)
def parse():
  try:
    feed = urllib2.urlopen('http://osint.bambenekconsulting.com/feeds/c2-masterlist.txt')
    #feed = urllib2.urlopen('http://osint.bambenekconsulting.com/feeds/c2-ipmasterlist.txt')
    ttp = TTP(category="c2",malware="GOZ")
    date = datetime.now()
    iocdb.dispatcher.propagate(__get__rumors(date,feed,ttp),__file__)

  except urllib2.HTTPError,e:
    print "****exception**** http  error code: %s" % (e.code)
    
def __get__rumors(date,feed,ttp):
  for i in feed:
      obv = ''
      desc = ''
      if re.match('^#',i):
	continue
      line = i.rstrip()
      line = line.split(',')
      docName = Document(name=line[3], category='osint', source='Bambenek',tlp='green')
      if re.match('\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}',line[0]):
	obv= Observable('ip',line[0])
	desc = line[1]
      elif re.match('[a-zA-Z0-9]+?\.?[a-zA-Z0-9]+\.\w{1,3}',line[0]) :
	obv= Observable('domain',line[0])
	desc = line[1]
      else:
	continue
      yield Rumor(obv,date,docName,ttp,description=desc)
