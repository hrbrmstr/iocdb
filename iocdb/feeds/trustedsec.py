#!/usr/bin/env python

import re
import json
import urllib2
from datetime import datetime

from celery.schedules import crontab
from celery.task import periodic_task
import celery.utils.log

from iocdb.model import Rumor, Document, Observable, TTP, Association
import iocdb.dispatcher

ignore = re.compile("^#")
ippat = re.compile('\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}')
ttp = TTP(category="malicious host")
feed = "https://www.trustedsec.com/banlist.txt"
desc = 'TrustedSec Malicious IP'

@periodic_task(run_every=crontab(minute=0, hour=4), ignore_result=True)
def parse():
    valid = datetime.now()
    doc = Document(name=feed, category='osint', source='TrustedSec IP Banlist')
    page = urllib2.urlopen(feed)
    iocdb.dispatcher.propagate(_get_rumors(valid, page, doc), __file__)

def _get_rumors(valid, page, doc):
    for line in page.readlines():
        if (not ignore.match(line)) and ippat.match(line):
            obs = Observable('ip', line.rstrip())
            yield Rumor(obs, valid, doc, ttp, description=desc)
