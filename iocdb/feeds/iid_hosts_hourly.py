#!/usr/bin/env python

import re
import json
import requests
import glob
import shutil
import time
import logging
import os
import sys
from datetime import datetime,date


from celery.schedules import crontab
from celery.task import periodic_task
import celery.utils.log

from iocdb.model import Rumor, Document, Observable, TTP, Association
import iocdb.dispatcher

unknowns=set()
FEED_ID="IID HOSTS FEED"

LOGGING_LEVEL = logging.INFO  # Modify if you just want to focus on errors
logging.basicConfig(level=LOGGING_LEVEL,
                    format='%(asctime)s %(levelname)-8s %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    stream=sys.stdout)

def get_mapping(designation):

    des=designation.upper()

    ttps = {'419':TTP(category="malicious host", actions="social: scam"),
        'ANGLER':TTP(category="exploit server", malware="Angler EK"),
        'BACKDOOR_RAT':TTP(category="c2"),
        'BAD HOSTNAME':TTP(category='malicious host'),
        'BLACKHOLE':TTP(category='exploit server', malware="Blackhole EK"),
        'BUTTERFLY_BOT':TTP(category='c2',malware='Mariposa'),
        'CITADEL':TTP(category='c2',malware='Citadel'),
        'CRIDEX':TTP(category='c2',malware='Cridex'),
        'CRYPTOLOCKER':TTP(category='c2',malware='CryptoLocker'),
        'CUTWAIL':TTP(category='c2',malware='Cutwail'),
        'EXPLOIT_KIT':TTP(category='exploit server'),
        'FIESTA':TTP(category='exploit server', malware="Fiesta EK"),
        'MAGNITUDE':TTP(category='exploit server', malware="Magnitude EK"),
        'MALICIOUS_NAMESERVER':TTP(category='malicious host'),
        'MALVERTISING':TTP(category='exploit server'),
        'MALWARE':TTP(category='c2'),
        'MALWARE_C2':TTP(category='c2'),
        'MALWARE_DOWNLOAD':TTP(category='malware distributor'),
        'MONEY_MULE_RECRUITMENT':TTP(category='malicious host', actions="social: scam"),
        'NEUTRINO':TTP(category='exploit server', malware='Neutrino EK'),
        'NUCLEAR':TTP(category='exploit server', malware='Nuclear EK'),
        'PARASITE':TTP(category='c2'),
        'PHARMA':TTP(category='malicious host'),
        'PHISH_KIT_COMPONENT':TTP(category='malicious host', actions='social: phishing'),
        'PHISHING':TTP(category='malicious host', actions='social: phishing'),
        'PONY_LOADER':TTP(category='c2', malware='Pony'),
        'PUSHDO':TTP(category='c2', malware='Pushdo'),
        'QAKBOT':TTP(category='c2', malware='Qakbot'),
        'RANSOMWARE':TTP(category='c2'),
        'REDKIT':TTP(category='exploit server', malware='Redkit'),
        'SAKURA':TTP(category='exploit server', malware='Sakura EK'),
        'SHYLOCK':TTP(category='c2', malware='Shylock'),
        'SPYEYE':TTP(category='c2', malware='Spyeye'),
        'STYX':TTP(category='exploit server', malware='Styx EK'),
        'SWEET_ORANGE':TTP(category='exploit server', malware='Sweet Orange EK'),
        'TM_VIOLATION':TTP(category='malicious host'),
        'UPATRE':TTP(category='malicious host'),
        'ZEROACCESS':TTP(category='c2', malware='ZeroAccess'),
        'ZEUS':TTP(category='c2', malware='Zeus'),
        'ZEUS_P2P':TTP(category='c2', malware='Zeus P2P'),
        'BOT ASPROX':TTP(category='bot', malware='Asprox'),
        'BOT CUTWAIL':TTP(category='bot', malware='Cutwail'),
        'BOT FESTI':TTP(category='bot', malware='Festi'),
        'BOT KELIHOS':TTP(category='bot', malware='Kehlihos'),
        'BOT SLENFBOT':TTP(category='bot', malware='Slenfbot'),
        'BOT_PUSHDO':TTP(category='bot', malware='Pusho'),
        'BOT_ZEROACCESS':TTP(category='bot', malware='ZeroAccess'),
        'BOT_ZEUS_P2P':TTP(category='bot', malware='Zeus P2P')}

    if des in ttps:
        return ttps[des]
    else:
        unknowns.add(des)
        return TTP(category='malicious host')

def download_data():

    API_KEY="288a3b982d7c4ea48a2b21ca79627875212fd7cfac5f47f49e5b8313b122200b"
    PULL_URL="https://api.activetrust.net:8000/api/data/threats/host?dga=false&period=1%20hours&data_format=json"
    LOG_REG="IID_HOSTS_ARCHIVE_%s.json" % time.strftime("%Y%m%d%H")
    STORE_PATH="/home/iocdb/incoming/iid/INCOMING-ALL-HOSTS"

    s = requests.Session()
    s.auth=(API_KEY,'')
    try:
        data=s.get(PULL_URL)
    except Exception as e:
        logging.debug("%s: Couldn't download feed... %s" % (FEED_ID,e))
        sys.exit(0)


    #::Archive::#
    # log_data=open(os.path.join(STORE_PATH,LOG_REG),'ab+')
    # log_data.write(data.text)

    #::Check to see if data available::#
    json_tmp=json.loads(data.text)
    if json_tmp.has_key("available_record_count"):
        if json_tmp["available_record_count"] > 0:
            return json_tmp
        else:
            logging.debug("%s: available_record_count == 0..." % (FEED_ID))
            sys.exit(0)
    else:
        logging.debug("%s: catastrophic - no available_record_count field in reply..." % (FEED_ID))
        sys.exit(0)

@periodic_task(run_every=crontab(minute=0, hour='*'), ignore_result=True)
def parse():

    results=int()
    results+=_propagate_rumors()
    print('IID HOSTS feed propagated {} rumors'.format(results))

    logging.debug("%s: Completed. Unmapped identifiers %s..." % (FEED_ID,",".join(unknowns)))


def _propagate_rumors(page_size=10000):

    doc_valid=datetime.now()
    doc_name="IID_HOSTS_ARCHIVE_%s.json" % time.strftime("%Y%m%d%H")
    doc_tlp="amber"
    doc_source="Internet Identify DGA Feed"
    description=""
    doc_category="csint"
    rumors=[]
    count=0

    iocdb_doc=Document(name=doc_name, category=doc_category, tlp=doc_tlp, source=doc_source)

    json_data=download_data()

    
    for record in json_data['threat']:
        if len(rumors) == page_size:
            logging.debug("%s: Attemping to propagate %s rumors..." % (FEED_ID,str(len(rumors))))
            try:
                iocdb.dispatcher.propagate(rumors)
            except:
                rumors=[]
                logging.debug("%s: Failed propagating %s rumors..." % (FEED_ID,str(len(rumors))))
                continue
            logging.debug("%s: Successfully propagated %s rumors..." % (FEED_ID,str(len(rumors))))
            rumors = []
        else:
            observable_valid=datetime.strptime(record['received'],'%Y-%m-%dT%H:%M:%S.%fZ')
            observable=record['host']
            iocdb_description="IID Indicator - Class: %s Property: %s Severity: %s" % (record['class'],record['property'],record['threat_level'])
            #::Build model::#
            try:
                mapping_tmp="_".join(record['property'].split("_")[1:])
                iocdb_ttp=get_mapping(str(mapping_tmp))
	    except:
                logging.debug("%s: Couldn't build TTP object for %s..." % (FEED_ID,observable))
                continue

            try:
                iocdb_observable=Observable('domain',observable)
            except:
                logging.debug("%s: Couldn't build Observable object for %s..." % (FEED_ID,observable))
                continue

            try:
                iocdb_rumor=Rumor(observable=iocdb_observable,valid=observable_valid,document=iocdb_doc,ttp=iocdb_ttp,description=iocdb_description)
            except:
                logging.debug("%s: Couldn't build Rumor object for %s..." % (FEED_ID,observable))
                continue

            rumors.append(iocdb_rumor)
            count+=1

    logging.debug("%s: Attemping to propagate %s rumors..." % (FEED_ID,str(len(rumors))))
    
    try:
        iocdb.dispatcher.propagate(rumors)
    except:
        logging.debug("%s: Failed propagating rumors on document %s..." % (FEED_ID,doc_name))
        pass

    return count
    

if __name__ == '__main__':
    main()
