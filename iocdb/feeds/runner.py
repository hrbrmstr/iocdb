__author__ = 'abarilla'

import logging
from functools import wraps

from celery.schedules import crontab

from iocdb.config import Config
from iocdb.model import Document
from iocdb.utils import urlopen_with_retry
import iocdb.dispatcher

config = Config()
page_size = 5000

hourly_schedule = crontab(minute=config.hourly_minute)
daily_schedule = crontab(hour=config.daily_hour, minute=config.daily_minute)
debug_schedule = crontab(minute='*')


def propagate(domain_objects, source=None):
    page = []
    count = 0
    for obj in domain_objects:
        if len(page) == page_size:
            pass_message(page, source)
            page = []
        page.append(obj)
        count += 1
    pass_message(page, source)


def pass_message(domain_objects, source, async=False):
    for index in range(len(config.message_handlers)):
        if async:
            iocdb.dispatcher.handle_message.delay(index, domain_objects, source)
        else:
            iocdb.dispatcher.handle_message(index, domain_objects, source)


def line_reader(feed_config):
    def line_reader_decoractor(process_line):
        @wraps(process_line)
        def wrapper(*args, **kwargs):
            def build_array():
                doc = Document(name=feed_config['url'], category=feed_config['category'], source=feed_config['id'])
                page = urlopen_with_retry(feed_config['url'])
                for line in page:
                    yield process_line(line, doc)
            if not config.feedtest:
                propagate(build_array(), feed_config['id'])
            else:
                return build_array()
        return wrapper
    return line_reader_decoractor
