#!/usr/bin/env python

from datetime import datetime

from celery.task import periodic_task

from iocdb.model import Rumor, Observable, TTP
import iocdb.feeds.runner as runner

# Required variables
feed_config = {
    'url': 'http://reputation.alienvault.com/reputation.data',
    'id': 'AlienVault Reputation Data Feed',
    'no_foreign': False,
    'no_commercial': False,
    'category': 'osint'
}

# Feed specific variables
default_ttp = TTP('malicious host')
cat_to_ttp = {
    'Scanning Host': TTP('malicious host', 'hacking: footprinting'),
    'Malware Domain': TTP('malware distributor'),
    'Spamming': TTP('malicious host', 'social: spam'),
    'Malware IP': TTP('malware distributor'),
    'C&C': TTP('c2')
}
valid = datetime.now()


# function called for each line in the feed
@periodic_task(run_every=runner.daily_schedule, ignore_result=True)
@runner.line_reader(feed_config=feed_config)
def process_line(line, doc):
    fields = line.split('#')
    observable_value = fields[0]
    cat = fields[3]
    desc = '{} from {}, {}'.format(*fields[3:6])
    ttp = cat_to_ttp[cat] if cat in cat_to_ttp else default_ttp
    obs = Observable('ip', observable_value)
    return Rumor(obs, valid, doc, ttp, description=desc)


if __name__ == '__main__':
    process_line(None, None)
