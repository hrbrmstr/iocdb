import base64
import hashlib
import hmac
import json
import requests
import time
import urllib
from datetime import date, datetime, timedelta
from celery.schedules import crontab
from celery.task import periodic_task
import celery.utils.log
from iocdb.model import Rumor, Document, Observable, TTP, Association
import iocdb.dispatcher

class ThreatConnect:
	def __init__(self, aid=None, skey=None, host=None):
		self.aid = aid
		self.skey = skey
		self.host = host
		
	def buildReq(self, path, host):
		timestamp = str(int(time.time()))
		if host.endswith('api'):
			realhost = host[:-4]
			num = len(realhost)+1
		else:
			num = 30
		sig = "%s:%s:%s" % ("/".join(['',path[num:]]), 'GET', timestamp)
		hmacsig = hmac.new(str(self.skey), sig, digestmod=hashlib.sha256).digest()
		auth = 'TC %d:%s' % (int(self.aid), base64.b64encode(hmacsig))
		headers = {'timestamp':timestamp, 'authorization':auth}
		return headers

	def setPath(self, portion, owner, page, host):
		base = 'v2'
		total = "/".join([host, base, portion])
		
		lasthour = datetime.today()-timedelta(hours=1)
		lasthour = lasthour.replace(microsecond=0)
		formed = str(lasthour.isoformat())+"Z"
		if page != None:
			start = page*500
			new_total = "%s?%s" % (total.rstrip('/'), urllib.urlencode({'owner':owner, 'resultStart': start, 'resultLimit': '500', 'modifiedSince':formed}).replace('+','%20'))
			return new_total
		else:
			start = page*500
			end = (page+1)*500
			new_total = "%s?%s" % (total.rstrip('/'), urllib.urlencode({'resultStart': start, 'resultLimit': '500', 'modifiedSince':formed}).replace('+', '%20'))	
			return new_total
		return total
		
	def makeReq(self, head, full):
		resp = requests.get(full, headers=head, verify=False)
		try:
			report = resp.json()
			if report['status'] == 'Success':
				return report
		except:
			return 'err'
		
	def indicators(self, host, ind, owner, pg):
		full = self.setPath('indicators', owner, pg, host)
		heads = self.buildReq(full, host)
		data = self.makeReq(heads, full)
		if pg != None and data != 'err':
			pg = pg + 1
			total = data['data']['resultCount']
			tmp = data['data']
			selection = tmp.keys()[0]
			if selection == 'resultCount':
				selection = tmp.keys()[1]

			while total > (pg*500):
				full = self.setPath('indicators', owner, pg, host)
				heads = self.buildReq(full, host)
				dat2 = self.makeReq(heads, full)
				pg = pg + 1
				data['data'][selection].extend(dat2['data'][selection])
		return data

	def attribs(self, host, ind, owner, select):
		base = 'v2'
		total = "/".join([host, base, 'indicators', select, ind, 'attributes'])
		new_total = '%s?%s' % (total.rstrip('/'), urllib.urlencode({'owner':owner}).replace('+','%20'))
		heads = self.buildReq(new_total, host)
		data = self.makeReq(heads, new_total)

		return data

@periodic_task(run_every=crontab(minute=40), ignore_result=True)		
def parse():
	host = 'https://ves.threatconnect.com/api'
	#Potentially change to an array if you want to pull from multiple owners. Would also likely require changing host to an array for public data like tcirt entries
	owner = 'Verizon'
	#Change to a config file in the future because better
	aid = '60504934081108103056'
	skey = 'R2Z%fWjOLS_t9rD-D#v5s6QRjV<u-25W2^3*+J_7UqlJRtQ(<bq,9.%K54IJ]AhX'

	inst = ThreatConnect(aid, skey, host)
	test = inst.indicators(host, None, owner, 0)
	total = []
	if test['status'] == "Success":
		for hit in test['data']['indicator']:
			if 'confidence' in hit:
				if hit['confidence'] > 0:
					if hit['type'] == "Address":
						term = "addresses"
						cat = TTP(category="malicious host")
						obs = 'ip'
					elif hit['type'] == "Host":
						term = "hosts"
						cat = TTP(category="malicious host")
						obs = 'domain'
					elif hit['type'] == "File":
						term = "files"
						cat = TTP(category="malware")
						obs = 'md5'
					elif hit['type'] == "emailAddress":
						term = "emailAddresses"
						cat = TTP(category="malicious email address")
						obs = 'email'
					elif hit['type'] == "url":
						term = "urls"
						cat = TTP(category="malicious host")
						obs = 'url'
					tempinst = ThreatConnect(aid, skey, host)
					attrib = tempinst.attribs(host,hit['summary'],owner,term)
					source = 'ThreatConnect'
					if attrib == 'err':
						#Means that something happened with the attribute URL request.
						print "request error"
						pass
					else:
						try:
							if attrib['status'] == "Success":
								for inst in attrib['data']['attribute']:
									if inst['type'] == "Category":
										if hit['type'] == "File":
											cat = TTP(category="malware")
										else:
											cat = TTP(category=inst['value'])
									elif inst['type'] == "Source":
										src = inst['value']
									elif inst['type'] == "Description":
										desc = inst['value']
								try:
									new_desc = src+" - "+desc
								except:
									new_desc = desc
								valid = datetime.now()
								doc = Document(name=hit['webLink'], category='csint', source=source)
								obs = Observable(obs, hit['summary'])
								total.append(Rumor(obs, valid, doc, cat, description=new_desc))
						except:
							#means there was some sort of data error (wasn't json)
							print "attribute error"
							pass
		iocdb.dispatcher.propagate(_get_rumors(total),__file__)

def _get_rumors(rums):
	for hit in rums:
		yield hit
