#!/usr/bin/env python

import re
import sys
from iocdb.feeds.legacylibs.lib.BeautifulSoup import BeautifulSoup
from iocdb.feeds.legacylibs.model_0_9 import Rumor, Document, Observable, TTP, Association
from datetime import datetime
import iocdb.feeds.legacylibs.lib.mechanize as mechanize
import json

ip = re.compile("(([2][5][0-5]\.)|([2][0-4][0-9]\.)|([0-1]?[0-9]?[0-9]\.)){3}(([2][5][0-5])|([2][0-4][0-9])|([0-1]?[0-9]?[0-9]))")

naughty = TTP(category="malicious host")

feeded = {"http://app.webinspector.com/recent_detections": {'ttp':naughty}}

def load(textfeeds=feeded, **query):
	feed = {'rumors' : []}

	for feedurl, feeddata in textfeeds.items():
		browser = mechanize.Browser()
		report = browser.open(feedurl)
		html = report.read()
		page = BeautifulSoup(html)
		
		doc = Document(name=feedurl, category='osint', tlp='white', source='Comodo SiteInspector Recent Detections')
		
		for entry in page.findAll("tr", {"class" : "error-row"}):
			dom = entry.findNext('td').text
			dat = entry.findNext('td').nextSibling.nextSibling.nextSibling.next.text.split()
			
			for prefix in ('http://', 'https://'): 
				if dom.startswith(prefix): 
					dom = dom[len(prefix):]
			
			if ip.match(dom):
				var = 'ip'
			else:
				var = 'domain'
				
			obs = Observable(var, dom)
			da = dat[0].split('-')
			rdat = datetime(int(da[0]),int(da[1]),int(da[2]))
			
			feed['rumors'].append(Rumor(obs, rdat, doc, ttp=feeddata['ttp'], description='High Risk Comodo Detection'))
			
	yield feed
