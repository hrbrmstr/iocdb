#!/usr/bin/env python

import json
import urllib2
from datetime import datetime
from iocdb.feeds.legacylibs.model_0_9 import Rumor, Document, Observable, TTP, Association

atomic = TTP(category="malicious host")

feeded = {"http://www.autoshun.org/files/shunlist.csv": {'ttp':atomic, 'variety':'ip'}}

def load(textfeeds=feeded, **query):
	feed = {'rumors' : []}
	valid = datetime.now()

	for feedurl, feeddata in textfeeds.items():
		page = urllib2.urlopen(feedurl)
		doc = Document(name=feedurl, category='osint', tlp='white', source='Autoshun.org Shunlist')
		
		for line in page.readlines()[1:]:
			elems = line.split(',')
			obs = Observable(feeddata['variety'], elems[0]) 

			feed['rumors'].append(Rumor(obs, valid, doc, ttp=feeddata['ttp'], description=elems[2].rstrip()))

	yield feed
