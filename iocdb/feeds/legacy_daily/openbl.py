#!/usr/bin/env python

import re
import json
import urllib2
from datetime import datetime
from iocdb.feeds.legacylibs.model_0_9 import Rumor, Document, Observable, TTP, Association

ignore = re.compile("^#")

ttp = TTP(category="malicious host")

feeded = {"http://www.openbl.org/lists/base.txt": {'ttp':ttp, 'variety':'ip', 'desc':'Blacklisted IP from OpenBL'}}

def load(textfeeds=feeded, **query):
	feed = {'rumors' : []}
	valid = datetime.now()

	for feedurl, feeddata in textfeeds.items():
		page = urllib2.urlopen(feedurl)
		doc = Document(name=feedurl, category='osint', tlp='white', source='OpenBL IP List')
		
		for line in page.readlines():
			if not ignore.match(line):
				obs = Observable(feeddata['variety'], line.rstrip()) 

				feed['rumors'].append(Rumor(obs, valid, doc, ttp=feeddata['ttp'], description=feeddata['desc']))
	
	yield feed
