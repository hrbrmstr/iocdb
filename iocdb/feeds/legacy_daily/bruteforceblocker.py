#!/usr/bin/env python

import json
import urllib2
from datetime import datetime
from iocdb.feeds.legacylibs.model_0_9 import Rumor, Document, Observable, TTP, Association

ttp = TTP(category="malicious host", actions="hacking: brute force")

feeded = {"http://danger.rulez.sk/projects/bruteforceblocker/blist.php": {'ttp':ttp, 'variety':'ip'}}

def load(textfeeds=feeded, **query):
	feed = {'rumors' : []}
	valid = datetime.now()

	for feedurl, feeddata in textfeeds.items():
		page = urllib2.urlopen(feedurl)
		doc = Document(name=feedurl, category='osint', tlp='white', source='Rulez.sk BruteForceBlocker')

		for line in page.readlines()[1::]:
			elems = line.split()
			obs = Observable(feeddata['variety'], elems[0]) 

			feed['rumors'].append(Rumor(obs, valid, doc, ttp=feeddata['ttp'], description='SSH Brute Forcer with '+elems[4]+' detections'))
	
	yield feed
