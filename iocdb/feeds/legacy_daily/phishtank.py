#!/usr/bin/env python

import re
import json
import requests
from datetime import datetime
import logging
from iocdb.feeds.legacylibs.model_0_9 import Rumor, Document, Observable, TTP, Association

doom = re.compile('[A-Z0-9_\-\.]+\.(?:XN--CLCHC0EA0B2G2A9GCD|XN--HGBK6AJ7F53BBA|XN--HLCJ6AYA9ESC7A|XN--11B5BS3A9AJ6G|XN--MGBERP4A5D4AR|XN--XKC2DL3A5EE0H|XN--80AKHBYKNJ4F|XN--XKC2AL3HYE2A|XN--LGBBAT1AD8J|XN--MGBC0A9AZCG|XN--9T4B11YI5A|XN--MGBAAM7A8H|XN--MGBAYH7GPA|XN--MGBBH1A71E|XN--FPCRJ9C3D|XN--FZC2C9E2C|XN--YFRO4I67O|XN--YGBI2AMMX|XN--3E0B707E|XN--JXALPDLP|XN--KGBECHTV|XN--OGBPF8FL|XN--0ZWM56D|XN--45BRJ9C|XN--80AO21A|XN--DEBA0AD|XN--G6W251D|XN--GECRJ9C|XN--H2BRJ9C|XN--J6W193G|XN--KPRW13D|XN--KPRY57D|XN--PGBS0DH|XN--S9BRJ9C|XN--90A3AC|XN--FIQS8S|XN--FIQZ9S|XN--O3CW4H|XN--WGBH1C|XN--WGBL6A|XN--ZCKZAH|XN--P1AI|XN--NGBC5AZD|XN--80ASEHDB|XN--80ASWG|XN--UNUP4Y|MUSEUM|TRAVEL|AERO|ARPA|ASIA|COOP|INFO|JOBS|MOBI|NAME|BIZ|CAT|COM|EDU|GOV|INT|MIL|NET|ORG|PRO|TEL|XXX|AC|AD|AE|AF|AG|AI|AL|AM|AN|AO|AQ|AR|AS|AT|AU|AW|AX|AZ|BA|BB|BD|BE|BF|BG|BH|BI|BJ|BM|BN|BO|BR|BS|BT|BV|BW|BY|BZ|CA|CC|CD|CF|CG|CH|CI|CK|CL|CM|CN|CO|CR|CU|CV|CW|CX|CY|CZ|DE|DJ|DK|DM|DO|DZ|EC|EE|EG|ER|ES|ET|EU|FI|FJ|FK|FM|FO|FR|GA|GB|GD|GE|GF|GG|GH|GI|GL|GM|GN|GP|GQ|GR|GS|GT|GU|GW|GY|HK|HM|HN|HR|HT|HU|ID|IE|IL|IM|IN|IO|IQ|IR|IS|IT|JE|JM|JO|JP|KE|KG|KH|KI|KM|KN|KP|KR|KW|KY|KZ|LA|LB|LC|LI|LK|LR|LS|LT|LU|LV|LY|MA|MC|MD|ME|MG|MH|MK|ML|MM|MN|MO|MP|MQ|MR|MS|MT|MU|MV|MW|MX|MY|MZ|NA|NC|NE|NF|NG|NI|NL|NO|NP|NR|NU|NZ|OM|PA|PE|PF|PG|PH|PK|PL|PM|PN|PR|PS|PT|PW|PY|QA|RE|RO|RS|RU|RW|SA|SB|SC|SD|SE|SG|SH|SI|SJ|SK|SL|SM|SN|SO|SR|ST|SU|SV|SX|SY|SZ|TC|TD|TF|TG|TH|TJ|TK|TL|TM|TN|TO|TP|TR|TT|TV|TW|TZ|UA|UG|UK|US|UY|UZ|VA|VC|VE|VG|VI|VN|VU|WF|WS|YE|YT|ZA|ZM|ZW|[0-9]{1,3})(?:\:\d+)?(?:\/\S+)')
ip = re.compile("(([2][5][0-5]\.)|([2][0-4][0-9]\.)|([0-1]?[0-9]?[0-9]\.)){3}(([2][5][0-5])|([2][0-4][0-9])|([0-1]?[0-9]?[0-9]))")

ttp = TTP(category="malicious host", actions="social: phishing")

feeded = {"http://data.phishtank.com/data/online-valid.json": {'ttp':ttp}}

def load(textfeeds=feeded, **query):
	feed = {'rumors' : [], 'associations': []}
	valid = datetime.now()

	for feedurl, feeddata in textfeeds.items():
		result=requests.get(feedurl)
		if result.status_code == 200:
			
			doc = Document(name=feedurl, category='osint', tlp='white', source='OpenBL IP List')

			try:
				data=json.loads(result.text)
			except:
				logging.debug("Downloaded but couldn't load json for Phishtank feed")
				break
			
			for phish in data:
				if phish['details'][0]['ip_address']:
					try:
						ipobs = Observable('ip', phish['details'][0]['ip_address'])
					except:
						logging.debug("Couldn't build observable for %s" % phish['details'][0]['ip_address'])
						continue
				else:
					continue
				
				#parse description
				ipdescription = None
				domdescription = None
				if phish['target']:
					ipdescription = 'hosted phishing site targeting ' + phish['target']
					domdescription = 'Phishing site targeting ' + phish['target']

				#strip protocol
				url = phish['url'].strip('/')
				protocol_regex = re.compile('[a-z]+://(.*)', re.IGNORECASE)
				match = re.match(protocol_regex, url)
				
				if match:
					url = match.group(1)

				if ip.match(url):
					try:
						domobs = Observable('ip', url)
						feed['rumors'].append(Rumor(domobs, valid, doc, ttp=feeddata['ttp'], description=domdescription))
					except:
						logging.debug("Couldn't build observable for %s" % url)
				
				elif doom.match(url):
					try:
						domobs = Observable('url', url)
						feed['rumors'].append(Rumor(domobs, valid, doc, ttp=feeddata['ttp'], description=domdescription))
					except:
						logging.debug("Couldn't build observable for %s" % url)
				else:
					try:
						domobs = Observable('domain', url)
						feed['rumors'].append(Rumor(domobs, valid, doc, ttp=feeddata['ttp'], description=domdescription))
					except:
						logging.debug("Couldn't build observable for %s" % url)
				
				feed['rumors'].append(Rumor(ipobs, valid, doc, ttp=feeddata['ttp'], description=ipdescription))

				try:
					url2ip = Association(domobs, ipobs, variety='resolved_to', valid=valid, document=doc)
					feed['associations'].append(url2ip)
				except:
					logging.debug("Couldn't build url2ip association")
				try:
					ip2url = Association(ipobs, domobs, variety='resolved_to', valid=valid, document=doc)
					feed['associations'].append(ip2url)
				except:
					logging.debug("Couldn't build ip2url association")
		
		else:
			logging.debug("Couldn't download Phistank JSON feed")
			
	yield feed
