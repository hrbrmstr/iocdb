#!/usr/bin/env python

from BeautifulSoup import BeautifulSoup
from time import strftime, gmtime
import mechanize
import argparse
import sqlite3
import json
import sys
from datetime import datetime
from iocdb.feeds.legacylibs.model_0_9 import Rumor, Document, Observable, TTP, Association

def dbsetup(name):
	try:
		conn = sqlite3.connect(name, isolation_level=None)
		cur = conn.cursor()
	except:
		ish = "Error connecting to database\n"
		print ish

	try:
		cur.execute("CREATE TABLE IF NOT EXISTS iplist(IP TEXT, Domain TEXT)")
		return cur
	except:
		ish = "Error creating database table"
		print ish

def request(ip):
	url = 'http://ip.robtex.com/' + ip + '.html'
	browser = mechanize.Browser()
	
	report = browser.open(url)
	html = report.read()
	page = BeautifulSoup(html)
	
	return page
	
def parse(page):
	doms = []
	try:
		section = page.find("span", {"id" : "sharedha"}).findNext('ul')
		for entry in section.findAll("li"):
			doms.append(entry.text)
		
		return doms
	except:
		return None

def add(ip, db):
	test = db.execute("SELECT COUNT(*) FROM iplist WHERE IP = ?", (ip,)).fetchone()[0]
	if test == 0:
		db.execute("INSERT INTO iplist VALUES(?, ?)", (ip, None))
	else:
		ish = "IP: " + ip + " is already in the database, not inserted"
		print ish

def list(db, choice):
	listing = []
	for row in db.execute("SELECT DISTINCT IP FROM iplist"):
		listing.append(row[0])
	
	if choice == "yes":	
		for entry in listing:
			print entry
	else:
		return listing

def rem(ip, db):
	db.execute("DELETE FROM iplist WHERE IP = ?", (ip,))
	
def update(db):
	ips = list(db, "No")
	for entry in ips:
		source = request(entry)
		newdom = parse(source)
		if newdom != None:
			domlist = ""
			for ind in newdom:
				domlist += ind+","
	
			if domlist == db.execute("SELECT Domain FROM iplist WHERE IP = ?", (entry,)).fetchone()[0]:
				pass
			else:
				db.execute("UPDATE iplist SET Domain = ? WHERE IP = ?", (domlist, entry))
		else:
			pass

def export(db):
	total = []
	feed = {'associations' : []}
	valid = datetime.now()
	doc = Document(category='derived', tlp='amber', source='IPTrack')

	for row in db.execute("SELECT * FROM iplist"):
		try:
			doms = row[1].split(',')
			
			ipobs = Observable('ip', row[0])
			
			for dom in doms[::-1]:
				domobs = Observable('domain', dom)
				
				dom2ip = Association(domobs, ipobs, variety='resolved_to', valid=valid, document=doc)
				ip2dom = Association(ipobs, domobs, variety='resolved_to', valid=valid, document=doc)
			
				feed['associations'].append(ip2dom)
				feed['associations'].append(dom2ip)				
		except:
			pass #empty row

	yield feed
	
def load(textfeeds='', **query):
	loc = '/home/iocdb/data/IPTrack'
	try:
		conn = sqlite3.connect(loc, isolation_level=None)
		cur = conn.cursor()
	except:
		ish = "Error connecting to database\n"
		print ish

	try:
		cur.execute("CREATE TABLE IF NOT EXISTS iplist(IP TEXT, Domain TEXT)")
	except:
		ish = "Error creating database table"
		print ish
	
	total = []
	feed = {'associations' : []}
	valid = datetime.now()
	doc = Document(name=loc, category='derived', tlp='amber', source='IPTrack')

	for row in cur.execute("SELECT * FROM iplist"):
		try:
			doms = row[1].split(',')
			
			ipobs = Observable('ip', row[0])
			
			for dom in doms[::-1]:
				domobs = Observable('domain', dom)
				
				dom2ip = Association(domobs, ipobs, variety='resolved_to', valid=valid, document=doc)
				ip2dom = Association(ipobs, domobs, variety='resolved_to', valid=valid, document=doc)
			
				feed['associations'].append(ip2dom)
				feed['associations'].append(dom2ip)				
		except:
			pass #empty row

	yield feed
	
def main():
	parser = argparse.ArgumentParser(description="Conduct IP -> Domain checks via robtex and export data for iocDB")
	parser.add_argument("-a", "--add", help="Add new IP to track")
	parser.add_argument("-l", "--list", action="store_true", help="List all IPs being tracked")
	parser.add_argument("-r", "--remove", help="Remove an IP from tracking")
	args = parser.parse_args()

	db = dbsetup('/home/iocdb/data/IPTrack')
	
	if args.add:
		add(args.add, db)
		db.close()
		sys.exit()
		
	if args.list:
		list(db, "yes")
		db.close()
		sys.exit()
		
	if args.remove:
		rem(args.remove, db)
		db.close()
		sys.exit()
	
	update(db)
	export(db)
	
	db.close()
	
if __name__ == "__main__":
	main()
