#!/usr/bin/env python

import json
import urllib2
from datetime import datetime
from iocdb.feeds.legacylibs.model_0_9 import Rumor, Document, Observable, TTP, Association

ttp = TTP(category="c2", actions="hacking: c2", malware="Gameover Zeus")

feeded = {"http://www.kleissner.org/text/ZeuSGameover_Domains.txt": {'ttp':ttp, 'variety':'domain'}}

def load(textfeeds=feeded, **query):
	feed = {'rumors' : []}

	for feedurl, feeddata in textfeeds.items():
		page = urllib2.urlopen(feedurl)
		doc = Document(name=feedurl, category='osint', tlp='white', source='K&A VirusTracker', nocomm=True)
		
		for line in page.readlines()[1::]:
			dat = line.split(',')
			obs = Observable(feeddata['variety'], dat[1]) 
			dates = dat[0].split('/')
			valid = datetime(int(dates[2]),int(dates[0]),int(dates[1]))
			feed['rumors'].append(Rumor(obs, valid, doc, ttp=feeddata['ttp'], description='Gameover Zeus domain from VirusTracker'))
	
	yield feed
