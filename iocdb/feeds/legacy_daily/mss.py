#!/usr/bin/env python
#This uses two levels of grouping to create one rumor for an IP on a date

import os
import csv
from datetime import datetime
from iocdb.feeds.legacylibs.model_0_9 import Rumor, Document, Observable, TTP, Association

target_dir = '/home/iocdb/incoming/MSS'
target_files = []
feed = {'rumors' : []}
doc = Document(name='Threat Library Non-Watch List', category='mss',
               tlp='amber', source='mss')
date_dict = {} #main container: key is date; value is a dictionary of IPs

# identify all files to parse
os.chdir(target_dir)
for f in os.listdir("."):
    if f.startswith('TL_NWL') and f.endswith('.csv'):
        target_files.append(f)

def process(target, f): #the load function will call this for each file
    stamp = target[21:33]
    date = datetime(int(stamp[0:4]), int(stamp[4:6]), int(stamp[6:8]))
    backend = target[7:9]
    f.readline()
    reader = csv.reader(f)

    if date not in date_dict: #create an empty dictionary for this date
        date_dict[date] = {}

    for row in reader:
        #extract attributes
        count = row[0]
        ip = row[1]
        dstport = row[2]
        protocol = row[3]
        siglabel = row[4]
        signame = row[5]
        description = row[6].decode('utf-8', 'ignore')
        categories = []
        actions = []

        #skip policy violations that aren't IOCs
        if 'DOWNLOAD:WIN-EXE' == signame:
            continue
        if 'policy violation' in description:
            continue
        
        #infer categories and actions
        if 'bot' in signame.lower():
            categories.append('bot')
        if 'brute force' in description.lower():
            if 'etc/passwd' not in description.lower():
                actions.append('hacking: brute force')
        if 'buffer overflow' in description.lower():
            actions.append('hacking: buffer overflow')
        if 'code execution' in description.lower():
            categories.append('exploit server')
        if 'dos' in signame.lower() or 'denial of service' in signame.lower():
            actions.append('hacking: dos')
        if 'footprinting' in description.lower():
            actions.append('hacking: footprinting')
        if 'GENERIC:UPX-EXE' == signame:
            categories.append('malware distributor')
        if 'remote file inclu' in description.lower():
            actions.append('hacking: rfi')
        if 'shell' in description.lower():
            if 'shellcode' not in description.lower():
                if 'arbitrary' not in description.lower():
                    actions.append('hacking: use of backdoor or c2')
        if 'sql injection' in signame.lower():
            actions.append('hacking: sqli')
            
        if ip not in date_dict[date]:
            #initialize a dictionary for this IP
            date_dict[date][ip] = {'count':[], 'dstport':[],
                'protocol':[], 'siglabel':[], 'signame':[], 'backend':[],
                'category':[], 'action':[], 'description':[]}
        
        #append the row's attributes to the this date's IP's lists
        date_dict[date][ip]['count'].append(int(count))
        date_dict[date][ip]['dstport'].append(dstport)
        date_dict[date][ip]['protocol'].append(protocol)
        date_dict[date][ip]['siglabel'].append(siglabel)
        date_dict[date][ip]['signame'].append(signame)
        date_dict[date][ip]['backend'].append(backend)
        date_dict[date][ip]['description'].append(description)

        for category in categories:
            date_dict[date][ip]['category'].append(category)
        for action in actions:
            date_dict[date][ip]['action'].append(action)

def load(textfeeds='', **query):
    #process each file
    for target in target_files:
        filename = target_dir + '/' + target
        with open(filename,'rb') as f:
            process(target, f)

    #add rumors
    for date, v, in date_dict.iteritems():
        for ip, attribs in date_dict[date].iteritems():
            obs = Observable('ip', ip)
            if len(set(attribs['category'])) != 1:
                cat = 'malicious host'
            else:
                cat = attribs['category'][0]

            action_json = ''
            if len(attribs['action']) > 0:
                acts = []
                for act in set(attribs['action']):
                    acts.append('{"type": "' + act.split(': ')[0] + \
                                '", "variety": "' + act.split(': ')[1] + '"}')
                action_json = '[' + ', '.join(acts) + ']'
            if action_json:
                ttp = TTP(category=cat, actions=action_json)
            else:
                ttp = TTP(category=cat)
            description = ';'.join(['backend(s)=' + \
                            ','.join(set(attribs['backend'])),
                            'total_count=' + str(sum(attribs['count'])),
                            'dstport(s)=' + ','.join(set(attribs['dstport'])),
                            'protocol(s)=' + \
                            ','.join(set(attribs['protocol'])),
                            'siglabel(s)=' + \
                            ','.join(set(attribs['siglabel'])),
                            'signame(s)=' + ','.join(set(attribs['signame'])),
                            'description(s)=\n' + \
                            '\n'.join(set(attribs['description']))
                            ])
            feed['rumors'].append(Rumor(obs, date, doc, ttp,
                                        description=description))

    #move processed files to /home/iocdb/incoming/MSS/archive/
    for target in target_files:
        os.rename(target_dir + '/' + target, target_dir + '/archive/' + target)
    yield feed
