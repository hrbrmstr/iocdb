#!/usr/bin/env python

import json
from urllib import urlopen
from datetime import datetime
from StringIO import StringIO
from iocdb.feeds.legacylibs.model_0_9 import Rumor, Document, Observable, TTP, Association

spam = TTP(category="c2")

feeded = {"http://security-research.dyndns.org/pub/malware-feeds/ponmocup-malware-ips.txt": {'ttp':spam, 'variety':'ip', 'desc':'Ponmocup Malware C2'},
"http://security-research.dyndns.org/pub/malware-feeds/ponmocup-malware-domains.txt":{'ttp':spam, 'variety':'domain', 'desc':'Ponmocup Malware C2'}}

def load(textfeeds=feeded, **query):
    feed = {'rumors' : []}
    valid = datetime.now()

    for feedurl, feeddata in textfeeds.items():
        url = urlopen(feedurl)
        page = StringIO(url.read())

        doc = Document(name=feedurl, category='osint', tlp='white', source='security-research.dyndns.org')

        for line in page.readlines():
            if not line.startswith('#'):
                obs = Observable(feeddata['variety'], line.rstrip())
                feed['rumors'].append(Rumor(obs, valid, doc, ttp=feeddata['ttp'], description=feeddata['desc']))

    yield feed
