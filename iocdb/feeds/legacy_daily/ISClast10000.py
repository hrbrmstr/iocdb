#!/usr/bin/env python

import json
import urllib2
from datetime import date, timedelta, datetime
import xml.etree.ElementTree as ET
from iocdb.feeds.legacylibs.model_0_9 import Rumor, Document, Observable, TTP, Association

yest = date.today()-timedelta(days=1)
url = "https://isc.sans.edu/api/sources/lastseen/10000/" + yest.isoformat()

ttp = TTP(category="malicious host")
feeded = {url: {'ttp':ttp, 'variety':'ip'}}

def load(textfeeds=feeded, **query):
	feed = {'rumors' : []}
	valid = datetime.now()

	for feedurl, feeddata in textfeeds.items():
		page = urllib2.urlopen(feedurl)
		tree = ET.parse(page)
		root = tree.getroot()
		
		doc = Document(name=feedurl, category='osint', tlp='white', source='SANS ISC Attack Sources')

		for child in root:
			clean_ip = child[0].text.replace('.0','.').replace('.0','.').lstrip('0')
			
			obs = Observable(feeddata['variety'], clean_ip) 

			feed['rumors'].append(Rumor(obs, valid, doc, ttp=feeddata['ttp'], description='There were '+child[1].text+' attacks with a count of ' +child[2].text+'. It was first seen on '+child[3].text))

	yield feed
