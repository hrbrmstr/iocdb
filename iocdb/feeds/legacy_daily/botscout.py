#!/usr/bin/env python

import sys
import urllib2
import logging
from iocdb.feeds.legacylibs.lib.BeautifulSoup import BeautifulSoup
from iocdb.feeds.legacylibs.model_0_9 import Rumor, Document, Observable, TTP, Association
from datetime import datetime
import json

malhost = TTP(category="malicious host", actions="social: spam")
malemail = TTP(category="malicious email address", actions="social: spam")

feeded = {"http://botscout.com/last_caught_cache.htm": {}}

def load(textfeeds=feeded, **query):
	feed = {'rumors' : [], 'associations' : []}
	valid = datetime.now()

	for feedurl, feeddata in textfeeds.items():
		page = urllib2.urlopen(feedurl)
		data = BeautifulSoup(page)
		
		doc = Document(name=feedurl, category='osint', tlp='white', source='Bostscout Latest Bots')
		
		for row in data.findAll('tr', {'style' : 'font-size:10px;'}):
			elems = []
			for col in row.findAll('td')[0:3]:
				elems.append(col.text)

			#::Added by matonis during migration::#
			try:	
				ipobs = Observable('ip', elems[2])
				feed['rumors'].append(Rumor(ipobs, valid, doc, ttp=malhost, description='Spambot with username '+elems[0]))
			except:
				logging.debug("Couldn't build observable for %s" % elems[2])

			try:
				emobs = Observable('email', elems[1])
				feed['rumors'].append(Rumor(emobs, valid, doc, ttp=malemail, description='Spambot with username '+elems[0]))
			except:
				logging.debug("Couldn't build observable for %s" % elems[1])
			
			try:
				ip2em = Association(ipobs, emobs, variety='sent_by', valid=valid, document=doc)
				feed['associations'].append(ip2em)
			except:
				logging.debug("Couldn't build association %s -> %s" % (elems[2],elems[1]))

			try:
				em2ip = Association(emobs, ipobs, variety='sent_by', valid=valid, document=doc)
				feed['associations'].append(em2ip)
			except:
				logging.debug("Couldn't build association %s -> %s" % (elems[1],elems[2]))
				
			
	yield feed
