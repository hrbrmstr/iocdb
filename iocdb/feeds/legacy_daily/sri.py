#!/usr/bin/env python

import sys
import json
import urllib2
from datetime import datetime, date
from iocdb.feeds.legacylibs.lib.BeautifulSoup import BeautifulSoup
from iocdb.feeds.legacylibs.model_0_9 import Rumor, Document, Observable, TTP, Association

ttp = TTP(category="malware distributor")

feeded = {"http://mtc.sri.com/live_data/attackers/": {'ttp':ttp, 'variety':'ip'}}

def load(textfeeds=feeded, **query):
	feed = {'rumors' : []}
	#valid = datetime.now()
	yr = date.today().year
	for feedurl, feeddata in textfeeds.items():
		page = urllib2.urlopen(feedurl)
		dat = BeautifulSoup(page)
		doc = Document(name=feedurl, category='osint', tlp='white', source='SRI Malware Threat Center', nocomm=True)

		tab = dat.find("table", {'class':'attackers'})
		tots = []
		for entry in tab.findAll('tr'):
			count = 0
			for ent in entry.findAll('td'):
				count += 1
				if count == 4:
					last = ent.text.split('/')
				elif count == 5:
					mal = ent.text
				elif count == 9:
					tots = ent.text.split(' ')

			if len(tots) > 0:		
				obs = Observable(feeddata['variety'], tots[3])
				if mal != 'unknown':
					ttp = TTP(category="malware distributor", malware=mal)
				else:
					ttp = TTP(category="malware distributor")

				valid = datetime(int(yr), int(last[0]), int(last[1]))
				feed['rumors'].append(Rumor(obs, valid, doc, ttp=feeddata['ttp'], description='SRI top malware source'))
	
	yield feed