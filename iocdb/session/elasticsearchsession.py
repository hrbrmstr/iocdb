__author__ = 'Andrew Barilla <andrew.barilla@one.verizon.com'

import logging

from elasticsearch import Elasticsearch
from elasticsearch import helpers
from elasticsearch_dsl import Search, F, Q

from .session_manager import SessionManager
from iocdb.model import Actor
from iocdb.model import Association
from iocdb.model import Campaign
from iocdb.model import Document
from iocdb.model import Left
from iocdb.model import Observable
from iocdb.model import Right
from iocdb.model import Rumor
from iocdb.model import TTP
from iocdb.errors import ParametersRequiredError
import iocdb.mappings.es


class ElasticsearchSession(SessionManager):
    config = None

    DEFAULT_INDEX = 'iocdb'

    def __init__(self, config):
        self.config = config
        self.es = Elasticsearch(config.elastic_search_hosts)

        if self.config.es_trace:
            tracer = logging.getLogger('elasticsearch.trace')
            tracer.setLevel(logging.DEBUG)
            tracer.addHandler(logging.FileHandler('/tmp/elasticsearch-py.sh'))

    def __str__(self):
        return "ElasticsearchSession %s" % self.config.elastic_search_hosts

    @classmethod
    def is_manager_for(cls, name):
        return name == 'elasticsearch'

    def get_rumor_by_id(self, rumor_id):
        hit = self.es.get(index=self.DEFAULT_INDEX, doc_type='rumor',
                          id=rumor_id)
        return self._map_to_rumor(hit)

    def search_rumors(self, observable_values=None, valid_starting=None,
                      valid_ending=None, received_starting=None,
                      received_ending=None, observable_varieties=None,
                      document_sources=None, document_names=None,
                      document_tlps=None, document_investigators=None,
                      actor_names=None, campaign_names=None, ttp_malware=None,
                      ttp_categories=None, observable_whitelist=None):

        has_param = False

        has_param = True if observable_values is not None else has_param
        has_param = True if valid_starting is not None else has_param
        has_param = True if valid_ending is not None else has_param
        has_param = True if received_starting is not None else has_param
        has_param = True if received_ending is not None else has_param
        has_param = True if observable_varieties is not None else has_param
        has_param = True if document_sources is not None else has_param
        has_param = True if document_names is not None else has_param
        has_param = True if document_tlps is not None else has_param
        has_param = True if document_investigators is not None else has_param
        has_param = True if actor_names is not None else has_param
        has_param = True if campaign_names is not None else has_param
        has_param = True if ttp_malware is not None else has_param
        has_param = True if ttp_categories is not None else has_param

        if not has_param:
            raise ParametersRequiredError()

        s = Search(using=self.es, index=self.DEFAULT_INDEX, doc_type=Rumor)

        if observable_values is not None:
            s = s.filter('terms', observable__value__untouched=observable_values)

        if valid_starting is not None:
            s = s.filter(F('range', valid={'gte': valid_starting}))

        if valid_ending is not None:
            s = s.filter(F('range', valid={'lt': valid_ending}))

        if received_starting is not None:
            s = s.filter(F('range', document__received={'gte': received_starting}))

        if received_ending is not None:
            s = s.filter(F('range', document__received={'lt': received_ending}))

        if observable_varieties is not None:
            s = s.filter('terms', observable__variety=observable_varieties)

        if document_sources is not None:
            s = s.filter('terms', document__source=document_sources)

        if document_names is not None:
            s = s.filter('terms', document__name__untouched=document_names)

        if document_tlps is not None:
            s = s.filter('terms', document__tlp=document_tlps)

        if document_investigators is not None:
            s = s.filter('terms', document__investigator=document_investigators)

        if actor_names is not None:
            s = s.filter('terms', actor__name=actor_names)

        if campaign_names is not None:
            s = s.filter('terms', campaign__name=campaign_names)

        if ttp_malware is not None:
            s = s.filter('terms', ttp__malware=ttp_malware)

        if ttp_categories is not None:
            s = s.filter('terms', ttp__category=ttp_categories)

        if observable_whitelist is not None:
            s = s.query(~Q('terms', observable__value__untouched=observable_whitelist))

        # TODO look into using dsl library to get objects back
        hits = helpers.scan(self.es, s.to_dict(), '60m', index=self.DEFAULT_INDEX, doc_type='rumor')
        # result = s.execute()
        for hit in hits:
            yield self._map_to_rumor(hit)

    def handle_message(self, domain_objects):
        helpers.bulk(self.es, [self._to_create_message(obj) for obj in domain_objects])

    def _to_create_message(self, obj):
        type_name = None
        if isinstance(obj, Rumor):
            type_name = 'rumor'
        elif isinstance(obj, Association):
            type_name = 'association'
        action = obj.to_dict()
        if 'id' in action and action['id'] is None:
            del action['id']
        action['_index'] = self.DEFAULT_INDEX
        action['_type'] = type_name
        return action

    def create_rumor(self, rumor):
        helpers.bulk(self.es, [self._to_create_message(rumor), ], raise_on_error=True)

    def update_rumor(self, rumor):
        if rumor.id is None or rumor.id == '':
            return

        action = {
            '_op_type': 'update',
            '_index': self.DEFAULT_INDEX,
            '_type': 'rumor',
            '_id': rumor.id,
            'doc': self._map_from_rumor(rumor)
        }

        helpers.bulk(self.es, [action, ], raise_on_error=True)

    def delete_rumor(self, rumor_id):
        action = {
            '_op_type': 'delete',
            '_index': self.DEFAULT_INDEX,
            '_type': 'rumor',
            '_id': rumor_id
        }

        helpers.bulk(self.es, [action, ], raise_on_error=True)

    def get_association_by_id(self, model_id):
        hit = self.es.get(index=self.DEFAULT_INDEX, doc_type='association',
                          id=model_id)
        return self._map_to_association(hit)

    def search_associations(self, valid_starting=None, valid_ending=None,
                            received_starting=None, received_ending=None):
        has_param = False

        has_param = True if valid_starting is not None else has_param
        has_param = True if valid_ending is not None else has_param
        has_param = True if received_starting is not None else has_param
        has_param = True if received_ending is not None else has_param

        if not has_param:
            raise ParametersRequiredError()

        clauses = []

        if valid_starting is not None:
            clauses.append({'range': {'valid': {'gte': valid_starting}}})

        if valid_ending is not None:
            clauses.append({'range': {'valid': {'lt': valid_ending}}})

        if received_starting is not None:
            clauses.append({'range': {'document.received': {'gte': received_starting}}})

        if received_ending is not None:
            clauses.append({'range': {'document.received': {'lt': received_ending}}})

        query = {'query': {'filtered': {'filter': {'bool': {'must': clauses}}}}}

        hits = helpers.scan(self.es, query, '60m', index=self.DEFAULT_INDEX,
                            doc_type='association')
        for hit in hits:
            yield self._map_to_association(hit)
            
    def create_association(self, association):
        helpers.bulk(self.es, [self._to_create_message(association), ], raise_on_error=True)

    def update_association(self, association):
        if association.id is None or association.id == '':
            return

        action = {
            '_op_type': 'update',
            '_index': self.DEFAULT_INDEX,
            '_type': 'association',
            '_id': association.id,
            'doc': self._map_from_association(association)
        }

        helpers.bulk(self.es, [action, ], raise_on_error=True)

    def delete_association(self, association_id):
        action = {
            '_op_type': 'delete',
            '_index': self.DEFAULT_INDEX,
            '_type': 'association',
            '_id': association_id
        }

        helpers.bulk(self.es, [action, ], raise_on_error=True)

    @classmethod
    def _map_to_rumor(cls, data):
        if data is None:
            return None

        ttp = None
        actor = None

        source = data.get('_source')

        observable = cls._map_to_observable(source.get('observable'))
        valid = source.get('valid')
        document = cls._map_to_document(source.get('document'))
        if 'ttp' in source:
            ttp = cls._map_to_ttp(source.get('ttp'))
        if 'actor' in source:
            actor = cls._map_to_actor(source.get('actor'))
        campaign = cls._map_to_campaign(source.get('campaign'))
        description = source.get('description')

        id = data.get('_id')

        return Rumor(observable, valid, document, ttp, actor, campaign,
                     description, id)

    @classmethod
    def _map_from_rumor(cls, rumor):
        if rumor is None:
            return None

        return {
            'observable': cls._map_from_observable(rumor.observable),
            'valid': rumor.valid,
            'document': cls._map_from_document(rumor.document),
            'ttp': cls._map_from_ttp(rumor.ttp),
            'actor': cls._map_from_actor(rumor.actor),
            'campaign': cls._map_from_campaign(rumor.campaign),
            'description': rumor.description
        }

    @classmethod
    def _map_to_observable(cls, data):
        if data is None:
            return None

        return Observable(data.get('variety'),
                          data.get('value'),
                          data.get('id', None))

    @classmethod
    def _map_from_observable(cls, observable):
        return {
            'variety': observable.variety,
            'value': observable.value,
            'id': observable.id
        }

    @classmethod
    def _map_to_document(cls, data):
        if data is None:
            return None

        return Document(data.get('name', None),
                        data.get('category', None),
                        data.get('source', None),
                        data.get('investigator', None),
                        data.get('tlp', None),
                        data.get('noforn', None),
                        data.get('nocomm', None),
                        data.get('received', None),
                        data.get('text', None),
                        data.get('id', None))

    @classmethod
    def _map_from_document(cls, document):
        if document is None:
            return None

        return {
            'name': document.name,
            'category': document.category,
            'source': document.source,
            'investigator': document.investigator,
            'tlp': document.tlp,
            'noforn': document.noforn,
            'nocomm': document.nocomm,
            'received': document.received,
            'text': document.text,
            'id': document.id
        }

    @classmethod
    def _map_to_ttp(cls, data):
        if data is None:
            return None

        return TTP(data.get('category'),
                   data.get('actions', None),
                   data.get('malware', None),
                   data.get('id', None))

    @classmethod
    def _map_from_ttp(cls, ttp):
        if ttp is None:
            return None

        return {
            'category': ttp.category,
            'actions': ttp.actions,
            'malware': ttp.malware,
            'id': ttp.id
        }

    @classmethod
    def _map_to_actor(cls, data):
        if data is None:
            return None

        return Actor(data.get('name'),
                     data.get('id', None))

    @classmethod
    def _map_from_actor(cls, actor):
        if actor is None:
            return None

        return {
            'name': actor.name,
            'id': actor.id
        }

    @classmethod
    def _map_to_association(cls, data):
        if data is None:
            return None

        source = data.get('_source')

        left = cls._map_to_left(source.get('left'))
        right = cls._map_to_right(source.get('right'))
        document = cls._map_to_document(source.get('document'))
        variety = source.get('variety')
        valid = source.get('valid')
        description = source.get('description')
        id = data.get('_id')

        return Association(left, right, valid, document, variety, description,
                           id)

    @classmethod
    def _map_from_association(cls, association):
        if association is None:
            return None

        return {
            'left': cls._map_from_left(association.left),
            'right': cls._map_from_right(association.right),
            'valid': association.valid,
            'document': cls._map_from_document(association.document),
            'variety': association.variety,
            'description': association.description
        }

    @classmethod
    def _map_to_left(cls, data):
        if data is None:
            return None

        return Left(data.get('value'), data.get('variety'), data.get('_id'))

    @classmethod
    def _map_from_left(cls, left):
        if left is None:
            return None

        return {
            'value': left.value,
            'variety': left.variety
        }

    @classmethod
    def _map_to_right(cls, data):
        if data is None:
            return None

        return Right(data.get('value'), data.get('variety'), data.get('_id'))

    @classmethod
    def _map_from_right(cls, right):
        if right is None:
            return None

        return {
            'value': right.value,
            'variety': right.variety
        }

    @classmethod
    def _map_to_campaign(cls, data):
        if data is None:
            return None

        return Campaign(data.get('name'),
                        data.get('id', None))

    @classmethod
    def _map_from_campaign(cls, campaign):
        if campaign is None:
            return None

        return {
            'name': campaign.name,
            'id': campaign.id
        }

    def setup(self, index='iocdb_fixed_action_mapping_2'):
        es = Elasticsearch(self.config.elastic_search_hosts)
        es.indices.create(index=index, body=iocdb.mappings.es.iocdb)
        if not es.indices.exists_alias('iocdb'):
            es.indices.put_alias(index=index, name='iocdb')
        else:
            # TODO: this looks like an es bug, it does not filter by alias name
            old_indexes = es.indices.get_aliases(name=['iocdb'])
            old_indexes = [i for i, v in old_indexes.items()
                           if 'iocdb' in v['aliases']]
            if len(old_indexes) > 1:
                print('WARNING: iocdb has multiple indexes. '
                      'must update aliases manually')
                return
            actions = [{'add': {'index': index, 'alias': 'iocdb'}}]
            if len(old_indexes) == 1:
                actions.append(
                    {'remove': {'index': old_indexes[0], 'alias': 'iocdb'}})
            es.indices.update_aliases(body={'actions': actions})
