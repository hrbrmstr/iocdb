class SessionManager(object):

    def get_rumor_by_id(self, rumor_id):
        raise NotImplementedError

    def search_rumors(self, **kwargs):
        raise NotImplementedError

    def get_association_by_id(self, association_id):
        raise NotImplementedError

    def search_associations(self, **kwargs):
        raise NotImplementedError

    def handle_message(self, message):
        raise NotImplementedError


def get_session_manager(config, repo_type=None):
    if repo_type is None:
        repo_type = config.repository

    for cls in SessionManager.__subclasses__():
        if cls.is_manager_for(repo_type):
            return cls(config)
