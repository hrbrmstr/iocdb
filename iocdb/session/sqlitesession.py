__author__ = 'Andrew Barilla <andrew.barilla@one.verizon.com'

from session_manager import SessionManager


class SqliteSession(SessionManager):
    config = None

    def __init__(self, config):
        self.config = config

    @classmethod
    def is_manager_for(cls, name):
        return name == 'sqlite'