__author__ = 'abarilla'

from elasticsearch.exceptions import RequestError

from iocdb.config import Config

def main():
    config = Config()
    session = config.make_session()
    try:
        session.setup()
        print('IOCDB Elasticsearch index created')
    except RequestError:
        print('IOCDB Elasticsearch index already exists')

if __name__ == '__main__':
    main()
