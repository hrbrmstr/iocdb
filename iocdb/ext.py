__author__ = 'abarilla'

import json
import re
import sys
import urllib
import urllib2

import GeoIP
import shodan

from iocdb.utils import urlopen_with_retry

VT_API_KEY = '1f575a54bb21ed2d5d774e851435f59ec7f0c1dc75dc8b23315137fbb4dc83a3'
DNSDB_API_KEY = '9e56df7123a7c3262d1b7fcd6d960d63a3afefcfc09827457e61b3add1f06610'
SHODAN_API_KEY = "5xrAY9zcdsFBUySLUHTTsan80bbtdoN1"
FREE_GEOIP_JSON_URL = "http://freegeoip.net/json/%s"
DEBUG = True


def shodan_lookup(ip, data):
    api = shodan.Shodan(SHODAN_API_KEY)
    host = api.host(ip)

    data['os'] = host.get('os', 'n/a')
    data['ports'] = []

    for item in host['data']:
        data['ports'].append({'port': item['port'], 'data': item['data']})


def geo_locate(ip_addy, data):
    geoip_city = GeoIP.open('/usr/local/share/GeoIP/GeoLiteCity.dat', GeoIP.GEOIP_MEMORY_CACHE)
    geoip_asn = GeoIP.open('/usr/local/share/GeoIP/GeoIPASNum.dat', GeoIP.GEOIP_MEMORY_CACHE)
    geoip_organization = GeoIP.open('/usr/local/share/GeoIP/GeoIPOrg.dat', GeoIP.GEOIP_MEMORY_CACHE)

    city = geoip_city.record_by_addr(str(ip_addy))
    if city is None:
        return

    for key, value in city.items():
        if value is None:
            city[key] = 'NULL'

    # ::ASN:: #
    asn_id = None
    asn = geoip_asn.name_by_addr(str(ip_addy))
    if asn is not None:
        asn_work = asn.split(" ")
        asn_id = asn_work[0]

    # ::Organization:: #
    organization = geoip_organization.org_by_addr(str(ip_addy))
    org_range = "-".join(geoip_organization.range_by_ip(str(ip_addy)))

    # :: [0] - organization, [1] - asn_id, [2] - asn_org, [3] - org_netmask, [4] country_code, [5] - country_name, [6] - CITY, [7] STATE, [8] REGION, [9] - REGION_NAME, [10] - TIMEZONE
    # geolocated_list = [str(ip_addy),"/" + str(org_netmask),city['country_code'],city['country_name'],organization,org_range,str(asn_id),city['city'],city['region'],city['region_name'],city['time_zone']]

    data['geo'] = {
        'organization': organization,
        'ccode': city['country_code'],
        'country': city['country_name'],
        'range': org_range,
        'asn': unicode(asn_id),
        'city': city['city'],
        'region': city['region'],
        'rname': city['region_name'],
        'tz': city['time_zone'],
        'longitude': city['longitude'],
        'latitude': city['latitude']
    }




def get_geodata_json(ip):
    url = FREE_GEOIP_JSON_URL % ip
    resp = urlopen_with_retry(url)

    try:
        data = json.loads(resp.read())
        return {
            'latitude': data['latitude'],
            'longitude': data['longitude']
        }
    except:
        return {
            'latitude': 0,
            'longitude': 0
        }


# def smain(address):
#
#     scavenged_data = dict()
#
#     if re.match('\s*', address):
#         pass
#     elif address == '':
#         pass
#     scavenged_data['address'] = address
#     if re.match('\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}', address):
#         scavenged_data['type'] = 'ip'
#         if vt_api:
#             scavenged_data['virustotal'] = vt_ip_check(address, vt_api)
#         if dnsdb_api:
#             pdns_data = pdns_ip_check(address, dnsdb_api)
#             if pdns_data:
#                 scavenged_data['pdns'] = pdns_data
#     else:
#         scavenged_data['type'] = 'name'
#         if vt_api:
#             scavenged_data['virustotal'] = vt_name_check(address, vt_api)
#         if dnsdb_api:
#             pdns_data = pdns_name_check(address, dnsdb_api)
#             if pdns_data:
#                 scavenged_data['pdns'] = pdns_data
#
#     return scavenged_data


# Checks VirusTotal for occurrences of an IP address
def vt_ip_check(ip, data):
    try:
        if DEBUG:
            sys.stderr.write("Attempting VT retrieval for %s\n" % ip)
        url = 'https://www.virustotal.com/vtapi/v2/ip-address/report'
        parameters = {'ip': ip, 'apikey': VT_API_KEY}
        # TODO: use get_url() to get error handling and similar benefits
        response = urllib.urlopen('%s?%s' % (url, urllib.urlencode(parameters))).read()
        return_dict = json.loads(response)
        data['virus'] = return_dict
    except:
        return None


# Checks VirusTotal for occurrences of a domain name
def vt_name_check(domain):
    try:
        if DEBUG:
            sys.stderr.write("Attempting VT retrieval for %s\n" % domain)
        url = 'https://www.virustotal.com/vtapi/v2/domain/report'
        parameters = {'domain': domain, 'apikey': VT_API_KEY}
        response = urllib.urlopen('%s?%s' % (url, urllib.urlencode(parameters))).read()
        return_dict = json.loads(response)
        return return_dict
    except:
        return None


# Checks Farsight passive DNS for information on an IP address
def pdns_ip_check(ip, data):
    pdns_results = []
    url = 'https://api.dnsdb.info/lookup/rdata/ip/'+ip+'/a'

    req = urllib2.Request(url)
    req.add_header('Accept', 'application/json')
    req.add_header('X-Api-Key', DNSDB_API_KEY)

    if DEBUG:
        sys.stderr.write("Attempting pDNS retrieval for %s\n" % ip)
    response = get_url(req)
    if not response:
        return False
    pdns_json_all = response.read()
    if len(pdns_json_all) > 0:
        for pdns_json in pdns_json_all.rstrip('\n').split('\n'):
            try:
                answer = json.loads(pdns_json)
                if 'rrname' in answer:
                    pdns_results.append(answer)
            except:
                sys.stderr.write("Could not parse '%s' as JSON\n" % pdns_json)
                pass
        try:
            new_res = sorted(pdns_results, key=lambda k: ("time_last" not in k, k.get("time_last", None)), reverse=True)
        except:
            new_res = pdns_results
        data['dns'] = new_res
    else:
        return None


# Checks Farsight passive DNS for information on an IP address
def pdns_name_check(name):
    pdns_results = []
    url = 'https://api.dnsdb.info/lookup/rrset/name/'+name+'/a'

    req = urllib2.Request(url)
    req.add_header('Accept', 'application/json')
    req.add_header('X-Api-Key', DNSDB_API_KEY)

    if DEBUG:
        sys.stderr.write("Attempting pDNS retrieval for %s\n" % name)
    response = get_url(req)
    if not response:
        return False
    pdns_json_all = response.read()
    if len(pdns_json_all) > 0:
        for pdns_json in pdns_json_all.rstrip('\n').split('\n'):
            try:
                answer = json.loads(pdns_json)
                if 'rrname' in answer:
                    pdns_results.append(answer)
            except:
                sys.stderr.write("Could not parse '%s' as JSON\n" % pdns_json)
                pass
        new_res = sorted(pdns_results, key=lambda k: k['time_last'], reverse=True)
        return new_res
    else:
        return None


# Utility function to get a URL with error handling
# Accepts URL string or urllib2.Request object
def get_url(orig_request):
    if isinstance(orig_request, basestring):
        url = orig_request.encode('utf8')
        req = urllib2.Request(url)
    elif isinstance(orig_request, urllib2.Request):
        req = orig_request
    else:
        return None

    try:
        response = urllib2.urlopen(req)
    except urllib2.HTTPError as e:
        # sys.stderr.write("The server couldn't fulfill the request for URL %s: %s\n" % (request.get_full_url(), e))
        return None
    except urllib2.URLError as e:
        # sys.stderr.write('We failed to reach a server for URL %s: %s\n' % (request.get_full_url(), e))
        return None
    else:
        return response
