import logging
import socket

import celery

from iocdb.config import Config
import feeds

# load config
config = Config()

socket.setdefaulttimeout(config.url_timeout)

logging.info('initialized dispatcher with {}' . format(config.message_handlers))
page_size = 5000

# initialize app
include = [] 
include.extend('iocdb.feeds.' + feed for feed in feeds.__all__)
app = celery.Celery('iocdb-dispatcher', backend='amqp', include=include)
app.conf.CELERY_ROUTES = {
    'iocdb.feeds.migrate._migrate_range': {'queue': 'migrate'}
}


# tasks
def propagate(domain_objects, source=None, delay=False):
    for page in _get_paged(domain_objects, source):
        if delay:
            celery.group(
                handle_message.s(index, page, source)
                for index in range(len(config.message_handlers))
            )()
        else:
            for index in range(len(config.message_handlers)):
                handle_message(index, page, source)


def _get_paged(domain_objects, source):
    page = []
    count = 0
    for obj in domain_objects:
        if len(page) == page_size:
            yield page
            page = []
        page.append(obj)
        count += 1
    yield page
    logging.info('propagating {} objects from {}'.format(
        count, source))


@app.task(ignore_results=True)
def handle_message(index, domain_objects, source):
    message_handler = config.make_session(config.message_handlers[index])
    logging.info('handling {} objects from {}: {}'.format(
        len(domain_objects), source, message_handler))
    message_handler.handle_message(domain_objects)

if __name__ == '__main__':
    app.start()
