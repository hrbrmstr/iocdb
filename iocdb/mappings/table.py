import json

import mapping_tools
import sqlalchemy
import sqlalchemy.orm
from sqlalchemy import (
    Column, Integer, DateTime, Enum, Boolean)
import sqlalchemy.types

import iocdb.model

#--- sqla type extenstions
class Unicode(sqlalchemy.types.TypeDecorator):

    impl = sqlalchemy.types.Unicode

    def process_bind_param(self, value, dialect):
        if isinstance(value, str):
            value = value.decode('utf-8')
        return value

class UnicodeText(sqlalchemy.types.TypeDecorator):

    impl = sqlalchemy.types.UnicodeText

    def process_bind_param(self, value, dialect):
        if isinstance(value, str):
            value = value.decode('utf-8')
        return value

class JSONEncodedDict(sqlalchemy.types.TypeDecorator):

    impl = UnicodeText

    def process_bind_param(self, value, dialect):
        if value is not None:
            value = json.dumps(value)
        return value

    def process_result_value(self, value, dialect):
        if value is not None:
            try:
                value = json.loads(value)
            except ValueError:
                pass #TODO: legacy data is not necessarily json compliant
        return value

OBSERVABLE_VARIETY = Enum(*iocdb.model.OBSERVABLE_VARIETY, 
                          name='OBSERVABLE_VARIETY')
DOCUMENT_CATEGORY = Enum(*iocdb.model.DOCUMENT_CATEGORY,
                         name='DOCUMENT_CATEGORY')
TLP = Enum(*iocdb.model.TLP, name='TLP')
TTP_CATEGORY = Enum(*iocdb.model.TTP_CATEGORY, name='TTP_CATEGORY')

sql_metadata = sqlalchemy.MetaData()

#--- relational tables
rumors_relation = sqlalchemy.Table(
    'rumors', sql_metadata,
    Column('rumor_id', Integer, primary_key=True),
    Column('valid', DateTime, nullable=False, index=True),
    Column('description', UnicodeText),
    Column('observable_id', Integer,
           sqlalchemy.ForeignKey('observables.observable_id')),
    Column('document_id', Integer,
           sqlalchemy.ForeignKey('documents.document_id')),
    Column('ttp_id', Integer, sqlalchemy.ForeignKey('ttps.ttp_id')),
    Column('actor_id', Integer, sqlalchemy.ForeignKey('actors.actor_id')),
    Column('campaign_id', Integer,
           sqlalchemy.ForeignKey('campaigns.campaign_id')))

rumors_relation_map = sqlalchemy.orm.mapper(
    iocdb.model.Rumor, rumors_relation, {
        'id':rumors_relation.c.rumor_id,
        'observable':sqlalchemy.orm.relationship(iocdb.model.Observable),
        'document':sqlalchemy.orm.relationship(iocdb.model.Document),
        'ttp':sqlalchemy.orm.relationship(iocdb.model.TTP),
        'actor':sqlalchemy.orm.relationship(iocdb.model.Actor),
        'campaign':sqlalchemy.orm.relationship(iocdb.model.Campaign)})

observables_relation = sqlalchemy.Table(
    'observables', sql_metadata,
    Column('observable_id', Integer, primary_key=True),
    Column('variety', OBSERVABLE_VARIETY,
           nullable=False, index=True),
    Column('value', Unicode(50), nullable=False, index=True),
    Column('value_text', UnicodeText))

observables_relation_map = sqlalchemy.orm.mapper(
    iocdb.model.Observable, observables_relation, {
        'id':observables_relation.c.observable_id,
        'variety':observables_relation.c.variety,
        'value':sqlalchemy.orm.column_property(
            observables_relation.c.value, observables_relation.c.value_text)
    })

documents_relation = sqlalchemy.Table(
    'documents', sql_metadata,
    Column('document_id', Integer, primary_key=True),
    Column('name', Unicode(100), nullable=False, index=True),
    Column('name_text', UnicodeText),
    Column('received', DateTime, nullable=False, index=True),
    Column('source', Unicode(50), nullable=False, index=True),
    Column('category', DOCUMENT_CATEGORY),
    Column('tlp', TLP, nullable=False),
    Column('noforn', Boolean),
    Column('nocomm', Boolean),
    Column('investigator', Unicode(50), index=True),
    Column('text', UnicodeText))

documents_relation_map = sqlalchemy.orm.mapper(
    iocdb.model.Document, documents_relation, {
        'id':documents_relation.c.document_id,
        'name': sqlalchemy.orm.column_property(
            documents_relation.c.name, documents_relation.c.name_text),
        'category':documents_relation.c.category,
        'tlp':documents_relation.c.tlp
    })

ttps_relation = sqlalchemy.Table(
    'ttps', sql_metadata,
    Column('ttp_id', Integer, primary_key=True),
    Column('malware', Unicode(50), index=True),
    Column('category', TTP_CATEGORY,
           nullable=False, index=True),
    Column('actions', UnicodeText))
    
ttps_relation_map = sqlalchemy.orm.mapper(
    iocdb.model.TTP, ttps_relation, {
        'id':ttps_relation.c.ttp_id,
        'category':ttps_relation.c.category,
        'actions':ttps_relation.c.actions
    })

actors_relation = sqlalchemy.Table(
    'actors', sql_metadata,
    Column('actor_id', Integer, primary_key=True),
    Column('name', Unicode(50), index=True))

actors_relation_map = sqlalchemy.orm.mapper(
    iocdb.model.Actor, actors_relation, {
        'id':actors_relation.c.actor_id
    })

campaigns_relation = sqlalchemy.Table(
    'campaigns', sql_metadata,
    Column('campaign_id', Integer, primary_key=True),
    Column('name', Unicode(50), index=True))

campaign_relation_map = sqlalchemy.orm.mapper(
    iocdb.model.Campaign, campaigns_relation, {
        'id':campaigns_relation.c.campaign_id
    })
#--- aggregate tables

rumors_aggregate_table = sqlalchemy.Table(
    'rumors_mv', sql_metadata,
    Column('rumor_id', Integer, primary_key=True),
    Column('valid', DateTime, nullable=False, index=True),
    Column('description', UnicodeText),
    Column('observable_id', Integer, nullable=False, index=True),
    Column('observable_variety', OBSERVABLE_VARIETY, 
           nullable=False, index=True),
    Column('observable_value', Unicode(50), nullable=False, index=True),
    Column('observable_value_text', UnicodeText),
    Column('document_id', Integer, nullable=False, index=True),
    Column('document_name', Unicode(100), nullable=False, index=True),
    Column('document_name_text', UnicodeText),
    Column('document_received', DateTime, nullable=False, index=True),
    Column('document_source', Unicode(50), nullable=False, index=True),
    Column('document_category', DOCUMENT_CATEGORY),
    Column('document_tlp', TLP, nullable=False),
    Column('document_noforn', Boolean),
    Column('document_nocomm', Boolean),
    Column('document_investigator', Unicode(50), index=True),
    Column('document_text', UnicodeText),
    Column('ttp_id', Integer, nullable=False, index=True),
    Column('ttp_malware', Unicode(50), index=True),
    Column('ttp_category', TTP_CATEGORY, nullable=False, index=True),
    Column('ttp_actions', UnicodeText),
    Column('actor_id', Integer, index=True),
    Column('actor_name', Unicode(50), index=True),
    Column('campaign_id', Integer, index=True),
    Column('campaign_name', Unicode(50), index=True))

class RumorAggregate(object):
    
    def __init__(self, rumor_id, valid, description, observable_id,
                 observable_variety, observable_value, document_id,
                 document_name, document_received, document_source,
                 document_category, document_tlp, document_noforn,
                 document_nocomm, document_investigator, document_text,
                 ttp_id, ttp_malware, ttp_category, ttp_actions, actor_id,
                 actor_name, campaign_id, campaign_name):
        self.rumor_id = rumor_id
        self.valid = valid
        self.description = description
        self.observable_id = observable_id
        self.observable_variety = observable_variety
        self.observable_value = observable_value
        self.document_id = document_id
        self.document_name = document_name
        self.document_received = document_received
        self.document_source = document_source
        self.document_category = document_category
        self.document_tlp = document_tlp
        self.document_noforn = document_noforn
        self.document_investigator = document_investigator
        self.document_text = document_text
        self.ttp_id = ttp_id
        self.ttp_malware = ttp_malware
        self.ttp_category = ttp_category
        self.ttp_actions = ttp_actions
        self.actor_id = actor_id
        self.actor_name = actor_name
        self.campaign_id = campaign_id
        self.campaign_name = campaign_name


rumor_aggregaten_relation_map = sqlalchemy.orm.mapper(
    RumorAggregate, rumors_aggregate_table, {
        'id':rumors_aggregate_table.c.rumor_id,
        'observable_value':sqlalchemy.orm.column_property(
            rumors_aggregate_table.c.observable_value,
            rumors_aggregate_table.c.observable_value_text),
        'document_name':sqlalchemy.orm.column_property(
            rumors_aggregate_table.c.document_name,
            rumors_aggregate_table.c.document_name_text)
    })

rumors_aggregate_mapping = mapping_tools.Mapper(RumorAggregate, {
    ('valid', 'description'):mapping_tools.identity,
    'id':mapping_tools.make_rotation('rumor_id'),
    'observable':mapping_tools.make_projection(iocdb.model.Observable),
    'document':mapping_tools.make_projection(iocdb.model.Document),
    'ttp':mapping_tools.make_projection(iocdb.model.TTP),
    'actor':mapping_tools.make_projection(iocdb.model.Actor),
    'campaign':mapping_tools.make_projection(iocdb.model.Campaign)})
                 
observable_properties = ('observable_id', 'observable_variety',
                         'observable_value_text')
document_properties = ('document_id', 'document_name_text',
                       'document_received', 'document_source',
                       'document_category', 'document_tlp',
                       'document_noforn', 'document_nocomm',
                       'document_investigator', 'document_text')
ttp_properties = ('ttp_id', 'ttp_malware', 'ttp_category', 'ttp_actions')
actor_properties = ('actor_id', 'actor_name')
campaign_properties = ('campaign_id', 'campaign_name')
aggregate_rumor_schema = mapping_tools.Mapper(iocdb.model.Rumor, {
    'rumor_id':mapping_tools.make_rotation('id'),
    ('valid', 'description'):mapping_tools.identity,
    observable_properties:mapping_tools.make_constructor(
        iocdb.model.Observable, 'observable',
        {'observable_value_text':'value'}),
    document_properties:mapping_tools.make_constructor(
        iocdb.model.Document, 'document',
        {'document_name_text':'name'}),
    ttp_properties:mapping_tools.make_constructor(
        iocdb.model.TTP, 'ttp'),
    actor_properties:mapping_tools.make_constructor(
        iocdb.model.Actor, 'actor'),
    campaign_properties:mapping_tools.make_constructor(
        iocdb.model.Campaign, 'campaign')})
    

