iocdb = {
    "mappings" : {
        "association" : {
            "_id" : {"path" : "id" },
            "properties" : {
              "left" : {
                "properties": {
                  "value": {
                    "type": "multi_field",
                    "fields":{
                      "value":{"type":"string", "index": "analyzed"},
                      "untouched":{"type" : "string", "index" : "not_analyzed"}
                    }
                  },
                  "variety": {
                    "type": "string", "index": "not_analyzed"
                  }
                }
              },
              "right" : {
                "properties": {
                  "value": {
                    "type": "multi_field",
                    "fields":{
                      "value":{"type":"string", "index": "analyzed"},
                      "untouched":{"type" : "string", "index" : "not_analyzed"}
                    }
                  },
                  "variety": {
                    "type": "string", "index": "not_analyzed"
                  }
                }
              },
              "document" : {
                "properties" : {
                  "source" : {
                    "type" : "string", "index" : "not_analyzed"
                  },
                  "name" : {
                    "type": "multi_field",
                    "fields":{
                      "name":{"type":"string", "index": "analyzed"},
                      "untouched":{"type" : "string", "index" : "not_analyzed"}
                    }
                  },
                  "investigator" : {
                    "type" : "string", "index" : "not_analyzed"
                  }
                }
              },
              "variety": {
                "type": "string", "index": "not_analyzed"
              }
            }
        },
        "rumor" : {
            "_id" : { "path" : "id" },
            "properties" : {
              "observable": {
                "properties": {
                  "value": {
                    "type": "multi_field",
                    "fields":{
                      "value":{"type":"string", "index": "analyzed"},
                      "untouched":{"type" : "string", "index" : "not_analyzed"}
                    }
                  },
                  "variety": {
                    "type": "string", "index": "not_analyzed"
                  }
                }
              },
              "document" : {
                "properties" : {
                  "source" : {
                    "type" : "string", "index" : "not_analyzed"
                  },
                  "name" : {
                    "type": "multi_field",
                    "fields":{
                      "name":{"type":"string", "index": "analyzed"},
                      "untouched":{"type" : "string", "index" : "not_analyzed"}
                    }
                  },
                  "investigator" : {
                    "type" : "string", "index" : "not_analyzed"
                  },
                }
              },
              "ttp" : {
                "properties" : {
                  "actions" : { "type" : "string" },
                  "malware" : {
                    "type" : "string", "index" : "not_analyzed"
                  }
                }
              },
              "actor" : {
                "properties" : {
                  "name" : {
                    "type" : "string", "index" : "not_analyzed"
                  } 
                }
              },
              "campaign" : {
                "properties" : {
                  "name" : {
                    "type" : "string", "index" : "not_analyzed"
                  } 
                }
              }
            }
        }
    }
}

