'''mapping for "legacy" data model'''

import mapping_tools

import iocdb.model

observable_mapping = mapping_tools.Mapper(iocdb.model.Observable, {
    'uid':mapping_tools.make_rotation('id'),
    'variety':mapping_tools.identity,
    'value_text':mapping_tools.make_rotation('value')})

document_mapping = mapping_tools.Mapper(iocdb.model.Document, {
    'uid':mapping_tools.make_rotation('id'),
    'name_text':mapping_tools.make_rotation('name'),
    ('category', 'source', 'investigator', 'tlp', 'noforn', 'nocomm',
     'received', 'text'):mapping_tools.identity})

ttp_mapping = mapping_tools.Mapper(iocdb.model.TTP, {
    'uid':mapping_tools.make_rotation('id'),
    ('category', 'actions', 'malware'):mapping_tools.identity})

actor_mapping = mapping_tools.Mapper(iocdb.model.Actor, {
    'uid':mapping_tools.make_rotation('id'),
    'name':mapping_tools.identity})
    
campaign_mapping = mapping_tools.Mapper(iocdb.model.Campaign, {
    'uid':mapping_tools.make_rotation('id'),
    'name':mapping_tools.identity})
    
rumor_mapping = mapping_tools.Mapper(iocdb.model.Rumor, {
    'uid':mapping_tools.make_rotation('id'),
    ('valid', 'description'):mapping_tools.identity,
    'observable':mapping_tools.make_map(observable_mapping),
    'document':mapping_tools.make_map(document_mapping),
    'ttp':mapping_tools.make_map(ttp_mapping),
    'actor':mapping_tools.make_map(actor_mapping),
    'campaign':mapping_tools.make_map(campaign_mapping)})

association_mapping = mapping_tools.Mapper(iocdb.model.Association, {
    'uid':mapping_tools.make_rotation('id'),
    ('valid', 'description', 'variety'):mapping_tools.identity,
    'left':mapping_tools.make_map(observable_mapping),
    'right':mapping_tools.make_map(observable_mapping),
    'document':mapping_tools.make_map(document_mapping)})
