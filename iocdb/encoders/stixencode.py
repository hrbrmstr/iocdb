import types

import cybox.core
import cybox.common.time
import stix.campaign
import stix.ttp
import stix.indicator.indicator
from stix.data_marking import Marking
from stix.data_marking import MarkingSpecification
from stix.core import STIXHeader
from stix.core import STIXPackage
from stix.common import InformationSource
from stix.common import Identity
from stix.extensions.marking.simple_marking import SimpleMarkingStructure
from stix.extensions.marking.tlp import TLPMarkingStructure
from stix.ttp.attack_pattern import AttackPattern
from stix.ttp.malware_instance import MalwareInstance

from iocdb.model import Rumor


ACTIONS_CAPEC_MAPPING = [
    ('3rd party desktop', 'n/a'),
    ('Abuse of functionality', 'n/a'),
    ('Adminware', 'n/a'),
    ('Adware', 'n/a'),
    ('Assault', 'n/a'),
    ('Auditor', 'n/a'),
    ('Backdoor', 'n/a'),
    ('Backdoor or C2', 'n/a'),
    ('Baiting', 'n/a'),
    ('Bribery', 'n/a'),
    ('Brute force', 'CAPEC-112'),
    ('Buffer overflow', 'capec-10'),
    ('Bypassed controls', 'n/a'),
    ('C2', 'n/a'),
    ('CSRF', 'capec-62'),
    ('Cache poisoning', 'capec-141'),
    ('Call center', 'n/a'),
    ('Capture app data', 'capec-65'),
    ('Capture stored data', 'capec-65'),
    ('Cashier', 'n/a'),
    ('Click fraud', 'capec-386'),
    ('Client-side attack', 'capec-22'),
    ('Command shell', 'capec-88'),
    ('Connection', 'n/a'),
    ('Cryptanalysis', 'capec-97'),
    ('Customer', 'n/a'),
    ('Data mishandling', 'n/a'),
    ('Desktop sharing', 'n/a'),
    ('Destroy data', 'n/a'),
    ('Developer', 'n/a'),
    ('Direct install', 'n/a'),
    ('Disable controls', 'n/a'),
    ('Disabled controls', 'n/a'),
    ('DoS', 'capec-343'),
    ('Documents', 'n/a'),
    ('Download by malware', 'n/a'),
    ('Downloader', 'n/a'),
    ('Elicitation', 'capec-410'),
    ('Email', 'capec-539'),
    ('Email attachment', 'capec-539'),
    ('Email autoexecute', 'capec-163'),
    ('Email link', 'capec-163'),
    ('Email misuse', 'capec-163'),
    ('Embezzlement', 'n/a'),
    ('End-user', 'n/a'),
    ('Executive', 'n/a'),
    ('Exploit vuln', 'n/a'),
    ('Export data', 'capec-65'),
    ('Extortion', 'n/a'),
    ('Finance', 'n/a'),
    ('Footprinting', 'capec-169'),
    ('Forced browsing', 'capec-87'),
    ('Forgery', 'n/a'),
    ('Format string attack', 'capec-135'),
    ('Former employee', 'n/a'),
    ('Fuzz testing', 'capec-28'),
    ('Guard', 'n/a'),
    ('HTTP Response Splitting', 'capec-34'),
    ('HTTP request smuggling', 'capec-273'),
    ('HTTP request splitting', 'capec-34'),
    ('HTTP response smuggling', 'capec-34'),
    ('Helpdesk', 'n/a'),
    ('Human resources', 'na'),
    ('IM', 'na'),
    ('Illicit content', 'n/a'),
    ('In-person', 'n/a'),
    ('Influence', 'n/a'),
    ('Instant messaging', 'n/a'),
    ('Integer overflows', 'capec-92'),
    ('Knowledge abuse', 'n/a'),
    ('LAN access', 'n/a'),
    ('LDAP injection', 'capec-136'),
    ('Mail command injection', 'capec-183'),
    ('Maintenance', 'n/a'),
    ('Manager', 'n/a'),
    ('MitM', 'capec-94'),
    ('Net misuse', 'n/a'),
    ('Network propagation', 'n/a'),
    ('Non-corporate', 'n/a'),
    ('Null byte injection', 'capec-361'),
    ('OS commanding', 'capec-364'),
    ('Offline cracking', 'capec-55'),
    ('Other', 'n/a'),
    ('Packet sniffer', 'n/a'),
    ('Partner', 'n/a'),
    ('Partner facility', 'n/a'),
    ('Partner vehicle', 'n/a'),
    ('Password dumper', 'n/a'),
    ('Path traversal', 'capec-126'),
    ('Personal residence', 'n/a'),
    ('Personal vehicle', 'n/a'),
    ('Phishing', 'capec-98'),
    ('Phone', 'n/a'),
    ('Physical access', 'n/a'),
    ('Pretexting', 'capec-407'),
    ('Privilege abuse', 'capec-122'),
    ('Privileged access', 'n/a'),
    ('Propaganda', 'n/a'),
    ('Public facility', 'n/a'),
    ('Public vehicle', 'n/a'),
    ('RFI', 'capec-338'),
    ('Ram scraper', 'n/a'),
    ('Ransomware', 'n/a'),
    ('Remote access', 'n/a'),
    ('Remote injection', 'capec-135'),
    ('Removable media', 'n/a'),
    ('Reverse engineering', 'n/a'),
    ('Rootkit', 'n/a'),
    ('Routing detour', 'capec-219'),
    ('SMS', 'n/a'),
    ('SQL injection', 'capec-66'),
    ('SQLi', 'capec-66'),
    ('SSI injection', 'capec-101'),
    ('Sabotage', 'n/a'),
    ('Scam', 'n/a'),
    ('Scan network', 'capec-300'),
    ('Session fixation', 'capec-61'),
    ('Session prediction', 'capec-351'),
    ('Session replay', 'capec-60'),
    ('Snooping', 'n/a'),
    ('Soap array abuse', 'capec-368'),
    ('Social media', 'n/a'),
    ('Software', 'n/a'),
    ('Spam', 'n/a'),
    ('Special element injection', 'n/a'),
    ('Spyware/Keylogger', 'n/a'),
    ('Surveillance', 'n/a'),
    ('System admin', 'n/a'),
    ('Tampering', 'n/a'),
    ('Theft', 'n/a'),
    ('URL redirector abuse', 'capec-371'),
    ('Unapproved hardware', 'n/a'),
    ('Unapproved software', 'n/a'),
    ('Unapproved workaround', 'n/a'),
    ('Uncontrolled location', 'n/a'),
    ('Unknown', 'n/a'),
    ('Use of backdoor or C2', 'n/a'),
    ('Use of stolen creds', 'n/a'),
    ('VPN', 'n/a'),
    ('Victim grounds', 'n/a'),
    ('Victim public area', 'n/a'),
    ('Victim secure area', 'n/a'),
    ('Victim work area', 'n/a'),
    ('Virtual machine escape', 'n/a'),
    ('Visitor privileges', 'n/a'),
    ('Web application', 'n/a'),
    ('Web download', 'n/a'),
    ('Web drive-by', 'n/a'),
    ('Website', 'n/a'),
    ('Wiretapping', 'n/a'),
    ('Worm', 'n/a'),
    ('XML attribute blowup', 'capec-229'),
    ('XML entity expansion', 'capec-197'),
    ('XML external entities', 'capec-221'),
    ('XML injection', 'capec-250'),
    ('XPath injection', 'capec-83'),
    ('XQuery injection', 'capec-84'),
    ('XSS', 'capec-341'),
]


def get_capec_for_action(action):
    if action is None:
        return None

    for key, value in ACTIONS_CAPEC_MAPPING:
        if key.lower() in action.lower():
            return value
    return None


class STIXEncoder(object):

    def convert(self, data, package_up=True):
        if isinstance(data, Rumor):
            indicator = rumor_to_indicator(data)
            if package_up:
                package = STIXPackage()
                package.add_indicator(indicator)
                return package
            else:
                return indicator
        elif isinstance(data, (list, types.GeneratorType)):
            package = STIXPackage()
            for obj in data:
                package.add(self.convert(obj, package_up=False))
            return package
        else:
            return None


def rumor_to_indicator(rumor):
    indicator = stix.indicator.indicator.Indicator(
        id_=rumor.id,
        description=rumor.description
    )

    indicator.add_valid_time_position(rumor.valid)

    observable = cybox.core.Observable(
        id_=rumor.observable.id,
        title=rumor.observable.value,
        description=rumor.observable.variety
    )
    indicator.add_observable(observable)

    if rumor.campaign is not None:
        campaign = stix.campaign.Campaign(
            id_=rumor.campaign.id,
            title=rumor.campaign.name
        )
        indicator.related_campaigns.append(campaign)

    # stix_package = stix.core.STIXPackage()

    # stix_header = STIXHeader()
    # stix_header.title = data.document.name

    if rumor.actor is not None:
        actor = stix.threat_actor.ThreatActor(
            id_=rumor.actor.id,
            title=rumor.actor.name
        )
        # stix_package.add_threat_actor(actor)

    source_time = cybox.common.time.Time(
        received_time=rumor.document.received
    )

    information_source = InformationSource(
        description=rumor.document.source,
        time=source_time,
        identity=Identity(name=rumor.document.name)
    )
    information_source.contributing_sources.append(rumor.document.investigator)
    # stix_header.information_source = information_source
    indicator.producer = information_source

    handling = Marking()
    marking_spec = MarkingSpecification()
    if rumor.document.nocomm:
        marking_spec.marking_structures.append(SimpleMarkingStructure(statement="nocomm"))
    if rumor.document.noforn:
        marking_spec.marking_structures.append(SimpleMarkingStructure(statement="noforn"))
    marking_spec.marking_structures.append(TLPMarkingStructure(color=rumor.document.tlp))
    handling.add_marking(marking_spec)
    # stix_header.handling = handling
    # stix_package.stix_header = stix_header

    indicator.handling = handling

    ttp = stix.ttp.TTP()
    ttp.behavior = stix.ttp.Behavior()
    attack_pattern = AttackPattern(title=rumor.ttp.actions, description=rumor.ttp.actions)
    attack_pattern.capec_id = get_capec_for_action(rumor.ttp.actions)
    ttp.behavior.attack_patterns.append(attack_pattern)
    malware = MalwareInstance()
    malware.names = rumor.ttp.malware
    ttp.behavior.add_malware_instance(malware)
    ttp.title = rumor.ttp.category

    indicator.add_indicated_ttp(ttp)

    return indicator

