__author__ = 'Andrew Barilla <andrew.barilla@one.verizon.com'

import time

from iocdb.model import Summary


class INTELSummaryEncoderSession(object):

    master_final = {}
    reporting_count = {}
    obs_tlps = {}

    def __enter__(self):
        self.master_final = {}
        self.reporting_count = {}
        self.obs_tlps = {}
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def get_summary(self, data):

        for obj in data:
            observable = obj.observable.value if obj.observable is not None else None
            doc_cat = obj.document.category if obj.document is not None else None
            doc_forn = obj.document.noforn if obj.document is not None else None
            doc_tlp = obj.document.tlp if obj.document is not None else None
            ttp_cat = obj.ttp.category if obj.ttp is not None else None
            ttp_action = obj.ttp.actions if obj.ttp is not None else None
            desc = obj.description
            doc_source = obj.document.source if obj.document is not None else None
            doc_name = obj.document.name if obj.document is not None else None
            valid = str(obj.valid).split('T')[0]
            actor = obj.actor.name if obj.actor is not None else None
            malware = obj.ttp.malware if obj.ttp is not None else None

            tmp_key = (observable, doc_tlp, ttp_cat, doc_cat, doc_source)
            date = int(time.mktime(time.strptime(valid, '%Y-%m-%d')))

            if tmp_key in self.master_final:
                first = self.master_final[tmp_key][0][0]
                last = self.master_final[tmp_key][0][1]

                if date < first:
                    prev_time = self.master_final[tmp_key][0][0]
                    self.master_final[tmp_key][0][0] = date
                    self.master_final[tmp_key][2] += 1
                    self.reporting_count[tmp_key].append(date)

                if date > last:
                    prev_time = self.master_final[tmp_key][0][0]
                    self.master_final[tmp_key][0][1] = date
                    self.master_final[tmp_key][2] += 1
                    self.reporting_count[tmp_key].append(date)

                self.master_final[tmp_key][1][4].append(malware)
                self.master_final[tmp_key][1][5].append(actor)

            else:
                self.master_final[tmp_key] = \
                    [[date, date],
                     [doc_forn,
                      ttp_action,
                      desc,
                      doc_name,
                      [malware],
                      [actor]],
                     1]
                self.reporting_count[tmp_key] = [date]

            if observable in self.obs_tlps:
                self.obs_tlps[observable].add(doc_tlp)
            else:
                self.obs_tlps[observable] = set()
                self.obs_tlps[observable].add(doc_tlp)

        self.quantify_records()
        return self.generate_summary()

    def quantify_records(self):
        for key, value in self.reporting_count.items():
            value = sorted(value)
            tmp_days = []
            for i in range(len(value)):
                if value[i] == value[-1]:
                    pass
                else:
                    tmp_days.append((value[i+1] - value[i]) / 86400)
            if len(tmp_days) > 0:
                min_day = str(min(tmp_days))
                max_day = str(max(tmp_days))
                avg = str(sum(tmp_days) / len(tmp_days))
                self.reporting_count[key] = {'avg': avg,
                                             'min': min_day,
                                             'max': max_day}
            else:
                self.reporting_count[key] = {'avg': "0",
                                             'min': "0",
                                             'max': "0"}

    def generate_summary(self):
        for key, value in self.master_final.items():

            summary_report = Summary()
            summary_report.observable = key[0]
            summary_report.document_category = key[3]
            summary_report.ttp_category = key[2]
            summary_report.ttp_actions = value[1][1]
            summary_report.document_source = key[4]
            summary_report.days_reported = str(value[2])
            summary_report.avg_gap = self.reporting_count[key]['avg']
            summary_report.min_gap = self.reporting_count[key]['min']
            summary_report.max_gap = self.reporting_count[key]['max']
            summary_report.tlp_conflict = 'no'
            summary_report.document_noforn = value[1][0]
            summary_report.document_tlp = key[1]
            summary_report.document_name = value[1][3]
            summary_report.description = value[1][2]
            summary_report.first_seen = \
                time.strftime('%Y-%m-%d', time.localtime(value[0][0]))
            summary_report.last_seen = \
                time.strftime('%Y-%m-%d', time.localtime(value[0][1]))
            summary_report.actor_name = \
                ':'.join(list(set(filter(None, value[1][5]))))
            summary_report.ttp_malware = \
                ':'.join(list(set(filter(None, value[1][4]))))

            if len(self.obs_tlps[summary_report.observable]) > 1:
                summary_report.tlp_conflict = 'yes'

            yield summary_report
