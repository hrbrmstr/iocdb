__author__ = 'v019420'


class ParametersRequiredError(StandardError):
    def __init__(self):
        super(ParametersRequiredError, self).__init__('At least one query parameter is required')


class InvalidRequestError(StandardError):
    def __init__(self):
        super(InvalidRequestError, self).__init__('No valid request entered')


class UnknownEncoderError(StandardError):
    def __init__(self):
        super(UnknownEncoderError, self).__init__('Unknown encoder specified')


class InvalidSettingsFileError(StandardError):
    def __init__(self):
        super(InvalidSettingsFileError, self).__init__('Invalid settings file specified')