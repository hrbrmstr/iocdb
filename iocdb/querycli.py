#!/usr/bin/env python
"""
Extract data from a query repo using provided query args and encode results.
"""


# samples --observable_values 122.117.144.2

import argparse
import datetime
import json

import iocdb.config
from iocdb.buildinfo import BuildInfo
from iocdb.config import Config
from iocdb.errors import UnknownEncoderError
from iocdb.encoders import INTELSummaryEncoderSession
from iocdb.utils import get_csv_output
from iocdb.encoders import CustomJSONEncoder
from iocdb.encoders import STIXEncoder


def main():
    config, args = parse_args()

    if args.model == 'rumors':
        search_rumors(config, args)
    else:
        search_associations(config, args)


def search_rumors(config, args):
    session = config.make_session()
    if args.object_id is not None:
        data = session.get_rumor_by_id(args.object_id)
    else:
        if args.observable_file is not None:
            if args.observable_values is None:
                args.observable_values = []

        if args.observable_file is not None:
            args.observable_values.extend(unicode(val.strip()) for val in args.observable_file)

        observable_whitelist = None
        if args.observable_whitelist is not None:
            observable_whitelist = [unicode(val.strip()) for val in args.observable_whitelist]

        data = session.search_rumors(observable_values=args.observable_values,
                                     valid_starting=args.valid_starting,
                                     valid_ending=args.valid_ending,
                                     received_starting=args.received_starting,
                                     received_ending=args.received_ending,
                                     observable_varieties=args.observable_varieties,
                                     document_sources=args.document_sources,
                                     document_names=args.document_names,
                                     document_investigators=args.document_investigators,
                                     actor_names=args.actor_names,
                                     campaign_names=args.campaign_names,
                                     ttp_malware=args.ttp_malware,
                                     ttp_categories=args.ttp_categories,
                                     observable_whitelist=observable_whitelist)
    output(data, args)


def search_associations(config, args):
    session = config.make_session()
    data = session.search_associations(valid_starting=args.valid_starting,
                                       valid_ending=args.valid_ending,
                                       received_starting=args.received_starting,
                                       received_ending=args.received_ending)
    output(data, args)


def output(data, args):
    if args.encoder == 'json':
        print json.dumps(data, cls=CustomJSONEncoder, indent=2,
                         sort_keys=True)
    elif args.encoder == 'csv':
        print get_csv_output(data, args.csv_columns)
    elif args.encoder == 'summary':
        with INTELSummaryEncoderSession() as summary_encoder:
            data = summary_encoder.get_summary(data)
            if args.summary_json:
                print json.dumps(data, cls=CustomJSONEncoder, indent=2,
                                 sort_keys=True)
            else:
                print get_csv_output(data)

    elif args.encoder == 'stix':
        encoder = STIXEncoder()
        print encoder.convert(data).to_xml()
    else:
        raise UnknownEncoderError


def parse_args(commandline=None):
    parser = argparse.ArgumentParser(description='Extract data from a query repo '
                                                 'using provided query args and encode results.',
                                     version=BuildInfo.get_version_detailed())

    group = parser.add_argument_group('config')
    group.add_argument('--config', help='path to config file')

    group = parser.add_argument_group('logging')
    group.add_argument('--loglevel',
                       choices=('DEBUG', 'INFO', 'WARNING', 'ERROR'))

    group.add_argument_group('repository session manager')
    group.add_argument('--repository', help='repository session manager key',
                       choices=iocdb.config.REPOSITORY_CHOICES)

    group.add_argument_group('encoder session manager')
    group.add_argument('--encoder', help='encoder session manager key',
                       choices=iocdb.config.ENCODER_CHOICES, default='json')
    group.add_argument('--summary_json', action='store_true', default=False)
    group.add_argument('--csv_columns', nargs='*', metavar='COLUMNNAME')

    group = parser.add_argument_group(
        'query args',
        'valid DATETIME formats are yyyy-mm-dd or yyyy-mm-ddThh:mm:ss')
    group.add_argument(
        'model', default='rumors', choices=['rumors', 'associations'],
        nargs='?', help='name of model to query')
    group.add_argument(
        '--id', type=unicode, nargs='?', metavar='TEXT')
    group.add_argument(
        '--observable_varieties', type=unicode, nargs='*', metavar='TEXT')
    group.add_argument(
        '--observable_values', type=unicode, nargs='*', metavar='TEXT')
    group.add_argument(
        '--observable_file', type=argparse.FileType(), metavar='FILE',
        help='query newline seperated observable values found in this file')
    group.add_argument(
        '--observable_whitelist', type=argparse.FileType(), metavar='FILE',
        help='ignore newline seperated observable values found in this file')
    group.add_argument(
        '--document_sources', type=unicode, nargs='*', metavar='TEXT')
    group.add_argument(
        '--document_names', type=unicode, nargs='*', metavar='TEXT')
    group.add_argument(
        '--document_tlps', type=unicode, nargs='*', metavar='TEXT')
    group.add_argument(
        '--document_investigators', type=unicode, nargs='*', metavar='TEXT')
    group.add_argument(
        '--valid_starting', nargs='?',
        type=_cli_datetime, metavar='DATETIME', help='(inclusive)')
    group.add_argument(
        '--valid_ending', nargs='?', type=_cli_datetime, metavar='DATETIME',
        help='(exclusive)')
    group.add_argument(
        '--received_starting', nargs='?',
        type=_cli_datetime, metavar='DATETIME', help='(inclusive)')
    group.add_argument(
        '--received_ending', nargs='?', type=_cli_datetime, metavar='DATETIME',
        help='(exclusive)')
    group.add_argument(
        '--actor_names', type=unicode,  nargs='*', metavar='TEXT')
    group.add_argument(
        '--campaign_names', type=unicode, nargs='*', metavar='TEXT')
    group.add_argument(
        '--ttp_malware', type=unicode, nargs='*', metavar='TEXT')
    group.add_argument(
        '--ttp_categories', type=unicode, nargs='*', metavar='TEXT')

    group.add_argument(
        '--object_id', type=unicode, metavar='TEXT', help='id of object as stored in data store')

    if commandline is None:
        args = parser.parse_args()
    else:
        args = parser.parse_args(args=commandline.split())

    config = Config(args.config)

    if args.loglevel is not None:
        config.debug_mode = True

    if args.repository is not None:
        config.repository = args.repository

    if config.encoder is not None:
        config.encoder = args.encoder

    return config, args


def _cli_datetime(arg):
    try:
        return datetime.datetime.strptime(arg, '%Y-%m-%d')
    except ValueError:
        return datetime.datetime.strptime(arg, '%Y-%m-%dT%H:%M:%S')

if __name__ == '__main__':
    main()
