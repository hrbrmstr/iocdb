#!/usr/bin/env python

import argparse
import importlib
import json
import sys
import os

from buildinfo import BuildInfo
from iocdb.config import Config
from iocdb.config import ENCODER_CHOICES
from iocdb.encoders import INTELSummaryEncoderSession
from iocdb.utils import get_csv_output
from iocdb.encoders import CustomJSONEncoder


def main():
    config, args = parse_args()

    errors = []
    error_count = 0
    warning_count = 0

    config.feedtest = True
    config.session_limit = 10

    try:
        sys.path.append(os.getcwd())
        feed = importlib.import_module(args.feed)
    except ImportError:
        print("ERROR: Module '%s' not found" % args.feed)
        return 1

    if hasattr(feed, 'feed_config'):
        for key in ['url', 'id', 'category']:
            if key not in feed.feed_config.keys():
                error_count += 1
                errors.append("ERROR: '%s' parameter missing from 'feed_config'" % key)

        for key in ['no_foreign', 'no_commercial']:
            if key not in feed.feed_config.keys():
                warning_count += 1
                errors.append("WARNING: '%s' parameter not explicitly set, 'True' assumed" % key)
    else:
        errors.append("ERROR: 'feed_config' dictionary required")

    if not hasattr(feed, 'process_line'):
        error_count += 1
        errors.append("ERROR: 'process_line' function missing")

    if len(errors) > 0:
        print('\n'.join(errors))
        print('%s errors and %s warnings found' % (error_count, warning_count))
        return 1

    data = []
    count = 0
    max_count = args.count
    for item in feed.process_line():
        count += 1
        data.append(item)
        if count == max_count:
            break

    output(data, args)


def output(data, args):
    if args.encoder == 'json':
        print json.dumps(data, cls=CustomJSONEncoder, indent=2,
                         sort_keys=True)
    elif args.encoder == 'csv':
        print get_csv_output(data, args.csv_columns)
    elif args.encoder == 'summary':
        with INTELSummaryEncoderSession() as summary_encoder:
            data = summary_encoder.get_summary(data)
            if args.summary_json:
                print json.dumps(data, cls=CustomJSONEncoder, indent=2,
                                 sort_keys=True)
            else:
                print get_csv_output(data)


def parse_args():
    parser = argparse.ArgumentParser(description='Extract data from a query repo '
                                                 'using provided query args and encode results.',
                                     version=BuildInfo.get_version_detailed())

    group = parser.add_argument_group('config')
    group.add_argument('--config', help='path to config file')

    group = parser.add_argument_group('logging')
    group.add_argument('--loglevel',
                       choices=('DEBUG', 'INFO', 'WARNING', 'ERROR'))

    group.add_argument_group('encoder session manager')
    group.add_argument('--encoder', help='encoder session manager key',
                       choices=ENCODER_CHOICES, default='json')
    group.add_argument('--csv_columns', nargs='*', metavar='COLUMNNAME')

    group.add_argument_group('feed details')
    group.add_argument('--feed', help='python module name of feed', required=True, type=unicode)
    group.add_argument('--count', help='number of records to display', default=10, type=int)

    args = parser.parse_args()
    config = Config(args.config)

    if config.encoder is not None:
        config.encoder = args.encoder

    return config, args

if __name__ == '__main__':
    main()
