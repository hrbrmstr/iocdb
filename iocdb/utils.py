__author__ = 'Andrew Barilla <andrew.barilla@verizon.com>'

import csv
from datetime import datetime
import StringIO
import cStringIO
import codecs
import urllib2

VALID_DATE_FORMATS = ['%Y-%m-%d', '%Y-%m-%dT%H:%M:%S', '%Y-%m-%dT%H:%M:%S.%f']


def arg_to_datetime(arg):
    if arg is None:
        return None

    for date_format in VALID_DATE_FORMATS:
        try:
            return datetime.strptime(arg, date_format)
        except ValueError:
            pass

    raise ValueError('Invalid date format')


def get_csv_output(data, csv_columns=None):
    buf = StringIO.StringIO()
    # writer = csv.writer(buf, delimiter=',')
    writer = UnicodeWriter(buf, delimiter=',')
    if data is not None:
        is_first = True
        csv_field_names = csv_columns if csv_columns is not None else []
        for row in data:
            if is_first:
                if csv_field_names is None or len(csv_field_names) == 0:
                    csv_field_names = row.csv_field_names
                writer.writerow(csv_field_names)
                is_first = False
            writer.writerow(row.get_csv_fields(csv_field_names))
    return buf.getvalue()


def encode(s):
    try:
        return s.encode('utf-8')
    except UnicodeEncodeError:
        return s
    except AttributeError:
        return s


class UnicodeWriter:
    """
    A CSV writer which will write rows to CSV file "f",
    which is encoded in the given encoding.
    """

    def __init__(self, f, dialect=csv.excel, encoding="utf-8", **kwds):
        # Redirect output to a queue
        self.queue = cStringIO.StringIO()
        self.writer = csv.writer(self.queue, dialect=dialect, **kwds)
        self.stream = f
        self.encoder = codecs.getincrementalencoder(encoding)()

    def writerow(self, row):
        self.writer.writerow([encode(s) if s is not None else None for s in row])
        # Fetch UTF-8 output from the queue ...
        data = self.queue.getvalue()
        data = data.decode("utf-8")
        # ... and reencode it into the target encoding
        data = self.encoder.encode(data)
        # write to the target stream
        self.stream.write(data)
        # empty queue
        self.queue.truncate(0)

    def writerows(self, rows):
        for row in rows:
            self.writerow(row)


def urlopen_with_retry(url, logger=None):
    msg = "Retrieving %s..." % url
    if logger:
        logger.warning()
    else:
        print msg
    delay = 30
    tries = 2
    while tries > 1:
        try:
            return urllib2.urlopen(url, timeout=delay)
        except urllib2.URLError:
            tries -= 1
            msg = "Retrying %s, %s left..." % (url, tries)
            if logger:
                logger.warning()
            else:
                print msg


def find_key(search, dictionary):
    try:
        for key, value in dictionary.iteritems():
            if key == search:
                return value
            else:
                return find_key(search, value)
    except AttributeError:
        pass


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]