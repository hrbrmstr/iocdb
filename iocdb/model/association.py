__author__ = 'Andrew Barilla <andrew.barilla@one.verizon.com'

from document import Document


class Association(object):

    def __init__(self, left, right, valid, document,
                 variety=None, description=None, id=None):
        self.left = left
        self.right = right
        self.valid = valid
        self.document = document
        self.variety = variety
        self.description = description
        self.id = id

    def to_dict(self):
        data = {
            'left': self.left.to_dict() if self.left is not None else None,
            'right': self.right.to_dict() if self.right is not None else None,
            'valid': self.valid,
            'document': self.document.to_dict() if self.document is not None else None,
            'variety': self.variety,
            'description': self.description,
            'id': self.id,
        }

        return data

    def __json__(self):
        return self.to_dict()

    @classmethod
    def from_dict(cls, data):
        if data is None:
            return None

        return Association(Left.from_dict(data['left']),
                           Right.from_dict(data['right']),
                           data.get('valid'),
                           Document.from_dict(data.get('document')),
                           variety=data.get('variety'),
                           description=data.get('description'),
                           id=data.get('id'))

class Left(object):

    def __init__(self, value, variety, id=None):
        self.value = value
        self.variety = variety
        self.id = id

    def to_dict(self):
        data = {
            'value': self.value,
            'variety': self.variety,
            'id': self.id,
        }

        return data

    @classmethod
    def from_dict(cls, data):
        if data is None:
            return None

        return Left(data.get('value'),
                    data.get('variety'),
                    id=data.get('id'))


class Right(object):

    def __init__(self, value, variety, id=None):
        self.value = value
        self.variety = variety
        self.id = id

    def to_dict(self):
        data = {
            'value': self.value,
            'variety': self.variety,
            'id': self.id,
        }

        return data

    @classmethod
    def from_dict(cls, data):
        if data is None:
            return None

        return Right(data.get('value'),
                     data.get('variety'),
                     id=data.get('id'))

