__author__ = 'Andrew Barilla <andrew.barilla@one.verizon.com'

from datetime import datetime

from iocdb.utils import arg_to_datetime

DOCUMENT_CATEGORY = ('ciscp', 'csint', 'govint', 'ids2', 'osint', 'vecirt',
                     'vzir', 'derived', 'atims', 'mss')

TLP = ('red', 'green', 'amber', 'white')


class Document(object):

    def __init__(self, name=None, category=None, source=None,
                 investigator=None, tlp='white', noforn=False, nocomm=False,
                 received=None, text=None, id=None):
        if not (name or category or source):
            raise AttributeError(
                'a document must have an name, category, or source')
        self._category = category
        self.source = source if source is not None else self.category
        self.name = name if name is not None else self.source
        self.investigator = investigator
        self._tlp = tlp
        self.noforn = noforn
        self.nocomm = nocomm
        self.received = received if received is not None else datetime.now()
        self.text = text
        self.id = id

    @property
    def category(self):
        return self._category

    @category.setter
    def category(self, category):
        #TODO: should document category be required?
        if category is not None:
            category = category.lower()
            if category not in DOCUMENT_CATEGORY:
                raise AttributeError(
                    'category %s not in enumerated set' % category)
        self._category = category

    @property
    def tlp(self):
        return self._tlp

    @tlp.setter
    def tlp(self, tlp):
        if tlp not in TLP:
            raise AttributeError(
                'tlp %s not in enumerated set' % tlp)
        self._tlp = tlp

    def to_dict(self):
        data = {
            'category': self.category,
            'id': self.id,
            'investigator': self.investigator,
            'name': self.name,
            'nocomm': self.nocomm,
            'noforn': self.noforn,
            'received': self.received,
            'source': self.source,
            'text': self.text,
            'tlp': self.tlp,
        }

        return data

    @classmethod
    def from_dict(cls, data):
        if data is None:
            return None

        return Document(name=data.get('name'),
                        category=data.get('category'),
                        source=data.get('source'),
                        investigator=data.get('investigator'),
                        tlp=data.get('tlp'),
                        noforn=data.get('noforn'),
                        nocomm=data.get('nocomm'),
                        received=arg_to_datetime(data.get('received')),
                        text=data.get('text'))
