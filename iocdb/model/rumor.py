__author__ = 'Andrew Barilla <andrew.barilla@one.verizon.com'

'''
iocdb domain model
'''

from observable import Observable
from document import Document
from ttp import TTP
from actor import Actor
from campaign import Campaign
import iocdb.utils


class Rumor(object):

    def __init__(self, observable, valid, document, ttp=None, actor=None,
                 campaign=None, description=None, id=None):

        if not (ttp or actor or campaign):
            raise AttributeError(
                'a rumor must have at least one threat ' +
                '(ttp, actor or campaign)')

        if ttp:
            if observable.variety in ('md5', 'sha256', 'sha1') \
               and not ttp.category == 'malware':
                    raise AttributeError(
                        'file observables can only be related to ttp '
                        'category "malware"')

            if ttp.category == 'malware' \
               and observable.variety not in ('md5', 'sha256', 'sha1'):
                    raise AttributeError(
                        'ttp category malware can only be related to '
                        'file observables')

        self.observable = observable
        self.valid = valid
        self.document = document
        self.ttp = ttp
        self.actor = actor
        self.campaign = campaign
        self.description = description
        self.id = id

    def to_dict(self):
        data = {
            'actor': self.actor.to_dict() if self.actor is not None else None,
            'campaign': self.campaign.to_dict() if self.campaign is not None else None,
            'description': self.description,
            'document': self.document.to_dict() if self.document is not None else None,
            'id': self.id,
            'observable': self.observable.to_dict() if self.observable is not None else None,
            'ttp': self.ttp.to_dict() if self.ttp is not None else None,
            'valid': self.valid,
        }

        return data

    def __json__(self):
        return self.to_dict()

    @classmethod
    def from_dict(cls, data):
        if data is None:
            return None

        return Rumor(Observable.from_dict(data['observable']),
                     iocdb.utils.arg_to_datetime(data['valid']),
                     Document.from_dict(data['document']),
                     ttp=TTP.from_dict(data.get('ttp')),
                     actor=Actor.from_dict(data.get('actor')),
                     campaign=Campaign.from_dict(data.get('campaign')),
                     description=data.get('description'),
                     id=data.get('id'))

    @property
    def csv_field_names(self):
        return ['observable_value',
                'ttp_malware',
                'campaign_id',
                'document_text',
                'ttp_id',
                'campaign_name',
                'document_investigator',
                'observable_id',
                'valid',
                'actor_id',
                'document_id',
                'description',
                'ttp_category',
                'document_source',
                'observable_variety',
                'document_name',
                'rumor_id',
                'actor_name',
                'document_received',
                'document_noforn',
                'document_tlp',
                'document_nocomm',
                'ttp_actions',
                'document_category']

    def get_csv_fields(self, field_names=None):
        if field_names is None:
            field_names = self.csv_field_names

        field_names = [x.lower() for x in field_names]

        output = []

        for field_name in field_names:
            if field_name == 'observable_value':
                if self.observable is not None:
                    output.append(self.observable.value)
                else:
                    output.append(None)
            elif field_name == 'ttp_malware':
                if self.ttp is not None:
                    output.append(self.ttp.malware)
                else:
                    output.append(None)
            elif field_name == 'campaign_id':
                if self.campaign is not None:
                    output.append(self.campaign.id)
                else:
                    output.append(None)
            elif field_name == 'document_text':
                if self.document is not None:
                    output.append(self.document.text)
                else:
                    output.append(None)
            elif field_name == 'ttp_id':
                if self.ttp is not None:
                    output.append(self.ttp.id)
                else:
                    output.append(None)
            elif field_name == 'campaign_name':
                if self.campaign is not None:
                    output.append(self.campaign.name)
                else:
                    output.append(None)
            elif field_name == 'document_investigator':
                if self.document is not None:
                    output.append(self.document.investigator)
                else:
                    output.append(None)
            elif field_name == 'observable_id':
                if self.observable is not None:
                    output.append(self.observable.id)
                else:
                    output.append(None)
            elif field_name == 'valid':
                output.append(self.valid)
            elif field_name == 'actor_id':
                if self.actor is not None:
                    output.append(self.actor.name)
                else:
                    output.append(None)
            elif field_name == 'document_id':
                if self.document is not None:
                    output.append(self.document.id)
                else:
                    output.append(None)
            elif field_name == 'description':
                output.append(self.description)
            elif field_name == 'ttp_category':
                if self.ttp is not None:
                    output.append(self.ttp.category)
                else:
                    output.append(None)
            elif field_name == 'document_source':
                if self.document is not None:
                    output.append(self.document.source)
                else:
                    output.append(None)
            elif field_name == 'observable_variety':
                if self.observable is not None:
                    output.append(self.observable.variety)
                else:
                    output.append(None)
            elif field_name == 'document_name':
                if self.document is not None:
                    output.append(self.document.name)
                else:
                    output.append(None)
            elif field_name == 'rumor_id':
                output.append(self.id)
            elif field_name == 'actor_name':
                if self.actor is not None:
                    output.append(self.actor.id)
                else:
                    output.append(None)
            elif field_name == 'document_received':
                if self.document is not None:
                    output.append(self.document.received)
                else:
                    output.append(None)
            elif field_name == 'document_noforn':
                if self.document is not None:
                    output.append(self.document.noforn)
                else:
                    output.append(None)
            elif field_name == 'document_tlp':
                if self.document is not None:
                    output.append(self.document.tlp)
                else:
                    output.append(None)
            elif field_name == 'document_nocomm':
                if self.document is not None:
                    output.append(self.document.nocomm)
                else:
                    output.append(None)
            elif field_name == 'ttp_actions':
                if self.ttp is not None:
                    output.append(self.ttp.actions)
                else:
                    output.append(None)
            elif field_name == 'document_category':
                if self.document is not None:
                    output.append(self.document.category)
                else:
                    output.append(None)
            else:
                output.append(None)
                # TODO handle invalid field name?

        return output
