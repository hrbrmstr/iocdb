__author__ = 'Andrew Barilla <andrew.barilla@one.verizon.com'


class Summary(object):

    observable = None
    document_category = None
    ttp_category = None
    ttp_actions = None
    document_source = None
    days_reported = None
    avg_gap = None
    min_gap = None
    max_gap = None
    tlp_conflict = None
    document_noforn = None
    document_tlp = None
    document_name = None
    description = None
    first_seen = None
    last_seen = None
    actor_name = None
    ttp_malware = None

    def to_dict(self):
        data = {
            'observable': self.observable,
            'document_category': self.document_category,
            'ttp_category': self.ttp_category,
            'ttp_actions': self.ttp_actions,
            'document_source': self.document_source,
            'days_reported': self.days_reported,
            'avg_gap': self.avg_gap,
            'min_gap': self.min_gap,
            'max_gap': self.max_gap,
            'tlp_conflict': self.tlp_conflict,
            'document_noforn': self.document_noforn,
            'document_tlp': self.document_tlp,
            'document_name': self.document_name,
            'description': self.description,
            'first_seen': self.first_seen,
            'last_seen': self.last_seen,
            'actor_name': self.actor_name,
            'ttp_malware': self.ttp_malware,
        }

        return data

    def __json__(self):
        return self.to_dict()

    @property
    def csv_field_names(self):
        return ['observable',
                'first_seen',
                'last_seen',
                'days_reported',
                'avg_gap',
                'min_gap',
                'max_gap',
                'tlp_conflict',
                'document_tlp',
                'document_category',
                'ttp_category',
                'ttp_actions',
                'document_source',
                'document_name',
                'description',
                'document_noforn',
                'actor_name',
                'ttp_malware']

    def get_csv_fields(self, field_names=None):
        if field_names is None:
            field_names = self.csv_field_names

        field_names = [x.lower() for x in field_names]

        output = []

        for field_name in field_names:
            if field_name == 'observable':
                output.append(self.observable)
            elif field_name == 'document_category':
                output.append(self.document_category)
            elif field_name == 'ttp_category':
                output.append(self.ttp_category)
            elif field_name == 'ttp_actions':
                output.append(self.ttp_actions)
            elif field_name == 'document_source':
                output.append(self.document_source)
            elif field_name == 'days_reported':
                output.append(self.days_reported)
            elif field_name == 'avg_gap':
                output.append(self.avg_gap)
            elif field_name == 'min_gap':
                output.append(self.min_gap)
            elif field_name == 'max_gap':
                output.append(self.max_gap)
            elif field_name == 'tlp_conflict':
                output.append(self.tlp_conflict)
            elif field_name == 'document_noforn':
                output.append(self.document_noforn)
            elif field_name == 'document_tlp':
                output.append(self.document_tlp)
            elif field_name == 'document_name':
                output.append(self.document_name)
            elif field_name == 'description':
                output.append(self.description)
            elif field_name == 'first_seen':
                output.append(self.first_seen)
            elif field_name == 'last_seen':
                output.append(self.last_seen)
            elif field_name == 'actor_name':
                output.append(self.actor_name)
            elif field_name == 'ttp_malware':
                output.append(self.ttp_malware)
            else:
                output.append(None)
                # TODO handle invalid field name?

        return output

