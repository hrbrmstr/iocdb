__author__ = 'Andrew Barilla <andrew.barilla@one.verizon.com'


class Actor(object):

    def __init__(self, name, id=None):
        self.name = name
        self.id = id

    def to_dict(self):
        data = {
            'id': self.id,
            'name': self.name,
        }

        return data

    def __json__(self):
        return self.to_dict()

    @classmethod
    def from_dict(cls, data):
        if data is None:
            return None

        return Actor(data['name'],
                     id=data.get('id'))
