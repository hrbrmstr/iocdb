__author__ = 'Andrew Barilla <andrew.barilla@one.verizon.com'

import json

TTP_CATEGORY = ('malicious host', 'bot', 'exfiltration site', 'c2',
                'proxy', 'malware distributor', 'exploit server',
                'malware', 'sinkhole', 'malicious email address')


class TTP(object):

    def __init__(self, category, actions=None, malware=None, id=None):
        self._actions = actions
        self._category = category
        self.malware = malware
        self.id = id

    @property
    def category(self):
        return self._category

    @category.setter
    def category(self, category):
        category = category.lower()
        if category not in TTP_CATEGORY:
            raise AttributeError(
                'category %s not in enumerated set' % category)
        self._category = category

    @property
    def actions(self):
        return self._actions

    @actions.setter
    def actions(self, actions):
        self._actions = json.dumps(actions)

    def to_dict(self):
        data = {
            'actions': self.actions,
            'category': self.category,
            'id': self.id,
            'malware': self.malware,
        }

        return data

    @classmethod
    def from_dict(cls, data):
        if data is None:
            return None

        return TTP(data['category'],
                   actions=data.get('actions'),
                   malware=data.get('malware'),
                   id=data.get('id'))
